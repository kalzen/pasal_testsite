<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('role')->insert([
            'id' => '1',
            'name' => 'Adminstrator',
            'route' => 'all'
        ]);
        DB::table('role')->insert([
            'id' => '2',
            'name' => 'Admin quản lý nội dung website',
            'route' => 'admin.index,admin.block.index,admin.block.create,admin.block.store,admin.block.edit,admin.block.update,admin.block.destroy,admin.testimonial.index,admin.testimonial.create,admin.testimonial.store,admin.testimonial.edit,admin.testimonial.update,admin.testimonial.destroy'
        ]);
        DB::table('role')->insert([
            'id' => '3',
            'name' => 'Admin quản lý data khách hàng',
            'route' => 'admin.index,admin.customer.index,admin.affiliate.create,admin.customer.store,admin.customer.destroy,admin.affiliate.edit,admin.affiliate.update,admin.affiliate.detail,admin.affiliate.toggle,admin.affiliate.toggleGroup,admin.affiliate.report'
        ]);
        DB::table('role')->insert([
            'id' => '4',
            'name' => 'Admin quản lý data link affiliate',
            'route' => 'admin.index,admin.affiliate.index,admin.affiliate.create,admin.affiliate.store,admin.affiliate.destroy,admin.affiliate.edit,admin.affiliate.update,admin.affiliate.detail,admin.affiliate.toogle,admin.affiliate.approve,admin.affiliate.unApprove'
        ]);
    }

}
