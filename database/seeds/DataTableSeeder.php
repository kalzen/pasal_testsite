<?php

use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i = 1; $i <= 100; $i++) {
            DB::table('customer')->insert([
                'fullname' => 'Người dùng số ' . $i,
                'tel' => '0123456789',
                'email' => 'user@email.com',
                'school' => 'DHBK',
                'affiliate_id' => rand(1, 12),
                'schedule_id' => rand(1, 10),
                'basis_id' => rand(1, 10),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 1; $i <= 10; $i++) {
            DB::table('basis')->insert([
                'name' => 'Cơ sở ' . $i,
                'code' => 'CS' . $i,
                'order' => $i,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 1; $i <= 10; $i++) {
            DB::table('schedule')->insert([
                'name' => 'Lịch học ' . $i,
                'schedule' => rand(1, 7),
                'order' => $i,
                'start_time' => '07:00',
                'end_time' => '09:00',
                'start_date' => '2018-06-01',
                'end_date' => '2018-06-30',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 1; $i <= 12; $i++) {
            DB::table('affiliate')->insert([
                'name' => 'Affiliate số ' . $i,
                'code' => 'AFF' . $i,
                'description' => 'Link gán quảng cáo Fb tháng ' . $i . '/2018',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 1; $i <= 12; $i++) {
            if($i<10){
                $name = '0'. $i . '/2018';
            }else{
                $name = $i . '/2018';
            }    
            DB::table('monthly')->insert([
                'id' => $i,
                'name' => $name,
            ]);
            for ($j = 1; $j <= 12; $j++) {
                DB::table('affiliate_monthly')->insert([
                    'affiliate_id' => $i,
                    'monthly_id' => $j,
                    'count_data' => $i,
                    'count_traffic' => $i*$j
                ]);
            }
            for ($j = 1; $j <= 10; $j++) {
                DB::table('basis_monthly')->insert([
                    'basis_id' => $j,
                    'monthly_id' => $i,
                    'count_data' => rand(1, 1000)
                ]);
            }
        }

    }

}
