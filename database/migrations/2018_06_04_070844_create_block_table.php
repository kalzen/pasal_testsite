<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('video_url', 255)->nullable();
            $table->string('position', 255)->nullable();
            //slide (3), about-us (1), statistics (1), faq (1-5), features (1-4), brochure (1-4), counters (1-4)
            $table->longtext('icon_url')->nullable();
            $table->integer('parent_id')->default(0);
            $table->integer('order')->default(0);
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block');
    }
}
