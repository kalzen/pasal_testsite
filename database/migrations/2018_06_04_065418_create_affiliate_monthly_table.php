<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateMonthlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_monthly', function (Blueprint $table) {
            $table->integer('affiliate_id');
            $table->integer('monthly_id');
            $table->integer('count_data');
            $table->integer('count_traffic');
            $table->primary(['affiliate_id', 'monthly_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_monthly');
    }
}
