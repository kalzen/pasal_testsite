

$(function() {

    // Full featured editor
    CKEDITOR.replace( 'editor-full', {
        height: '400px',
        extraPlugins: 'forms'
    });
    
});
