jQuery(function () {
    $('body').delegate('[data-action="remove_thumb"]', 'click', function () {
        console.log('adssad');
        $(this).parents('.thumb-container').remove();
        $('input[name="' + this.dataset.field + '"]').val('');
    });
    if ($('.select').length)
        $('.select').select({minimumResultsForSearch: Infinity});
    if ($('.file-input-overwrite').length) {
        //
        // Define variables
        //

        // Modal template
        var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                '  <div class="modal-content">\n' +
                '    <div class="modal-header">\n' +
                '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                '    </div>\n' +
                '    <div class="modal-body">\n' +
                '      <div class="floating-buttons btn-group"></div>\n' +
                '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>\n';

        // Buttons inside zoom modal
        var previewZoomButtonClasses = {
            toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
            fullscreen: 'btn btn-default btn-icon btn-xs',
            borderless: 'btn btn-default btn-icon btn-xs',
            close: 'btn btn-default btn-icon btn-xs'
        };

        // Icons inside zoom modal classes
        var previewZoomButtonIcons = {
            prev: '<i class="icon-arrow-left32"></i>',
            next: '<i class="icon-arrow-right32"></i>',
            toggleheader: '<i class="icon-menu-open"></i>',
            fullscreen: '<i class="icon-screen-full"></i>',
            borderless: '<i class="icon-alignment-unalign"></i>',
            close: '<i class="icon-cross3"></i>'
        };

        // File actions
        var fileActionSettings = {
            zoomClass: 'btn btn-link btn-xs btn-icon',
            zoomIcon: '<i class="icon-zoomin3"></i>',
            dragClass: 'btn btn-link btn-xs btn-icon',
            dragIcon: '<i class="icon-three-bars"></i>',
            removeClass: 'btn btn-link btn-icon btn-xs',
            removeIcon: '<i class="icon-trash"></i>',
            indicatorNew: '<i class="icon-file-plus text-slate"></i>',
            indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
            indicatorError: '<i class="icon-cross2 text-danger"></i>',
            indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
        };
        $(".file-input-overwrite").each(function () {
            var field = this.dataset.field;
            var images_str = $('input[name="' + field + '"]').data('value');
            var url = [];
            var file_size = [];
            var file_name = [];
            var images = [];
            var image_info = [];
            if (images_str) {
                url = images_str.split('|');
                file_size = this.dataset.size.split('|');
                file_name = this.dataset.name.split('|');
            }
            if (url && url.length) {
                for (i = 0; i < url.length; i++) {
                    images.push('/' + url[i]);
                    image_info.push({caption: file_name[i], size: file_size[i], key: (i + 1)});
                }
            }
            $(this).fileinput({
                browseLabel: 'Browse',
                browseIcon: '<i class="icon-file-plus"></i>',
                uploadIcon: '<i class="icon-file-upload2"></i>',
                removeIcon: '<i class="icon-cross3"></i>',
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    modal: modalTemplate
                },
                showUpload: false,
                initialPreview: images,
                initialPreviewConfig: image_info,
                initialPreviewAsData: true,
                overwriteInitial: true,
                previewZoomButtonClasses: previewZoomButtonClasses,
                previewZoomButtonIcons: previewZoomButtonIcons,
                fileActionSettings: fileActionSettings
            });
        });
        $('.file-input-overwrite').on('change', function (event) {
            var field = this.dataset.field;
            $('input[name="' + field + '_old"]').val('');
        });
        $('.fileinput-remove').on('click', function (event) {
            $('input[name="icon_url_old"]').val('');
        });
        $('[data-toggle="tooltip"]').tooltip();
    }
    $('[data-action="answer"]').click(function (e) {
        $('input[name="question_id"]').attr('value', $(this).data('id'));
    })
    $('body').delegate('[data-action="mark"]', 'click', function (e) {
        e.preventDefault();
        trainer_id = $(this).data('trainerId');
        user_id = $(this).data('userId');
        if (trainer_id !== user_id) {
            alert('Bạn không được phép chấm điểm cho thành viên này');
            return;
        }
        $.ajax({
            url: '/api/get-mark-modal',
            method: 'post',
            type: 'JSON',
            data: {id: $(this).data('id')},
            success: function (resp) {
                resp = JSON.parse(resp);
                $('#modal_form_vertical').html(resp.html);
                $('#modal_form_vertical').modal('show');
                if ($('.touchspin-vertical').length) {
                    $(".touchspin-vertical").TouchSpin({
                        min: -1000000000,
                        max: 1000000000,
                        verticalbuttons: true,
                        verticalupclass: 'icon-arrow-up22',
                        verticaldownclass: 'icon-arrow-down22'
                    });
                }
            }
        })
    })
    $('#frmMembertest').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/update-member-test',
            method: 'post',
            type: 'JSON',
            data: $(this).serialize,
            success: function (resp) {
                resp = JSON.parse(resp);
                if (resp.success == true) {
                    alert('Chấm điểm thành công!');
                    location.href = '/admin/member_test';
                }
            }
        })
    })
    $('body').delegate('[data-action="toggle"]', 'click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/toggle-member-test',
            method: 'post',
            type: 'JSON',
            data: {id: $(this).data('id')},
            success: function (resp) {
                if (resp.success === true) {
                    alert('Cập nhật thành công!');
                    location.href = '/admin/member_test';
                }
            }
        })
    })
    $('.editable').editable({
        type: 'number',
        title: "Nhập giá trị mới",
        success: function (response, newValue) {
            var attr = $(this).data('attr');
            var id = $(this).data('id');
            var table = $(this).data('table');
            $.ajax({
                url: "/api/quickupdate",
                method: "POST",
                data: {
                    attr: attr,
                    newValue: newValue,
                    id: id,
                    table: table
                },
                success: function (response) {

                },
                error: function (res) {
                    console.log(res);
                }
            });
        }
    });
    //Chuyển quản lý mới
    $('#switchUser').on('click', function () {
        var ids = [];
        $('.check').each(function () {
            if ($(this).parents('span').hasClass('checked')) {
                ids.push(this.value);
            }
        })
        if (!ids.length) {
            alert('Bạn chưa chọn thành viên nào');
            return;
        }
        var member_ids = new Set(ids);
        $('input[name="member_ids"]').attr('value', Array.from(member_ids).join(','));
        $('#switchUserModal').modal('show');
    })
    //Chọn huấn luyện
    $('#selectMember').on('click', function () {
        var ids = [];
        has_trainer = false;
        $('.check').each(function () {
            if ($(this).parents('span').hasClass('checked')) {
                console.log(this.value);
                ids.push(this.value);
                if ($(this).parents('tr').find('#trainer').html() !== "")
                    has_trainer = true;
            }
        })
        if (!ids.length) {
            alert('Bạn chưa chọn thành viên nào');
            return;
        }
        if (has_trainer) {
            alert('Thành viên bạn chọn đã có huấn luyện viên');
            return;
        }
        var member_ids = Array.from(new Set(ids)).join(',');
        $.ajax({
            url: '/api/update-trainer',
            data: {member_ids: member_ids},
            method: 'post',
            type: 'JSON',
            success: function (resp) {
                alert(resp.message);
                location.reload();
            }
        })
    })
    //Thay đổi trạng thái của thành viên
    $('[data-action="toggle-status"]').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/quickupdate',
            method: 'post',
            data: {id: $(this).data('id'), table: $(this).data('table'), attr: $(this).data('attr'), newValue: $(this).data('value')},
            success: function (resp) {
                alert('Cập nhật thành công');
                location.reload();
            }
        })
    })
    //
    $('#role_id').change(function (e) {
        var sel = document.getElementById("role_id");
        var role_id = sel.options[sel.selectedIndex].value;
        e.preventDefault();
        $.ajax({
            url: '/api/get-trainer-by-role',
            method: 'post',
            data: {'role_id': role_id},
            success: function (resp) {
                $('#trainer_id').html(resp.html);
            }
        })
    })
    // Default file input style
    $(".file-styled").uniform({
        fileButtonClass: 'action btn btn-default'
    });
    // Primary file input
    $(".file-styled-primary").uniform({
        fileButtonClass: 'action btn bg-blue'
    });
    $('.styled').uniform();
    $('#add-answer').click(function () {
        var count = $('.group-question').length + 1;
        var html = '<div class="group group-question">' +
                '<label class="control-label col-md-3 required">Đáp án ' + count + ':</label>' +
                '<div class="col-md-9">' +
                '<div class="input-group">' +
                '<input type="text" class="form-control" name="content_question[' + count + ']">' +
                '<span class="input-group-addon">' +
                '<input type="checkbox" class="styled" name="status_question[' + count + ']" value="' + count + '">' +
                '</span>' +
                '</div>' +
                '</div>' +
                '</div>';
        $('.content-answer').append(html);
        $('.styled').uniform();
    });

    $('.province-value').change(function () {
        if ($(this).val() != 0) {
            $('.province').hide();
            $('.province-' + $(this).val()).show();
        } else {
            $('.province').hide();
            $('select[name=basis_id]').val('');
        }
    });
});