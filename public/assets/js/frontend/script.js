$(document).ready(function () {
    $(".a-row").on('click', function () {
        $('.a-row').removeAttr("style").attr("style", "")
        $(this).css('color', '#f15b26');
    });
    $('.btn-next-question').click(function () {
        var nth = $(this).attr('data-nth');
        var max = $(this).data('max');
        var new_nth = ++nth;
        if (new_nth <= max) {
            $(this).attr('data-nth', new_nth);
            $('.list-question').hide();
            $('.question-' + new_nth).slideDown();
            $('.nth-question').html(new_nth)
        }else{
            var href = $('#next-request').val();
            $(this).attr('href', href);
        }
    })
    $('.province-value').change(function (){
        if($(this).val()!=0){
            $('.province').hide();
            $('.province-'+$(this).val()).show();
        }else{
            $('.province').hide();
            $('select[name=basis_id]').val('');
        }
    });

});