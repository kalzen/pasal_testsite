<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



/* AUTH */
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/login', ['as' => 'postLogin', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
//Route::get('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
//Route::post('/register', ['as' => 'postRegister', 'uses' => 'Auth\AuthController@postRegister']);
//Route::get('login/{provider}', 'Auth\SocialController@redirect');
//Route::get('login/{provider}/callback', 'Auth\SocialController@callback');
Route::get('/thong-tin', ['as' => 'register', 'uses' => 'Frontend\FrontendController@registerFE']);
Route::post('/thong-tin', ['as' => 'register.info', 'uses' => 'Frontend\FrontendController@register']);
Route::get('/kiem-tra/{alias}', ['as' => 'test', 'uses' => 'Frontend\FrontendController@test']);
Route::get('/thank-you', ['as' => 'thank.you', 'uses' => 'Frontend\FrontendController@thankYou']);
Route::get('/ket-qua', ['as' => 'result', 'uses' => 'Frontend\FrontendController@result']);
Route::post('/api/push-result', ['as' => 'api.push.result', 'uses' => 'Frontend\FrontendController@pushResult']);
Route::post('/appointment', ['as' => 'appointment', 'uses' => 'Frontend\FrontendController@appointment']);
Route::get('/api/getUpdatePhase4', ['as' => 'getUpdatePhase4', 'uses' => 'Frontend\FrontendController@getUpdatePhase4']);
Route::get('/member/export/result/{tel}', ['as' => 'admin.member.export', 'uses' => 'Backend\MemberController@export']);
Route::get('/member/sendmail/result/{tel}', ['as' => 'admin.member.sendmail', 'uses' => 'Backend\MemberController@sendmail']);
/* ADMIN */
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('/', ['as' => 'admin.index', 'uses' => 'Backend\BackendController@index']);
    /* Cấu hình website */
    Route::get('/config', ['as' => 'admin.config.index', 'uses' => 'Backend\ConfigController@index']);
    Route::post('/config/update/{config}', ['as' => 'admin.config.update', 'uses' => 'Backend\ConfigController@update']);

    /* Quản lý user */
    Route::get('/user', ['as' => 'admin.user.index', 'uses' => 'Backend\UserController@index']);
    Route::get('/user/create', ['as' => 'admin.user.create', 'uses' => 'Backend\UserController@create']);
    Route::post('/user/store', ['as' => 'admin.user.store', 'uses' => 'Backend\UserController@store']);
    Route::get('/user/edit/{id}', ['as' => 'admin.user.edit', 'uses' => 'Backend\UserController@edit']);
    Route::post('/user/update/{id}', ['as' => 'admin.user.update', 'uses' => 'Backend\UserController@update']);
    Route::delete('/user/delete/{id}', ['as' => 'admin.user.destroy', 'uses' => 'Backend\UserController@destroy']);

    /* Quản lý customer */
    Route::get('/customer', ['as' => 'admin.customer.index', 'uses' => 'Backend\CustomerController@index']);
    Route::post('/customer/store', ['as' => 'admin.customer.store', 'uses' => 'Backend\CustomerController@store']);
    Route::delete('/customer/delete/{id}', ['as' => 'admin.customer.destroy', 'uses' => 'Backend\CustomerController@destroy']);
    Route::post('/customer/export', ['as' => 'admin.customer.export', 'uses' => 'Backend\CustomerController@export']);
    Route::get('/customer/chart', ['as' => 'admin.customer.chart', 'uses' => 'Backend\CustomerController@chart']);

    /* Quản lý link affiliate */
    Route::get('/affiliate', ['as' => 'admin.affiliate.index', 'uses' => 'Backend\AffiliateController@index']);
    Route::get('/affiliate/create', ['as' => 'admin.affiliate.create', 'uses' => 'Backend\AffiliateController@create']);
    Route::post('/affiliate/store', ['as' => 'admin.affiliate.store', 'uses' => 'Backend\AffiliateController@store']);
    Route::delete('/affiliate/delete/{id}', ['as' => 'admin.affiliate.destroy', 'uses' => 'Backend\AffiliateController@destroy']);
    Route::get('/affiliate/update/{id}', ['as' => 'admin.affiliate.edit', 'uses' => 'Backend\AffiliateController@edit']);
    Route::post('/affiliate/update/{id}', ['as' => 'admin.affiliate.update', 'uses' => 'Backend\AffiliateController@update']);
    Route::get('/affiliate/detail/{id}', ['as' => 'admin.affiliate.detail', 'uses' => 'Backend\AffiliateController@detail']);
    Route::get('/affiliate/toggle/{id}', ['as' => 'admin.affiliate.toggle', 'uses' => 'Backend\AffiliateController@toggle']);
    Route::post('/affiliate/toggle/group', ['as' => 'admin.affiliate.toggleGroup', 'uses' => 'Backend\AffiliateController@toggleGroup']);
    Route::get('/affiliate/report', ['as' => 'admin.affiliate.report', 'uses' => 'Backend\AffiliateController@report']);

    /* Quản lý cơ sở */
    Route::get('/basis', ['as' => 'admin.basis.index', 'uses' => 'Backend\BasisController@index']);
    Route::get('/basis/create', ['as' => 'admin.basis.create', 'uses' => 'Backend\BasisController@create']);
    Route::post('/basis/store', ['as' => 'admin.basis.store', 'uses' => 'Backend\BasisController@store']);
    Route::get('/basis/update/{id}', ['as' => 'admin.basis.edit', 'uses' => 'Backend\BasisController@edit']);
    Route::post('/basis/update/{id}', ['as' => 'admin.basis.update', 'uses' => 'Backend\BasisController@update']);
    Route::delete('/basis/delete/{id}', ['as' => 'admin.basis.destroy', 'uses' => 'Backend\BasisController@destroy']);

    /* Quản lý tỉnh thành */
    Route::get('/province', ['as' => 'admin.province.index', 'uses' => 'Backend\ProvinceController@index']);
    Route::get('/province/create', ['as' => 'admin.province.create', 'uses' => 'Backend\ProvinceController@create']);
    Route::post('/province/store', ['as' => 'admin.province.store', 'uses' => 'Backend\ProvinceController@store']);
    Route::get('/province/update/{id}', ['as' => 'admin.province.edit', 'uses' => 'Backend\ProvinceController@edit']);
    Route::post('/province/update/{id}', ['as' => 'admin.province.update', 'uses' => 'Backend\ProvinceController@update']);
    Route::delete('/province/delete/{id}', ['as' => 'admin.province.destroy', 'uses' => 'Backend\ProvinceController@destroy']);

    /* Quản lý lịch học */
    Route::get('/schedule', ['as' => 'admin.schedule.index', 'uses' => 'Backend\ScheduleController@index']);
    Route::get('/schedule/create', ['as' => 'admin.schedule.create', 'uses' => 'Backend\ScheduleController@create']);
    Route::post('/schedule/store', ['as' => 'admin.schedule.store', 'uses' => 'Backend\ScheduleController@store']);
    Route::get('/schedule/update/{id}', ['as' => 'admin.schedule.edit', 'uses' => 'Backend\ScheduleController@edit']);
    Route::post('/schedule/update/{id}', ['as' => 'admin.schedule.update', 'uses' => 'Backend\ScheduleController@update']);
    Route::delete('/schedule/delete/{id}', ['as' => 'admin.schedule.destroy', 'uses' => 'Backend\ScheduleController@destroy']);

    /* Quản lý content website */
    Route::get('/block', ['as' => 'admin.block.index', 'uses' => 'Backend\BlockController@index']);
    Route::get('/block/create', ['as' => 'admin.block.create', 'uses' => 'Backend\BlockController@create']);
    Route::post('/block/store', ['as' => 'admin.block.store', 'uses' => 'Backend\BlockController@store']);
    Route::get('/block/update/{id}', ['as' => 'admin.block.edit', 'uses' => 'Backend\BlockController@edit']);
    Route::post('/block/update/{id}', ['as' => 'admin.block.update', 'uses' => 'Backend\BlockController@update']);
    Route::delete('/block/delete/{id}', ['as' => 'admin.block.destroy', 'uses' => 'Backend\BlockController@destroy']);

    /* Quản lý content testimonial */
    Route::get('/testimonial', ['as' => 'admin.testimonial.index', 'uses' => 'Backend\TestimonialController@index']);
    Route::get('/testimonial/create', ['as' => 'admin.testimonial.create', 'uses' => 'Backend\TestimonialController@create']);
    Route::post('/testimonial/store', ['as' => 'admin.testimonial.store', 'uses' => 'Backend\TestimonialController@store']);
    Route::get('/testimonial/update/{id}', ['as' => 'admin.testimonial.edit', 'uses' => 'Backend\TestimonialController@edit']);
    Route::post('/testimonial/update/{id}', ['as' => 'admin.testimonial.update', 'uses' => 'Backend\TestimonialController@update']);
    Route::delete('/testimonial/delete/{id}', ['as' => 'admin.testimonial.destroy', 'uses' => 'Backend\TestimonialController@destroy']);

    /* Quản lý content test */
    Route::get('/test/{phase}', ['as' => 'admin.test.index', 'uses' => 'Backend\TestController@index']);
    Route::get('/test/{phase}/question', ['as' => 'admin.test.list', 'uses' => 'Backend\TestController@listQuestion']);
    Route::get('/test/create', ['as' => 'admin.test.create', 'uses' => 'Backend\TestController@create']);
    Route::post('/test/store', ['as' => 'admin.test.store', 'uses' => 'Backend\TestController@store']);
    Route::post('/test/storeQuestion', ['as' => 'admin.test.storeQuestion', 'uses' => 'Backend\TestController@storeQuestion']);
    Route::get('/test/update/{id}', ['as' => 'admin.test.edit', 'uses' => 'Backend\TestController@edit']);
    Route::post('/test/update/{id}', ['as' => 'admin.test.update', 'uses' => 'Backend\TestController@update']);
    Route::get('/test/update/question/{id}', ['as' => 'admin.test.question.edit', 'uses' => 'Backend\TestController@editQuestion']);
    Route::post('/test/update/question/{id}', ['as' => 'admin.test.question.update', 'uses' => 'Backend\TestController@updateQuestion']);
    Route::post('/result/update', ['as' => 'admin.result.update', 'uses' => 'Backend\TestController@updateResult']);
    Route::delete('/test/delete/{id}', ['as' => 'admin.test.destroy', 'uses' => 'Backend\TestController@destroy']);
    Route::get('/input_test', ['as' => 'admin.input_test.get', 'uses' => 'Backend\TestController@inputTest']);
    Route::post('/input_test', ['as' => 'admin.input_test.post', 'uses' => 'Backend\TestController@inputTestPost']);

    /* Rank */
    Route::get('/rank/{phase}', ['as' => 'admin.rank.index', 'uses' => 'Backend\RankController@index']);
    Route::get('/rank/create/{phase}', ['as' => 'admin.rank.create', 'uses' => 'Backend\RankController@create']);
    Route::post('/rank/store', ['as' => 'admin.rank.store', 'uses' => 'Backend\RankController@store']);
    Route::get('/rank/edit/{id}', ['as' => 'admin.rank.edit', 'uses' => 'Backend\RankController@edit']);
    Route::post('/rank/update/{id}', ['as' => 'admin.rank.update', 'uses' => 'Backend\RankController@update']);
    Route::delete('/rank/delete/{id}', ['as' => 'admin.rank.destroy', 'uses' => 'Backend\RankController@destroy']);

    /* Quản lý huấn luyện viên */
    Route::get('/trainer', ['as' => 'admin.trainer.index', 'uses' => 'Backend\TrainerController@index']);
    Route::get('/trainer/create', ['as' => 'admin.trainer.create', 'uses' => 'Backend\TrainerController@create']);
    Route::post('/trainer/store', ['as' => 'admin.trainer.store', 'uses' => 'Backend\TrainerController@store']);
    Route::get('/trainer/update/{id}', ['as' => 'admin.trainer.edit', 'uses' => 'Backend\TrainerController@edit']);
    Route::post('/trainer/update/{id}', ['as' => 'admin.trainer.update', 'uses' => 'Backend\TrainerController@update']);
    Route::delete('/trainer/delete/{id}', ['as' => 'admin.trainer.destroy', 'uses' => 'Backend\TrainerController@destroy']);

    /* Quản lý thành viên */
    Route::get('/member', ['as' => 'admin.member.index', 'uses' => 'Backend\MemberController@index']);
    Route::get('/member/create', ['as' => 'admin.member.create', 'uses' => 'Backend\MemberController@create']);
    Route::post('/member/store', ['as' => 'admin.member.store', 'uses' => 'Backend\MemberController@store']);
    Route::get('/member/update/{id}', ['as' => 'admin.member.edit', 'uses' => 'Backend\MemberController@edit']);
    Route::post('/member/update/{id}', ['as' => 'admin.member.update', 'uses' => 'Backend\MemberController@update']);
    Route::delete('/member/delete/{id}', ['as' => 'admin.member.destroy', 'uses' => 'Backend\MemberController@destroy']);
    Route::post('/member/switch', ['as' => 'admin.member.switch', 'uses' => 'Backend\MemberController@switchTrainer']);

    /* Quản lý thử thách thành viên */
    Route::get('/member_test', ['as' => 'admin.member_test.index', 'uses' => 'Backend\MemberTestController@index']);
    Route::post('/member_test/update/{member_id}/{test_id}', ['as' => 'admin.member_test.update', 'uses' => 'Backend\MemberTestController@update']);
    Route::get('/member_test/{member_id}', ['as' => 'admin.member_test.detail', 'uses' => 'Backend\MemberTestController@detail']);
    Route::delete('/member_test/delete/{id}', ['as' => 'admin.member_test.destroy', 'uses' => 'Backend\MemberTestController@destroy']);
});
Route::get('/{ref?}', ['as' => 'home', 'uses' => 'Frontend\FrontendController@index']);

Route::group(['prefix' => 'api', 'middleware' => 'api'], function() {
    Route::post('/register', ['as' => 'customer.register', 'uses' => 'Frontend\FrontendController@register']);
    Route::post('/traffic', ['as' => 'customer.traffic', 'uses' => 'Frontend\FrontendController@traffic']);
    Route::post('/modal/login', ['as' => 'api.login', 'uses' => 'Api\AuthController@login']);
    Route::post('/modal/register', ['as' => 'api.register', 'uses' => 'Api\UserController@register']);
    Route::get('/logout', ['as' => 'api.logout', 'uses' => 'Api\AuthController@logout']);
    Route::post('/get-mark-modal', ['as' => 'api.member_test.get_modal', 'uses' => 'Backend\MemberTestController@getMarkModal']);
    Route::post('/update-member-test/{member_id}/{test_id}', ['as' => 'api.member_test.update', 'uses' => 'Backend\MemberTestController@update']);
    Route::post('/upload-audio', ['as' => 'upload.audio', 'uses' => 'Api\UserController@uploadAudio']);
    Route::post('/add-message', ['as' => 'api.question.create', 'uses' => 'Frontend\QuestionController@create']);
});

Route::post('/update-profile', ['as' => 'update.profile', 'uses' => 'Frontend\MemberController@updateProfile']);
Route::post('/api/update-member-test/{id}', ['as' => 'api.member_test.update', 'uses' => 'Backend\MemberTestController@update']);
Route::post('/api/toggle-member-test', ['as' => 'api.member_test.toggle', 'uses' => 'Backend\MemberTestController@toggle']);
Route::post('/api/update-view-count', ['as' => 'api.member_test.update_view_count', 'uses' => 'Frontend\MemberController@updateViewCount']);
Route::post('/api/quickupdate', ['as' => 'api.backend.quickupdate', 'uses' => 'Backend\BackendController@quickUpdate']);
Route::post('/api/answer/update', ['as' => 'api.answer.update', 'uses' => 'Backend\AnswerController@update']);
Route::post('/api/get-trainer-by-role', ['as' => 'api.user.get_trainer', 'uses' => 'Backend\TrainerController@getTrainerOptions']);
Route::post('/api/update-trainer', ['as' => 'api.member.update_trainer', 'uses' => 'Backend\MemberController@updateTrainer']);
Route::post('/create/url-video', ['as' => 'create.url.video', 'uses' => 'Frontend\MemberController@createURLChanllenge']);
