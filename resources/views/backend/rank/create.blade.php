
@extends('backend.layouts.master')
@section('content')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_test')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.rank.index', ['phase'=>$phase])!!}">Quản lý xếp hạng</a></li>
            <li class="action">{{trans('base.update_test')}}</li>
        </ul>
    </div>
</div>
<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Tạo mới xếp hạng</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <form action="{!!route('admin.rank.store')!!}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="phase_id" value="{{$phase}}"/>
                <div class="group">
                    <label class="control-label col-md-3 required">Tên xếp hạng:</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="name" />
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Mốc điểm đầu:</label>
                    <div class="col-md-9">
                        <input class="form-control col-md-9" type="number" name="start_point" />
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Mốc điểm cuối:</label>
                    <div class="col-md-9">
                        <input type="number" class="form-control" name="end_point"/>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Nhập nội dung vấn đề:</label>
                    <div class="col-md-9">
                        <textarea id="content" name="content"></textarea>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Nhập nội dung giải pháp:</label>
                    <div class="col-md-9">
                        <textarea id="solution" name="solution"></textarea>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
@stop

@section('script')
<script type="text/javascript">
    CKEDITOR.replace('content', {
    });
    CKEDITOR.replace('solution', {
    });
</script>
@stop