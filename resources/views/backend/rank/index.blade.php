@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản lý xếp hạng</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{!!route('admin.rank.create', ['phase'=>$phase])!!}" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Tạo mới bài tập</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.rank.index', ['phase'=>$phase])!!}">Quản lý xếp hạng</a></li>
            </ul>
        </div>
    </div>
    <div class="content">

        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-solid nav-tabs-component">
                        <li><a href="{!!route('admin.test.index', ['phase'=>$phase])!!}">Danh sách bài tập</a></li>
                        <li><a href="{!!route('admin.test.list', ['phase'=>$phase])!!}">Danh sách câu hỏi</a></li>
                        <li class="active"><a>Quy tắc xếp hạng</a></li>
                    </ul>
                </div>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="table-responsive">
                        <table class="table datatable-basic">
                            <thead>
                                <tr>
                                    <th>
                                        <form method="POST" class="form-group">  
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input class="styled" id="checkall" type="checkbox" name="group[]" value="0"/>
                                        </form>
                                    </th>
                                    <th>{{trans('base.id')}}</th>
                                    <th>Tên xếp hạng</th>
                                    <th>Mốc điểm đầu</th>
                                    <th>Mốc điểm cuối</th>       
                                    <th>{{trans('base.action')}}</th>                           
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($records as $key=>$record)
                                <tr>
                                    <td><input class="check styled" type="checkbox" name="group[]" value="{{$record->id}}"/></td>
                                    <td>{!!$record->id!!}</td>
                                    <td>{!!$record->name!!}</td>
                                    <td>{!!$record->start_point!!}</td>
                                    <td>{!!$record->end_point!!}</td>
                                    <td>
                                        <a  href="{!!route('admin.rank.edit', $record->id)!!}" title="Cập nhật" class="text-success">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <div class="delete" style="display: inline-block">
                                            <form action="{!! route('admin.rank.destroy', $record->id) !!}" method="POST">
                                                {!! method_field('DELETE') !!}
                                                {!! csrf_field() !!}
                                                <a title="{!! trans('backend/base.btn_delete') !!}" class="delete text-danger">
                                                    <i class="icon-close2"></i>
                                                </a>              
                                            </form>  
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>

</div>

@stop
