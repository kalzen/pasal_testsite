@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_member_test')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.member_test.index')!!}">{{trans('base.manage_member_test')}}</a></li>
            </ul>
        </div>
    </div>
    <div class="content">
        @if (Session::has('success'))
        <div class="alert bg-success alert-styled-left">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">{{ Session::get('success') }}</span>
        </div>
        @endif
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.member_information')}}</h5> 
            </div>   
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label text-bold col-md-3">Họ tên:</label>
                                <span class="col-md-9">{{$member->fullname}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label text-bold col-md-3">Ngày tạo:</label>
                                <span class="col-md-9">{{$member->created_at()}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label text-bold col-md-3">Số điện thoại:</label>
                                <span class="col-md-9">{{$member->phone}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label text-bold col-md-3">Email:</label>
                                <span class="col-md-9">{{$member->email}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_prcatical_test')}}</h5> 
            </div>   
            <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>{{trans('base.test_name')}}</th>
                        <th>{{trans('base.practical_link')}}</th>
                        <th>{{trans('base.member')}}</th>
                        <th>{{trans('base.trainer')}}</th>
                        <th>{{trans('base.point')}}</th>
                        <th>{{trans('base.test_status')}}</th>
                        <th>{{trans('base.sent_day')}}</th>
                        <th>{{trans('base.action')}}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($records as $key=>$record)
                    <tr>
                        <td>{{$record->test->name}}</td>
                        <td>
                            @if($record->video_link)
                            <a youtubeid="{{$record->getIdYoutube()}}" class="youtube-link">
                                <img src="/uploads/play-all.png" width="50">
                            </a>
                            @elseif($record->audio_link)
                            <audio controls="">
                                <source src="{!!$record->linkAudio()!!}" type="audio/mpeg">
                            </audio>
                            @endif
                        </td>
                        <td>{{$record->member->fullname}}</td>                        
                        <td>@if($record->member->trainer()->exists()){{$record->member->trainer->name}}@endif</td>
                        <td>{{$record->percent_point}}%</td>
                        <td title="{{$record->comment}}">{{$record->getComment()}}</td>
                        <td>{{$record->getCreatedAt()}}</td>
                        <td>
                            <i class="icon-pencil7 position-right" data-trainer-id="{{$record->member->trainer()->exists()?$record->member->trainer->id:0}}" data-user-id="{{\Auth::user()->id}}" title="Chấm điểm" style="cursor:pointer" data-action="mark" data-id="{{$record->id}}"></i>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Khung trò chuyện cùng huấn luyện viên</h5> 
            </div>   
            <table class="table">
                <thead>
                    <tr>
                        <th>Tin nhắn của thành viên</th>
                        <th>Ngày gửi</th>
                        <th>Trả lời của huấn luyện viên</th>
                        <th>Ngày trả lời</th>
                        <th>Control</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($questions as $key=>$record)
                    <tr>
                        <td>{{$record->content}}</td>
                        <td>{{$record->getCreatedAt()}}</td>
                        <td>@if($record->answer()->exists()){{$record->answer->content}}@endif</td>
                        <td>@if($record->answer()->exists()){{$record->answer->getUpdatedAt()}}@endif</td>  
                        <td>
                            <i class="icon-pencil7 position-right" title="Trả lời" style="cursor:pointer" data-action="answer" data-toggle="modal" href="#modal_answer" data-id="{{$record->id}}"></i>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <!-- /basic datatable -->
    </div>
</div>
<div id="modal_answer" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title text-bold">Nhập nội dung</h5>
            </div>

            <form id="frmAnswer" action="{{route('api.answer.update')}}" method="post">
                <input class="hidden" value="" name="question_id"/>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea type="text" name="content" autofocus class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="modal_form_vertical" class="modal fade" tabindex="-1">
</div>
<!-- /vertical form modal -->
@stop

