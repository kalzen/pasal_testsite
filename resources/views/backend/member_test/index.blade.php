@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_member_test')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.member_test.index')!!}">{{trans('base.manage_member_test')}}</a></li>
            </ul>
        </div>
    </div>
    <div class="content">
        @if (Session::has('success'))
        <div class="alert bg-success alert-styled-left">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">{{ Session::get('success') }}</span>
        </div>
        @endif
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Công cụ tìm kiếm<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form action="{{route('admin.member_test.index')}}" method="GET">  
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-3 control-label">Trạng thái trò chuyện:</label>
                                <div class="col-md-9 form-group">
                                    <input class="styled" type="checkbox" @if(isset($filter['chat_status']))checked @endif name="chat_status" value="1">
                                    Có tin nhắn mới
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary legitRipple">Tìm kiếm <i class="icon-arrow-right14 position-right"></i></button>
                    </div>

                </div>
            </form>
        </div>
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_member_test')}}</h5> 
            </div>   
            <div class="table-responsive">
                <table class="table datatable-basic table-bordered ">
                    <thead>
                        <tr>
                            <th>{{trans('base.test_name')}}</th>
                            <th>{{trans('base.practical_link')}}</th>
                            <th>{{trans('base.member')}}</th>
                            <th>{{trans('base.trainer')}}</th>
                            <th>{{trans('base.point')}}</th>
                            <th>{{trans('base.test_status')}}</th>
                            <th>Trạng thái trò chuyện</th>
                            <th>{{trans('base.sent_day')}}</th>
                            <th>{{trans('base.action')}}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($records as $key=>$record)
                        <tr>
                            <td><span data-toggle="tooltip" data-html="true" title="{{$record->test->content}}">{{$record->test->name}}</span></td>
                            <td>
                                @if($record->video_link)
                                <a youtubeid="{{$record->getIdYoutube()}}" class="youtube-link">
                                    <img src="/uploads/play-all.png" width="50">
                                </a>
                                @elseif($record->audio_link)
                                <audio controls="">
                                    <source src="{!!$record->linkAudio()!!}" type="audio/mpeg">
                                </audio>
                                @endif
                            </td>
                            <td>{{$record->member->fullname}}</td>                        
                            <td>@if($record->member->trainer()->exists()){{$record->member->trainer->name}}@endif</td>
                            <td>{{$record->percent_point}}%</td>
                            <td title="{{$record->comment}}">{{$record->getComment()}}</td>
                            <td>{!!$record->message()!!}
                            </td>
                            <td>{{$record->getCreatedAt()}}</td>
                            <td>
                                <i class="icon-pencil7 position-right" data-trainer-id="{{$record->member->trainer()->exists()?$record->member->trainer->id:0}}" data-user-id="{{\Auth::user()->id}}" title="Chấm điểm" style="cursor:pointer" data-action="mark" data-id="{{$record->id}}"></i>
                                <a href="{{route('admin.member_test.detail', $record->member_id)}}"><i class="icon-history" title="Lịch sử"></i></a>
                                <a href="javascript:void(0)" data-action="toggle" data-id="{{$record->id}}" title="Nổi bật">
                                    @if(!$record->is_hot)
                                    <i class="icon-circle"></i>
                                    @else
                                    <i class="icon-circle2"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>
</div>
<div id="modal_form_vertical" class="modal fade" tabindex="-1">
</div>
<!-- /vertical form modal -->
@stop

