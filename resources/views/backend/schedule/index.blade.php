@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_schedule')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{!!route('admin.schedule.create')!!}" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-user-plus text-primary"></i><span>Thêm mới</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.schedule.index')!!}">{{trans('base.manage_schedule')}}</a></li>
            </ul>
        </div>
    </div>
    <div class="content">

        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_schedule')}}</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <table class="table datatable-basic">
                <thead>
                    <tr>
                        <th>{{trans('base.id')}}</th>
                        <th>{{trans('base.schedule_name')}}</th>
                        <th>{{trans('base.order')}}</th>
                        <th>{{trans('base.schedule')}}</th>
                        <th>{{trans('base.schedule_time')}}</th>
                        <th>{{trans('base.schedule_date')}}</th>
                        <th style="text-align: center">{{trans('base.update')}}</th>           
                        <th>{{trans('base.delete')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($schedule as $value)
                    <tr>
                        <td>{{$value->id}}</td>
                        <td>{{$value->name}}</td> 
                        <th>{{$value->order}}</th>
                        <td>
                            @foreach(explode(',', $value->schedule) as $schedule)
                            <span class="btn bg-teal-400 legitRipple">{{$weekdays[$schedule]}}</span>
                            @endforeach
                        </td>
                        <td><span class="btn bg-teal-400 legitRipple">{{$value->start_time}}</span> - <span class="btn bg-teal-400 legitRipple">{{$value->end_time}}</span></td>
                        <td><span class="btn bg-teal-400 legitRipple">{{date( "d/m/Y", strtotime($value->start_date))}}</span> - <span class="btn bg-teal-400 legitRipple">{{date( "d/m/Y", strtotime($value->end_date))}}</span></td>
                        <th style="text-align: center" >
                            <a  href="{!!route('admin.schedule.edit', $value->id)!!}" title="Cập nhật" class="text-success">
                                <i class="icon-pencil"></i>
                            </a>
                        </th>
                        <td>
                            <form action="{!! route('admin.schedule.destroy', $value->id) !!}" method="POST">
                                {!! method_field('DELETE') !!}
                                {!! csrf_field() !!}
                                <a title="{!! trans('backend/base.btn_delete') !!}" class="delete text-danger">
                                    <i class="icon-close2"></i>
                                </a>              
                            </form>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /basic datatable -->



    </div>

</div>
<!-- /dashboard content -->


@stop
