
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_schedule')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.schedule.index')!!}">{{trans('base.manage_schedule')}}</a></li>
            <li class="action">{{trans('base.update_schedule')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <form action="{!!route('admin.schedule.update', $schedule->id)!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> {{trans('base.update_schedule')}} </legend>
                        <div class="row">
                            <div class="form-group col-md-10">
                                <label class="required">{{trans('base.schedule_name')}}</label>
                                <input name="name" type="text" class="form-control" value="{!!is_null(old('name'))?$schedule->name:old('name')!!}">
                                {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                            </div> 
                            <div class="form-group col-md-2">
                                <label class="required">{{trans('base.order')}}</label>
                                <input name="order" type="number" class="form-control" value="{!!is_null(old('order'))?$schedule->order:old('order')!!}">
                                {!! $errors->first('order', '<span class="text-danger">:message</span>') !!}
                            </div> 
                        </div>
                        
                        <div class="form-group">
                            <label>{{trans('base.schedule')}}</label>
                            <select multiple="" class="select" name="schedule[]">
                                @foreach($weekdays as $key=>$weekday)
                                @if(is_null(old('schedule')))
                                <option value="{{$key}}" {{in_array($key, explode(',', $schedule->schedule))?'selected':''}}>{{$weekday}}</option>
                                @else
                                <option value="{{$key}}" {{in_array($key, old('schedule'))?'selected':''}}>{{$weekday}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div> 
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label class="required">{{trans('base.start_time')}}</label>
                                <input name="start_time" type="time" class="form-control" value="{!!is_null(old('start_time'))?$schedule->start_time:old('start_time')!!}">
                                {!! $errors->first('start_time', '<span class="text-danger">:message</span>') !!}
                            </div> 
                            <div class="form-group col-md-2">
                                <label class="required">{{trans('base.end_time')}}</label>
                                <input name="end_time" type="time" class="form-control" value="{!!is_null(old('end_time'))?$schedule->end_time:old('end_time')!!}">
                                {!! $errors->first('end_time', '<span class="text-danger">:message</span>') !!}
                            </div> 
                            <div class="form-group col-md-4">
                                <label class="required">{{trans('base.start_date')}}</label>
                                <input name="start_date" type="date" class="form-control" value="{!!is_null(old('start_date'))?$schedule->start_date:old('start_date')!!}">
                                {!! $errors->first('start_date', '<span class="text-danger">:message</span>') !!}
                            </div> 
                            <div class="form-group col-md-4">
                                <label class="required">{{trans('base.end_date')}}</label>
                                <input name="end_date" type="date" class="form-control" value="{!!is_null(old('end_date'))?$schedule->end_date:old('end_date')!!}">
                                {!! $errors->first('end_date', '<span class="text-danger">:message</span>') !!}
                            </div> 
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>
@stop
