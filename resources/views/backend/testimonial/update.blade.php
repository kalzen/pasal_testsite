
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_testimonial')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.testimonial.index')!!}">{{trans('base.manage_testimonial')}}</a></li>
            <li class="action">{{trans('base.create_testimonial')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <form action="{!!route('admin.testimonial.update', $testimonial->id)!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold required"><i class="icon-reading position-left"></i> {{trans('base.create_testimonial')}} </legend>
                        <div class="form-group">
                            <label>{{trans('base.name')}}</label>
                            <input name="name" type="text" class="form-control" value="{!!is_null(old('name'))?$testimonial->name:old('name')!!}">
                            {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label>{{trans('base.testimonial_title')}}</label>
                            <input name="title" type="text" class="form-control" value="{!!is_null(old('title'))?$testimonial->title:old('title')!!}">
                            {!! $errors->first('title', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="row">
                            <div class="col-md-4"> 
                                <div class="form-group">
                                    <label for="image">{{trans('base.testimonial_image')}}</label>
                                    <div class="thumb-container" data-field="image"></div>
                                    <input type="hidden" name="image" value="{!!is_null(old('image'))?$testimonial->image:old('image')!!}"/>
                                    <input type="hidden" id="image" />
                                    <div data-field="image" class="dropzone dropzone-single"></div>
                                    {!! $errors->first('image', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{trans('base.content')}}</label>
                                    <textarea name="content" rows="18" class="form-control wysihtml5-default">{!!is_null(old('content'))?$testimonial->content:old('content')!!}</textarea>
                                    {!! $errors->first('content', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>

        </div>
    </form>
</div>
@stop
