@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_affiliate')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{!!route('admin.affiliate.create')!!}" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Thêm mới</span></a>
                    <a id="button-active" href="#" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-checkmark-circle text-success"></i><span>Bật</span></a>
                    <a id="button-deactive"  href="#" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-cancel-circle2 text-dèault"></i><span>Tắt</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.affiliate.index')!!}">{{trans('base.manage_affiliate')}}</a></li>

            </ul>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Công cụ tìm kiếm<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form action="{{route('admin.affiliate.index')}}" method="GET">  
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="panel-body row">
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-3 control-label">Nhập từ:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="keyword" value="{!!isset($search['keyword'])?$search['keyword']:''!!}">
                            </div>                                                                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-3 control-label">Từ ngày:</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" name="start_date" value="{!!isset($search['start_date'])?$search['start_date']:''!!}">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label class="col-md-3 control-label">Đến ngày:</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" name="end_date" value="{!!isset($search['end_date'])?$search['end_date']:''!!}">
                            </div>
                        </div>  
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary legitRipple">Tìm kiếm <i class="icon-arrow-right14 position-right"></i></button>
                    </div>

                </div>
            </form>
        </div>
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_affiliate')}}</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>      
            <div class="table-responsive">
                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                <form action="{{route('admin.affiliate.toggleGroup')}}" method="POST" class="form-group">  
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input class="styled checked" id="checkall" type="checkbox" name="group[]" value="0">
                                </form>
                            </th>
                            <th>{{trans('base.name')}}</th>
                            <th>{{trans('base.code_link')}}</th>
                            <th>{{trans('base.description')}}</th>
                            <th>{{trans('base.created_at')}}</th>
                            <th>{{trans('base.sum_traffic')}}</th>
                            <th>{{trans('base.sum_data')}}</th>
                            <th>{{trans('base.conversion')}}</th>
                            <th>{{trans('base.control')}}</th>
                            <th>{{trans('base.delete')}}</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($affiliates as $key=>$affiliate)
                        <tr>
                            <th>{{$key+1}}</th>
                            <th><input class="check" type="checkbox" name="group[]" value="{{$affiliate->id}}"/></th>
                            <td>{{$affiliate->name}}</td>
                            <td><a href="{{$server_link.$affiliate->code}}">{{$server_link.$affiliate->code}}</a></td>
                            <td>{{$affiliate->description}}</td>
                            <td>{{$affiliate->created_at()}}</td>
                            <td>{{$affiliate->monthly->sum('count_traffic')}}</td>
                            <td>{{$affiliate->monthly->sum('count_data')}}</td>
                            @if($affiliate->monthly->sum('count_traffic')!=0)
                            <td>{{number_format($affiliate->monthly->sum('count_data')/$affiliate->monthly->sum('count_traffic')*100, 2).'%'}}</td>
                            @else
                            <td>0%</td>
                            @endif
                            <td class="text-center">
                                @if($affiliate->status==1)
                                <a href="{{route('admin.affiliate.toggle', $affiliate->id)}}" title="{{trans('base.approve')}}">
                                    <i class="icon-checkmark-circle text-success"></i>
                                </a>
                                @else
                                <a href="{{route('admin.affiliate.toggle', [$affiliate->id])}}" title="{{trans('base.approve')}}">
                                    <i class="icon-checkmark-circle text-default"></i>
                                </a>
                                @endif
                                <a href="{{route('admin.affiliate.edit', $affiliate->id)}}" title="{{trans('base.update')}}">
                                    <i class="icon-pencil5"></i>
                                </a>
                                <a onclick="copyToClipboard('{{$server_link.$affiliate->code}}')" title="{{trans('base.clipboard_link')}}">
                                    <i class="icon-link"></i>
                                </a>
                            </td>
                            <td>
                                <form action="{!! route('admin.affiliate.destroy', $affiliate->id) !!}" method="POST">
                                    {!! method_field('DELETE') !!}
                                    {!! csrf_field() !!}
                                    <a title="{!! trans('backend/base.delete') !!}" class="delete text-danger">
                                        <i class="icon-close2"></i>
                                    </a>              
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $('#button-active').click(function() {
    console.log('active');
    $('.form-group').append('<input type="hidden" name="status" value="1">');
    $('.form-group').submit();
    });
    $('#button-deactive').click(function() {
    console.log('deactive');
    $('.form-group').append('<input type="hidden" name="status" value="0">');
    $('.form-group').submit();
    });
</script>
@stop
