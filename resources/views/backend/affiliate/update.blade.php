
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_affiliate')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.affiliate.index')!!}">{{trans('base.manage_affiliate')}}</a></li>
            <li class="action">{{trans('base.update_affiliate')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <form action="{!!route('admin.affiliate.update', $affiliate->id)!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> {{trans('base.create_affiliate')}} </legend>
                        <div class="form-group">
                            <label class="required">{{trans('base.name')}}</label>
                            <input name="name" type="text" class="form-control" value="{!!!is_null(old('name'))?old('name'):$affiliate->name!!}">
                            {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="form-group">
                            <label class="required">{{trans('base.description')}}</label>
                            <textarea name="description" class="form-control">{!!!is_null(old('description'))?old('description'):$affiliate->description!!}</textarea>
                            {!! $errors->first('description', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="form-group">
                            <label class="required">{{trans('base.code')}}</label>
                            <input type="text" name="code" class="form-control" value="{!!!is_null(old('code'))?old('code'):$affiliate->code!!}">
                            {!! $errors->first('code', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label>{{trans('base.code_link')}}</label>: <a class="code_link" href="{!!$server_link.$affiliate->code!!}">{!!$server_link.$affiliate->code!!}</a>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>
@stop

@section('script')
@parent
<script>
    $('input[name=code]').change(function () {
        var code = $(this).val();
        var link = '{{$server_link}}';
        $('.code_link').html(link+code);
        $('.code_link').attr('href', link+code);
    });
</script>
@stop