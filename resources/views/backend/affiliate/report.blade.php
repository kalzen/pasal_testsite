@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_affiliate')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{!!route('admin.affiliate.create')!!}" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Thêm mới</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.affiliate.index')!!}">{{trans('base.manage_affiliate')}}</a></li>

            </ul>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Công cụ tìm kiếm<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form action="{{route('admin.affiliate.report')}}" method="GET">  
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="panel-body row">
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-3 control-label">Từ tháng:</label>
                            <div class="col-md-9">
                                <select class="select" name="start">
                                    <option value="">---Chọn tháng---</option>
                                    @if(isset($search['start']))
                                    @foreach($allMonthly as $value)
                                    <option value="{{$value->id}}" {{$value->id==$search['start']?'selected':''}}>{{$value->name}}</option>
                                    @endforeach
                                    @else
                                    @foreach($allMonthly as $key=>$value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label class="col-md-3 control-label">Đến tháng:</label>
                            <div class="col-md-9">
                                <select class="select" name="end">
                                    <option value="">---Chọn tháng---</option>
                                    @if(isset($search['end']))
                                    @foreach($allMonthly as $value)
                                    <option value="{{$value->id}}" {{$value->id==$search['end']?'selected':''}}>{{$value->name}}</option>
                                    @endforeach
                                    @else
                                    @foreach($allMonthly as $key=>$value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>          
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary legitRipple">Filter <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_affiliate')}}</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div> 
            <div class="table-overflow">
                <table class="table table-striped table-bordered">
                    <thead class="text-center">
                        <tr>
                            <th rowspan="2">{{trans('base.name')}}</th>
                            @foreach($monthly as $mkey=>$month)
                            <th colspan="3" class="text-center">{{$month}}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($monthly as $month)
                            <th>{{trans('base.sum_traffic')}}</th>
                            <th>{{trans('base.sum_data')}}</th>
                            <th>{{trans('base.conversion')}}</th> 
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($affiliates as $key=>$affiliate)
                        <tr>
                            <th>{{$affiliate->name}}</th>
                            @foreach($monthly as $mkey=>$month)
                            @if(!is_null($affiliate->monthly($mkey)))
                            <th>{{$affiliate->monthly($mkey)->count_traffic}}</th>
                            <th>{{$affiliate->monthly($mkey)->count_data}}</th>
                            <th>{{number_format($affiliate->monthly($mkey)->count_data/$affiliate->monthly($mkey)->count_traffic*100, 2)}}%</th>
                            @else
                            <th></th>
                            <th></th>
                            <th></th>
                            @endif
                            @endforeach   
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>
</div>
@stop
