@section('style')
@parent
<!--Thêm file style--> 
<style>
    .dataTables_filter{
        display: none;
    }
    label{
        padding: 9px!important;
    }
</style>
@stop

@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.report_customer')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.customer.chart')!!}">{{trans('base.report_customer')}}</a></li>

            </ul>
        </div>
    </div>
    <div class="content">

        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.report_customer')}}</h5>
            </div>    
            <canvas id="myChart" >

            </canvas>

        </div>
        <!-- /basic datatable -->
    </div>
</div>
@stop
@section('script')
@parent
<script type="text/javascript" src="{{asset('assets/js/plugins/chart/chart.min.js')}}"></script>
<script>

</script>
<script>
    console.log({!!$monthly!!});
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: {
            labels: [{!!$monthly!!}],
            datasets: [{!!$data!!}]
        },
        // Configuration options go here
        options: {}
    });
</script>
@stop