@extends('backend.layouts.auth')
@section('content')
<!-- Advanced login -->
<form action="{!! route('postLogin') !!}" method="post">
    <div class="panel panel-body login-form">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="text-center">
            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
            <h5 class="content-group">Login to your account <small class="display-block">Your credentials</small></h5>
        </div>

        <div class="form-group has-feedback has-feedback-left">
            <input name="username" type="text" class="form-control" placeholder="Username">
            <div class="form-control-feedback">
                <i class="icon-user text-muted"></i>
            </div>
        </div>

        <div class="form-group has-feedback has-feedback-left">
            <input name="password" type="password" class="form-control" placeholder="Password">
            <div class="form-control-feedback">
                <i class="icon-lock2 text-muted"></i>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn bg-pink-400 btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
        </div>

        @if(Session::has('error'))
        <div class='alert alert-danger'>
            <p>{!! Session::get('error') !!}</p>                       
        </div>
        @endif   
    </div>
</form>
<!-- /advanced login -->

@stop

@section('script')
@parent
<script type="text/javascript" src="{!!asset('assets/backend/js/pages/login.js')!!}"></script>
@stop