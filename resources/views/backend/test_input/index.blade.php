@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Nhập bài test</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a data-toggle="modal" data-target="#modal-create" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Tạo mới bài tập</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="#">Nhập bài test</a></li>
            </ul>
        </div>
    </div>
    <form action="{!!route('admin.input_test.post')!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="content">
            <div class="panel panel-body results">
                <div class="row">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Nhập thông tin khách hàng</legend>
                        <div class="form-group col-md-4">
                            <label class="required">{{trans('base.fullname')}}</label>
                            <input name="fullname" type="text" class="form-control" value="{!!old('fullname')!!}">
                            {!! $errors->first('fullname', '<span class="text-danger">:message</span>') !!}
                        </div>  
                        <div class="form-group col-md-4">
                            <label class="required">{{trans('base.phone')}}</label>
                            <input name="tel" type="text" class="form-control" value="{!!old('tel')!!}">
                            {!! $errors->first('tel', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="form-group col-md-4">
                            <label class="required">{{trans('base.email')}}</label>
                            <input name="email" type="email" class="form-control" value="{!!old('email')!!}">
                            {!! $errors->first('email', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="form-group col-md-6">
                            <label class="required" for="province_id">Tỉnh thành:</label>
                            <select name="province_id" class="form-control custom-select province-value" required>
                                <option value="" selected>- Lựa chọn tỉnh thành -</option>
                                @foreach($provinces as $record)
                                <option value="{{$record->id}}">{{$record->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('province_id', '<span class="text-danger">:message</span>') !!}
                        </div>
                         <div class="form-group col-md-6">
                            <div class="required"><label for="basis_id"><span>Chọn cơ sở Pasal gần nhất:</span></label></div>
                            <select name="basis_id" class="form-control custom-select" required>
                                <option value="" selected>- Lựa chọn cơ sở gần bạn nhất -</option>
                                @foreach($basis as $record)
                                <option class="province province-{{$record->province_id}}" value="{{$record->id}}">{{$record->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('basis_id', '<span class="text-danger">:message</span>') !!}
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="panel panel-body results">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Nhập kết quả Phần 1 Listening </legend>
                            @foreach($phase1->tests as $key=>$test)                          
                            <div class="list-question question-{{++$key}}">
                                <h3>{{$key}}. {{$test->content}} </h3>                  
                                <div class="list-listening">
                                    <div class="row row-eq-height">
                                        @foreach($test->getChildren as $ckey=>$children)
                                        <div class="col-md-12">
                                            <div class="a-question">
                                                <p class="question">{{$key}}.{{++$ckey}}. {!!$children->content!!} </p>            
                                                @foreach($children->answers as $akey=>$record)    
                                                <div class="col-md-3">
                                                    <label class="a-row row-inline" data-question="{!!$children->id!!}">
                                                        <input type="radio" name="question[1][{{$children->id}}]" class="styled" {!!$akey==0?'checked':''!!} value="{{$record->id}}">
                                                        {{$record->content}}
                                                    </label>
                                                </div>
                                                @endforeach                                     
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </fieldset>
                    </div>
                </div>                
            </div>
            <div class="panel panel-body results">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Nhập kết quả Phần 2 Reading </legend>
                            @foreach($phase2->tests as $key=>$test)                          
                            <div class="list-question question-{{++$key}}">
                                <h3>{{$key}}. {{$test->content}} </h3>                  
                                <div class="list-listening">
                                    <div class="row row-eq-height">
                                        @foreach($test->getChildren as $ckey=>$children)
                                        <div class="{{$test->layout==1?'col-md-6':'col-md-12'}}">
                                            <div class="a-question">
                                                <p class="question">{{$key}}.{{++$ckey}}. {!!$children->content!!} </p>
                                                <form action="">
                                                    @foreach($children->answers as $akey=>$record)
                                                    @if($test->layout==2)
                                                    <div class="col-md-3">
                                                        <label class="a-row row-inline" data-question="{!!$children->id!!}">
                                                            <input type="radio" name="question[2][{{$children->id}}]" class="styled" {!!$akey==0?'checked':''!!} value="{{$record->id}}">
                                                            {{$record->content}}
                                                        </label>
                                                    </div>
                                                    @else
                                                    <label class="radio-inline" data-question="{!!$children->id!!}">
                                                        <input type="radio" name="question[2][{{$children->id}}]" class="styled" {!!$akey==0?'checked':''!!} value="{{$record->id}}">
                                                        {{$record->content}}
                                                    </label>
                                                    @endif
                                                    @endforeach                                  
                                                </form>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </fieldset>
                    </div>
                </div>                
            </div>
            <div class="panel panel-body results">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Nhập kết quả Phần 3 writting </legend>
                            @foreach($phase3->tests as $key=>$test)
                            @if($test->type==1)
                            <div class="list-question question-{{++$key}}">  
                                @foreach($test->getChildren as $ckey=>$children)
                                <h3>{{$key}}. {!!$children->content!!} </h3>
                                @foreach($children->answers as $akey=>$record)
                                <div class="list-listening">
                                    <div class="row row-eq-height">
                                        <div class="{{$test->layout==1?'col-md-6':'col-md-12'}}">
                                            <div class="a-question">
                                                <p class="question">{{$key}}.{{++$ckey}}. {!!$children->content!!} </p>
                                                @foreach($record->random_content() as $value)
                                                <button type="button" class="btn btn-pasal" data-value='{{$value}}'>{!!$value!!}</button>
                                                @endforeach
                                                <button type="button" class="btn btn-danger btn-re-pasal">Chọn lại</button>
                                                <input type="text" name="question[3][{{$record->id}}]" data-question='{{$record->id}}'  class="form-control form-pasal" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach                 
                            </div>
                            @endforeach
                            @else
                            <div class="list-question question-{{++$key}}">
                                <h3>{{$key}}. {{$test->content}} </h3>
                                <div class="list-listening">
                                    <div class="row row-eq-height">
                                        @foreach($test->getChildren as $ckey=>$children)
                                        <div class="{{$test->layout==1?'col-md-6':'col-md-12'}}">
                                            <div class="a-question">
                                                <p class="question">{{$key}}.{{++$ckey}}. {!!$children->content!!} </p>
                                                @foreach($children->answers as $akey=>$record)
                                                @if($test->layout==2)
                                                <div class="col-md-3">
                                                    <label class="a-row row-inline" data-question="{!!$children->id!!}">
                                                        <input type="radio" name="question[3][{{$children->id}}]" class="styled" {!!$akey==0?'checked':''!!} value="{{$record->id}}">
                                                        {{$record->content}}
                                                    </label>
                                                </div>
                                                @else
                                                <label class="a-row row-inline" data-question="{!!$children->id!!}">
                                                    <input type="radio" name="question[3][{{$children->id}}]" class="styled" {!!$akey==0?'checked':''!!} value="{{$record->id}}">
                                                    {{$record->content}}
                                                </label>
                                                @endif
                                                @endforeach                                  
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </fieldset>
                    </div>
                </div>                
            </div>
            <div class="panel panel-body results">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Nhập kết quả Phần 4 Speak </legend>
                            @foreach($phase4->tests as $key=>$test)
                            @foreach($test->getChildren as $ckey=>$children)
                            <div class="list-question question-{{++$key}}">
                                <h4 class="col-md-12">{{$key}}.{{++$ckey}}. {!!$children->name!!}</h4>             
                                <div class="list-listening">
                                    <div class="row">
                                        @foreach($children->answers as $akey=>$record)
                                        <div class="{{$children->layout==1?'col-md-6':'col-md-12'}}">
                                            <div class="input-group mrbt-15">
                                                <span class="input-group-addon">{{$ckey}}.{{++$akey}}. {!!$record->content!!}</span>
                                                <div class="fl-right mr-right-50">
                                                    <input name="answer[{{$children->id}}][{{$record->id}}]" type="number" class="form-control wd-70px" min="1" max="10">
                                                    <span class="input-group-addon">/10</span>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endforeach
                        </fieldset>
                    </div>
                </div>                
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>

@stop
@section('script')
<script type="text/javascript">
    $('.btn-pasal').click(function () {
        $(this).attr('disabled', 'true');
        var value = $(this).data('value');
        var old = $(this).parent().find('input').val();

        if (old.length == 0) {
            $(this).parent().find('input').val(value);
        } else {
            $(this).parent().find('input').val(old + ' ' + value);
        }

    });
    $('.btn-re-pasal').click(function () {
        $(this).parent().find('.btn-pasal').removeAttr('disabled');
        $(this).parent().find('input').val('');
    });
    $('.province-value').change(function () {
        if ($(this).val() != 0) {
            $('.province').hide();
            $('.province-' + $(this).val()).show();
        } else {
            $('.province').hide();
            $('select[name=basis_id]').val('');
        }
    });
</script>
@stop