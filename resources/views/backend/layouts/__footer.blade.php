
<footer class="main-footer">
    <div class="footer text-muted">
        <p>Copyright <i class="fa fa-copyright"></i> 2018 Pasal English Education - Phát triển bởi Phòng Công nghệ Pasal</p>
    </div>
</footer>
<script type="text/javascript" src="{!!asset('assets/js/custom.js')!!}"></script>
<script type="text/javascript">
function openKCFinder(field) {
    window.KCFinder = {
        callBack: function(url) {
            field.value = url;
            window.KCFinder = null;
            $(field).val(url);
            }
        }
    window.open('/public/kcfinder/browse.php?type=files&dir=files/public', 'kcfinder_textbox',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=800, height=600'
    );
}
</script>
<script>
//    // Default initialization
//    $(".styled, .multiselect-container input").uniform({
//        radioClass: 'choice'
//    });
    $('.delete').click(function () {
    confirm('{!!trans('base.arlet')!!}') ? $(this).parent().submit() : false;
    });
    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
        orderable: false,
                width: '100px',
                targets: [ 5 ]
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
        search: '<span>' + '{!!trans('base.search')!!}' + ':</span> _INPUT_',
                lengthMenu: '{!!trans('base.show')!!}' + ': _MENU_',
                paginate: { 'first': 'Trang đầu', 'last': 'Trang cuối', 'next': '&rarr;', 'previous': '&larr;' },
                info: "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục"
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice( - 3).find('.dropdown, .btn-group').addClass('dropup');
            $(".styled").uniform();
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice( - 3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    $(".youtube-link").grtyoutube({ theme: "dark",autoPlay:'false' });
    $( ".datepicker" ).datepicker();
    // Basic datatable
    $('.datatable-basic').DataTable();
    $('.select').select2({
    minimumResultsForSearch: Infinity,
            containerCssClass: 'border-default'
    });
    
    $('#checkall').change(function() {
        $(this).removeClass('checked');
        $('.group').remove();
        if ($(this).is(":checked")) {
            $('.check').each(function() {
                $(this).parents('span').addClass("checked");
            });
        } else{  
            $('.check').each(function() {
                $(this).parents('span').removeClass("checked");
            });   
        }
    });
    $(document).on('click', '.paginate_button', function () {
        if ($('#checkall').is(":checked")) {
           $('.check').each(function() {
                $(this).prop("checked", true);
            }); 
        }
    });
    
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(element).select();
        document.execCommand("copy");
        $temp.remove();
    }
    $('[data-toggle="tooltip"]').tooltip();
    
</script>