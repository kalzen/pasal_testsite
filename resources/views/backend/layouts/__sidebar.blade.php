<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    <a href="#" class="legitRipple"><img src="{{asset('assets/images/placeholder.jpg')}}" class="img-circle img-responsive" alt=""></a>
                    <h6>{!!Auth::user()->name!!}</h6>
                    <span class="text-size-small">{!!Auth::user()->address!!}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse" class="legitRipple"><span>{{trans('base.account')}}</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li><a href="{{route('logout')}}" class="legitRipple"><i class="icon-switch2"></i> <span>Logout</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>{{trans('base.manage')}} {{trans('base.system')}}</span> <i class="icon-menu" title="" data-original-title="Main pages"></i></li>
                    @if(Auth::user()->role_id == 1)
                    <li class="active"><a href="{{route('admin.index')}}" class="legitRipple"><i class="icon-home4"></i> <span>{{trans('base.system')}}</span></a></li>
                    <li><a href="{{route('admin.config.index')}}" class="legitRipple"><i class="icon-gear"></i> <span>{{trans('base.config')}}</span></a></li>
                    <li><a href="{{route('admin.user.index')}}" class="legitRipple"><i class="icon-user"></i> <span>{{trans('base.manage')}} {{trans('base.user')}}</span></a></li>
                    <li class="{!!strpos(\Request::route()->getName(), 'admin.basis') !== false || strpos(\Request::route()->getName(), 'admin.schedule') !== false?'active':''!!}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-insert-template"></i> <span>{{trans('base.manage')}} {{trans('base.form_register')}}</span></a>
                        <ul class="{!!strpos(\Request::route()->getName(), 'admin.basis') !== false || strpos(\Request::route()->getName(), 'admin.schedule') !== false?'':'hidden-ul'!!}">
                            <li><a href="{{route('admin.basis.index')}}" class="legitRipple">Quản lý option cơ sở</a></li>
                            <li><a href="{{route('admin.province.index')}}" class="legitRipple">Quản lý tỉnh thành</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(!in_array(Auth::user()->role_id,[3,4]))
                    <li class="{!!strpos(\Request::route()->getName(), 'admin.block') !== false?'active':''!!}">
                        <a href="#" class="has-ul legitRipple">
                            <i class="icon-stack2"></i> <span>{{trans('base.manage')}} {{trans('base.content_website')}}</span>
                        </a>
                        <ul class="{!!strpos(\Request::route()->getName(), 'admin.block') !== false?'':'hidden-ul'!!}">
                            <li><a href="{{route('admin.block.index')}}" class="legitRipple">Quản lý block nội dung</a></li>
                            <!--<li><a href="" class="legitRipple">Quản lý câu chuyện thành công của học viên</a></li>
                            <li><a href="" class="legitRipple">Quản lý banner</a></li>
                            <li><a href="" class="legitRipple">Quản lý video</a></li>
                            <li><a href="" class="legitRipple">Quản lý footer</a></li>-->
                        </ul>
                    </li>
                    @endif
                    @if(!in_array(Auth::user()->role_id,[2,4]))
                    <li class="{!!strpos(\Request::route()->getName(), 'admin.trainer') !== false || strpos(\Request::route()->getName(), 'admin.member') !== false?'active':''!!}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-user"></i> <span>Quản lý thành viên</span></a>
                        <ul class="{!!strpos(\Request::route()->getName(), 'admin.trainer') !== false || strpos(\Request::route()->getName(), 'admin.member') !== false?'':'hidden-ul'!!}">
                            <li><a href="{!!route('admin.trainer.index')!!}" class="legitRipple">Danh sách giáo viên</a></li>           
                            <li><a href="{!!route('admin.member.index')!!}" class="legitRipple">Danh sách thành viên</a></li>                
                        </ul>
                    </li>
                    @endif
                    @if(!in_array(Auth::user()->role_id,[2,3]))
                    <li class="{!!strpos(\Request::route()->getName(), 'admin.affiliate') !== false?'active':''!!}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-stats-growth"></i> <span>{{trans('base.link_affiliate')}}</span></a>
                        <ul class="{!!strpos(\Request::route()->getName(), 'admin.affiliate') !== false?'':'hidden-ul'!!}">
                            <li><a href="{{route('admin.affiliate.index')}}" class="legitRipple">{{trans('base.manage')}} {{trans('base.link_affiliate')}}</a></li>
                            <li><a href="{{route('admin.affiliate.report')}}" class="legitRipple">{{trans('base.report')}} {{trans('base.link_affiliate')}}</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(in_array(Auth::user()->role_id,[1,2,4]))
                    <li class="{!!strpos(\Request::route()->getName(), 'admin.test') !== false || strpos(\Request::route()->getName(), 'admin.rank') !== false?'active':''!!}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-flip-horizontal2"></i> <span>{{trans('base.manage_test')}}</span></a>
                        <ul class="{!!strpos(\Request::route()->getName(), 'admin.test')!== false || strpos(\Request::route()->getName(), 'admin.rank') !== false?'':'hidden-ul'!!}">
                            @foreach($share_phase as $phase)
                            <li><a href="{!!route('admin.test.index', ['phase'=>$phase->id])!!}" class="legitRipple"><span>{{$phase->name}}</span></a></li>
                            @endforeach
                        </ul>
                    </li>
                    @endif 
                    @if(!in_array(Auth::user()->role_id,[2,4]))
                    <li class="{!!strpos(\Request::route()->getName(), 'admin.input_test') !== false?'active':''!!}">
                        <a href="{!!route('admin.input_test.get')!!}" class="has-ul legitRipple"><i class="icon-flip-horizontal2"></i> <span>Nhập kết quả</span></a> 
                    </li>
                    @endif 
                    <!-- /main -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>