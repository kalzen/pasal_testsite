@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.user')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{!!route('admin.user.create')!!}" id="button-export" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Thêm tài khoản</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.user.index')!!}">{{trans('base.user')}}</a></li>
            </ul>
        </div>
    </div>
    <div class="content">

        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_user')}}</h5> 
            </div>   
            <div class="table-responsive">
                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>{{trans('base.id')}}</th>
                            <th>{{trans('base.username')}}</th>
                            <th>{{trans('base.name')}}</th>
                            <th>{{trans('base.role')}}</th>
                            <th>{{trans('base.manage_test')}}</th>
                            <th>{{trans('base.created_at')}}</th>
                            <th>{{trans('base.control')}}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->username}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->role->name}}</td>
                            <td>
                                @if($user->member()->exists())
                                <a href="{{route('admin.member.index', $user->id)}}" title="Báo cáo danh sách thành viên tham gia thử thách được lọc tìm kiếm theo user quản lý tương ứng"> 
                                    <i class="icon-stack-text"></i> ({{$user->member()->count()}})
                                </a>
                                @else
                                <i class="icon-stack-empty"></i> (-)
                                @endif
                            </td>
                            <td>{{$user->created_at()}}</td>
                            <td class="text-center">
                                <a href="{{route('admin.user.edit', $user->id)}}" title="{!! trans('base.edit') !!}" class="success"><i class="icon-pencil"></i></a>   
                                <form action="{!! route('admin.user.destroy', $user->id) !!}" method="POST" style="display: inline-block">
                                    {!! method_field('DELETE') !!}
                                    {!! csrf_field() !!}
                                    <a title="{!! trans('base.delete') !!}" class="delete text-danger">
                                        <i class="icon-close2"></i>
                                    </a>              
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>
</div>
@stop

