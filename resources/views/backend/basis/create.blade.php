
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_basis')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.basis.index')!!}">{{trans('base.manage_basis')}}</a></li>
            <li class="action">{{trans('base.create_basis')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    <form action="{!!route('admin.basis.store')!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> {{trans('base.create_basis')}} </legend>
                        <div class="form-group">
                            <label class="required">{{trans('base.basis_name')}}</label>
                            <input name="name" type="text" class="form-control" value="{!!old('name')!!}">
                            {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div>  
                        <div class="form-group">
                            <label class="required">{{trans('base.basis_code')}}</label>
                            <input name="code" type="text" class="form-control" value="{!!old('code')!!}">
                            {!! $errors->first('code', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="form-group">
                            <label class="required">{{trans('base.order')}}</label>
                            <input name="order" type="number" class="form-control" value="{!!old('order')!!}">
                            {!! $errors->first('order', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="form-group">
                            <label class="required">{{trans('base.description')}}</label>
                            <textarea name="description" rows="18" class="form-control wysihtml5-default">{!!old('description')!!}</textarea>
                            {!! $errors->first('description', '<span class="text-danger">:message</span>') !!}
                        </div> 
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>
@stop
