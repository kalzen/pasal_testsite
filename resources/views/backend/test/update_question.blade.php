@extends('backend.layouts.master')
@section('content')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_test')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.test.index', ['phase'=>$phase_id])!!}">{{trans('base.manage_test')}}</a></li>
            <li class="action">{{trans('base.update_test')}}</li>
        </ul>
    </div>
</div>
<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Danh sách bài tập</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <form action="{!!route('admin.test.question.update', ['id'=>$record->id])!!}" method="POST">

                {{ csrf_field() }}
                <input type="hidden" name="phase_id" value="{{$phase_id}}"/>
                <div class="group">
                    <label class="control-label col-md-3 required">Tên câu hỏi:</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="name" value="{{$record->name}}"/>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Chọn bài tập:</label>
                    <div class="col-md-9">
                        <select class="form-control" name="parent_id">
                            @foreach($arr_phase as $key=>$phase)
                            <option value="{!!$key!!}" {{$record->parent_id==$key?'selected':''}}>{{$phase}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Số thứ tự:</label>
                    <div class="col-md-9">
                        <input class="form-control col-md-9" type="number" name="order" value="{{$record->order}}"/>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Thang điểm</label>
                    <div class="col-md-9">
                        <input class="form-control col-md-9" type="number" name="point" value="{{$record->point}}"/>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Nội dung bài test:</label>
                    <div class="col-md-9">
                        <textarea rows="5" class="form-control" name="content">{!!$record->content!!}</textarea>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Tải file bài tập:</label>
                    <div class="col-md-9">
                        <input id="file" type="text" name="file" readonly class="form-control" value="{!!$record->file!!}"/>
                        <input type="file" id="image" onclick="openKCFinder('#file')" class="file-styled-primary">
                    </div>
                </div> 
                @if($phase_id==4)
                <div class="group">
                    <label class="control-label col-md-3 required">Bố cục bài test:</label>
                    <div class="col-md-9">
                        <select name="layout" class="form-control">
                            <option value="1" {!!$record->layout==1?'selected':''!!}>Nửa màn hình</option>
                            <option value="2" {!!$record->layout==2?'selected':''!!}>Toàn màn hình</option>  
                            <option value="3" {!!$record->layout==3?'selected':''!!}>1/6 màn hình</option>
                        </select>
                    </div>
                </div>
                @endif
                @if($phase_id==3)
                <label class="radio-inline">
                    <input type="radio" name="type" class="styled" value="0" {{$record->type==0?'checked':''}} disabled>
                    Trắc nghiệm
                </label>

                <label class="radio-inline">
                    <input type="radio" name="type" class="styled" value="1" {{$record->type==1?'checked':''}} disabled>
                    Sắp xếp từ
                </label>
                
                @endif
                <hr>
                <h5>Tích vào chọn nếu là đáp án đúng</h5>
                <div class="content-answer">
                    @foreach($record->answers as $akey=>$answer)
                    <div class="group group-question">
                        <label class="control-label col-md-3 required">Đáp án {{++$akey}}:</label>
                        <div class="col-md-9">
                            <div class="input-group wd-100">
                                <input type="text" class="form-control" name='content_question[{{$akey}}]' value="{{$answer->content}}">
                                
                                <span class="input-group-addon">
                                    <input type="checkbox" class="styled" name="status_question[{{$akey}}]" value="{{$akey}}" {{$answer->status==1?'checked':''}}>
                                </span>
                                
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <button id="add-answer" type="button" class="btn bg-teal-400">Thêm đáp án <i class="icon-plus-circle2 position-right"></i></button>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>

</div>
@stop
