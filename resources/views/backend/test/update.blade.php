
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_test')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.test.index', ['phase'=>$phase])!!}">{{trans('base.manage_test')}}</a></li>
            <li class="action">{{trans('base.update_test')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Danh sách bài tập</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <form action="{!!route('admin.test.update', ['id'=>$record->id])!!}" method="POST">
                {{ csrf_field() }}
                <div class="group">
                    <label class="control-label col-md-3 required">Tên bài tập:</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="name" value="{!!$record->name!!}"/>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Số thứ tự:</label>
                    <div class="col-md-9">
                        <input class="form-control col-md-9" type="number" name="order" value="{!!$record->order!!}"/>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Nội dung bài test:</label>
                    <div class="col-md-9">
                        <textarea rows="10" class="form-control" name="content">{!!$record->content!!}</textarea>
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Tải file bài tập:</label>
                    <div class="col-md-9">
                        <input id="file" type="text" name="file" readonly class="form-control" value="{!!$record->file!!}"/>
                        <input type="file" id="image" onclick="openKCFinder('#file')" class="file-styled-primary">
                    </div>
                </div>
                <div class="group">
                    <label class="control-label col-md-3 required">Bố cục bài test:</label>
                    <div class="col-md-9">
                        <select name="layout" class="form-control">
                            <option value="1" {!!$record->layout==1?'selected':''!!}>Nửa màn hình</option>
                            <option value="2" {!!$record->layout==2?'selected':''!!}>Toàn màn hình</option>        
                        </select>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>
    </div>

</div>
@stop
