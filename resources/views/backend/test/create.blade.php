
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_test')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.test.index')!!}">{{trans('base.manage_test')}}</a></li>
            <li class="action">{{trans('base.create_test')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    <form action="{!!route('admin.test.store')!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold required"><i class="icon-reading position-left"></i> {{trans('base.create_test')}} </legend>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="required">{{trans('base.name')}}</label>
                                    <input required name="name" type="text" class="form-control" value="{!!old('name')!!}">
                                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="required">{{trans('base.order')}}</label>
                                    <input name="order" type="number" class="form-control" value="{!!old('order')!!}">
                                    {!! $errors->first('order', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-md-4"> 
                                <div class="form-group">
                                    <label>{{trans('base.video_url')}}</label>
                                    <input name="video_url" type="url" class="form-control" value="{!!old('video_url')!!}">
                                    {!! $errors->first('video_url', '<span class="text-danger">:message</span>') !!}
                                </div>
                                <div class="form-group has-feedback">
                                    <label>{{trans('base.percent_point')}}</label>
                                    <input name="percent_point" type="number" class="form-control" value="{!!old('percent_point')!!}">
                                    <div class="form-control-feedback">%</div>
                                    {!! $errors->first('percent_point', '<span class="text-danger">:message</span>') !!}
                                </div>
                                <div class="form-group" data-field="icon_url">
                                    <label for="icon_url">{{trans('base.icon_url')}}</label>
                                    <input type="file" name="icon_url" class="file-input-overwrite" data-field="icon_url">
                                    {!! $errors->first('icon_url', '<span class="text-danger">:message</span>') !!}
                                </div>
                                <div class="form-group">
                                    <label class="display-block text-semibold">Có quà tặng hoàn thành</label>
                                    <label class="radio-inline">
                                        <div class="choice"><span class="checked"><input type="radio" name="has_gift" class="styled" checked="checked"></span></div>
                                        Có
                                    </label>
                                    <label class="radio-inline">
                                        <div class="choice"><span><input type="radio" name="has_gift" class="styled"></span></div>
                                        Không
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('base.gift')}}</label>
                                    <textarea name="gift" rows="4" class="form-control">{!!old('gift')!!}</textarea>
                                    {!! $errors->first('gift', '<span class="text-danger">:message</span>') !!}
                                </div>                                
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{trans('base.content')}}</label>
                                    <textarea name="content" rows="15" class="form-control wysihtml5-default">{!!old('content')!!}</textarea>
                                    {!! $errors->first('content', '<span class="text-danger">:message</span>') !!}
                                </div> 
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>
@stop
