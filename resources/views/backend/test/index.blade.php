@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản lý bài tập</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a data-toggle="modal" data-target="#modal-create" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Tạo mới bài tập</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.test.index', ['phase'=>$phase])!!}">Quản lý bài tập</a></li>
            </ul>
        </div>
    </div>
    <div class="content">

        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-solid nav-tabs-component">
                        <li class="active"><a>Danh sách bài tập</a></li>
                        <li><a href="{!!route('admin.test.list', ['phase'=>$phase])!!}">Danh sách câu hỏi</a></li>
                        <li><a href="{!!route('admin.rank.index', ['phase'=>$phase])!!}">Quy tắc xếp hạng</a></li>
                    </ul>
                </div>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="table-responsive">
                        <table class="table datatable-basic">
                            <thead>
                                <tr>
                                    <th>
                                        <form method="POST" class="form-group">  
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input class="styled" id="checkall" type="checkbox" name="group[]" value="0"/>
                                        </form>
                                    </th>
                                    <th>{{trans('base.id')}}</th>
                                    <th>Tên câu hỏi</th>
                                    <th>Thứ tự</th>
                                    <th>Số câu hỏi con</th>
                                    <th>Thang điểm</th>       
                                    <th>{{trans('base.action')}}</th>                           
                                </tr>
                            </thead>
                            <tbody>
                                @if($records->count()>0)
                                @foreach($records as $key=>$record)
                                <tr>
                                    <td><input class="check styled" type="checkbox" name="group[]" value="{{$record->id}}"/></td>
                                    <td>{!!$record->id!!}</td>
                                    <td>{!!$record->name!!}</td>
                                    <td>{!!$record->order!!}</td>
                                    <td><a href="{!!route('admin.test.list', ['phase'=>$phase, 'parent_id'=>$record->id])!!}">({!!$record->getChildren()->count()!!})</a></td>
                                    <td>{!!$record->getChildren()->sum('point')!!}</td>
                                    <td>
                                        <a  href="{!!route('admin.test.edit', $record->id)!!}" title="Cập nhật" class="text-success">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <div class="delete" style="display: inline-block">
                                            <form action="{!! route('admin.test.destroy', $record->id) !!}" method="POST">
                                                {!! method_field('DELETE') !!}
                                                {!! csrf_field() !!}
                                                <a title="{!! trans('backend/base.btn_delete') !!}" class="delete text-danger">
                                                    <i class="icon-close2"></i>
                                                </a>              
                                            </form>  
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>

</div>

<!-- Modal -->
<div id="modal-create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form action="{!!route('admin.test.store')!!}" method="POST">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tạo mới bài tập</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="phase_id" value="{{$phase}}"/>
                    <input type="hidden" name="parent_id" value="0"/>
                    <div class="group">
                        <label class="control-label col-md-3 required">Tên bài tập:</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="name" />
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Số thứ tự:</label>
                        <div class="col-md-9">
                            <input class="form-control col-md-9" type="number" name="order" />
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Nội dung bài test:</label>
                        <div class="col-md-9">
                            <textarea rows="8" class="form-control" name="content"></textarea>
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Tải file bài tập:</label>
                        <div class="col-md-9">
                            <input id="file" type="text" name="file" readonly class="form-control"/>
                            <input type="file" id="image" onclick="openKCFinder('#file')" class="file-styled-primary">
                        </div>
                    </div>     
                    <div class="group">
                        <label class="control-label col-md-3 required">Bố cục bài test:</label>
                        <div class="col-md-9">
                            <select name="layout" class="form-control">
                                <option value="1">Nửa màn hình</option>
                                <option value="2">Toàn màn hình</option>        
                            </select>
                        </div>
                    </div>
                    @if($phase==3)
                    <label class="radio-inline">
                        <input type="radio" name="type" class="styled" value="0">
                        Trắc nghiệm
                    </label>

                    <label class="radio-inline">
                        <input type="radio" name="type" class="styled" value="1">
                        Sắp xếp từ
                    </label>

                    @endif
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>
<!-- /dashboard content -->


@stop
