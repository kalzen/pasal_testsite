@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản lý bài tập</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a data-toggle="modal" data-target="#modal-create" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-plus-circle2 text-primary"></i><span>Tạo mới câu hỏi</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.test.index', ['phase'=>$phase])!!}">Quản lý bài tập</a></li>
            </ul>
        </div>
    </div>
    <div class="content">

        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-solid nav-tabs-component">
                        <li><a href="{!!route('admin.test.index', ['phase'=>$phase])!!}">Danh sách bài tập</a></li>
                        <li class="active"><a href="{!!route('admin.test.list', ['phase'=>$phase])!!}">Danh sách câu hỏi</a></li>
                        <li><a href="{!!route('admin.rank.index', ['phase'=>$phase])!!}">Quy tắc xếp hạng</a></li>
                    </ul>
                </div>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="table-responsive">
                        <table class="table datatable-basic">
                            <thead>
                                <tr>
                                    <th>
                                        <form method="POST" class="form-group">  
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input class="styled" id="checkall" type="checkbox" name="group[]" value="0"/>
                                        </form>
                                    </th>
                                    <th>{{trans('base.id')}}</th>
                                    <th>Tên câu hỏi</th>
                                    <th>Thứ tự</th>
                                    <th>Bài tập</th>
                                    <th>Thang điểm</th>       
                                    <th>{{trans('base.action')}}</th>                           
                                </tr>
                            </thead>
                            <tbody>
                                @if($records->count()>0)
                                @foreach($records as $key=>$record)
                                <tr>
                                    <td><input class="check styled" type="checkbox" name="group[]" value="{{$record->id}}"/></td>
                                    <td>{!!$record->id!!}</td>
                                    <td>{!!$record->name!!}</td>
                                    <td>{!!$record->order!!}</td>
                                    <td>{!!$record->getParent()!==null?$record->getParent()->name:''!!}</td>
                                    <td>{!!$record->point!!}</td>
                                    <td>
                                        <a  href="{!!route('admin.test.question.edit', $record->id)!!}" title="Cập nhật" class="text-success">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <div class="delete" style="display: inline-block">
                                            <form action="{!! route('admin.test.destroy', $record->id) !!}" method="POST">
                                                {!! method_field('DELETE') !!}
                                                {!! csrf_field() !!}
                                                <a title="{!! trans('backend/base.btn_delete') !!}" class="delete text-danger">
                                                    <i class="icon-close2"></i>
                                                </a>              
                                            </form>  
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>

</div>

<!-- Modal -->
<div id="modal-create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form action="{!!route('admin.test.store')!!}" method="POST">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tạo mới câu hỏi</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="phase_id" value="{{$phase}}"/>
                    <div class="group">
                        <label class="control-label col-md-3 required">Tên câu hỏi:</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="name" />
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Chọn bài tập:</label>
                        <div class="col-md-9">
                            <select class="form-control" name="parent_id">
                                @foreach($arr_phase as $key=>$record)
                                <option value="{!!$key!!}">{{$record}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Số thứ tự:</label>
                        <div class="col-md-9">
                            <input class="form-control col-md-9" type="number" name="order" />
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Thang điểm</label>
                        <div class="col-md-9">
                            <input class="form-control col-md-9" type="number" name="point" />
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Nội dung bài test:</label>
                        <div class="col-md-9">
                            <textarea rows="5" class="form-control" name="content"></textarea>
                        </div>
                    </div>
                    <div class="group">
                        <label class="control-label col-md-3 required">Tải file bài tập:</label>
                        <div class="col-md-9">
                            <input id="file" type="text" name="file" readonly class="form-control"/>
                            <input type="file" id="image" onclick="openKCFinder('#file')" class="file-styled-primary">
                        </div>
                    </div>
                    @if($phase==4)
                    <div class="group">
                        <label class="control-label col-md-3 required">Bố cục bài test:</label>
                        <div class="col-md-9">
                            <select name="layout" class="form-control">
                                <option value="1" >Nửa màn hình</option>
                                <option value="2" >Toàn màn hình</option>  
                                <option value="3" >1/6 màn hình</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    <hr>
                    <h5>Tích vào chọn nếu là đáp án đúng</h5>
                    <div class="content-answer">
                        <div class="group group-question">
                            <label class="control-label col-md-3 required">Đáp án 1:</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" name='content_question[0]'>
                                    <span class="input-group-addon">
                                        <input type="checkbox" class="styled" name="status_question[0]" value="1">
                                    </span>

                                </div>
                            </div>
                        </div>
                        <div class="group group-question">
                            <label class="control-label col-md-3 required">Đáp án 2:</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" name='content_question[1]'>
                                    <span class="input-group-addon">
                                        <input type="checkbox" class="styled" name="status_question[1]" value="2">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button id="add-answer" type="button" class="btn bg-teal-400">Thêm đáp án <i class="icon-plus-circle2 position-right"></i></button>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>
<!-- /dashboard content -->


@stop
