
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_block')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.block.index')!!}">{{trans('base.manage_block')}}</a></li>
            <li class="action">{{trans('base.create_block')}}</li>
        </ul>
    </div>
</div>

<div class="content">
    <form action="{!!route('admin.block.store')!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold required"><i class="icon-reading position-left"></i> {{trans('base.create_block')}} </legend>
                        <p class="content-group">
                            Có các block sau: <code>slide</code>, <code>about-us</code>, <code>statistics</code>, <code>faq</code> (chứa block con), <code>features</code> (chứa block con), <code>brochure</code> (chứa block con), <code>counters</code> (chứa block con).
                        </p>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="required">{{trans('base.name')}}</label>
                                    <input required name="name" type="text" class="form-control" value="{!!old('name')!!}">
                                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('base.position')}}</label>
                                    <input name="position" type="text" class="form-control" value="{!!old('position')!!}">
                                    {!! $errors->first('position', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="parent_id">{{trans('base.block_parent')}}</label>
                                    <select name="parent_id" id="parent_id" class="select form-control">
                                        <option value="0">{{trans('base.choose')}}</option>
                                        @foreach($parent_list as $parent_item)
                                        <option value="{{$parent_item->id}}" {{$parent_item->id==old('parent_id')?'selected':''}}>{{$parent_item->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('parent_id', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('base.video_url')}}</label>
                                    <input name="video_url" type="url" class="form-control" value="{!!old('video_url')!!}">
                                    {!! $errors->first('video_url', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required">{{trans('base.order')}}</label>
                                    <input name="order" type="number" class="form-control" value="{!!old('order')!!}">
                                    {!! $errors->first('order', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">                 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tag</label>
                                    <input name="tag" type="text" class="form-control" value="{!!old('tag')!!}">
                                    {!! $errors->first('tag', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required">Tên vị trí</label>
                                    <input name="position_title" type="text" class="form-control" value="{!!old('position_title')!!}">
                                    {!! $errors->first('position_title', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"> 
                                <div class="form-group" data-field="icon_url">
                                    <label for="icon_url">{{trans('base.icon_url')}}</label>
                                    <input type="file" name="icon_url" class="file-input-overwrite" data-field="icon_url">
                                    {!! $errors->first('icon_url', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{trans('base.content')}}</label>
                                    <textarea id="create_editor_blog" name="content" rows="18" class="form-control">{!!old('content')!!}</textarea>
                                    {!! $errors->first('content', '<span class="text-danger">:message</span>') !!}
                                </div> 
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>
@stop
@section('script')
<script type="text/javascript">
    CKEDITOR.replace('create_editor_blog', {
    });
</script>
@stop