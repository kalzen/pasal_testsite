
@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_member')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}} </a></li>
            <li><a href="{!!route('admin.member.index')!!}"> {{trans('base.manage_member')}}</a></li>

        </ul>
    </div>
</div>
<div class="content">
    <form action="{!!route('admin.member.store')!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> {{trans('base.create')}}</legend>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="required">{{trans('base.email')}}</label>
                                <input name="email" type="text" class="form-control" value="{!!old('email')!!}">
                                {!! $errors->first('email', '<span class="text-danger">:message</span>') !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label class="required">{{trans('base.fullname')}}</label>
                                <input name="fullname" type="text" class="form-control" value="{!!old('fullname')!!}">
                                {!! $errors->first('fullname', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-md-6"> 
                                <div class="form-group" data-field="avatar">
                                    <label for="avatar">{{trans('base.avatar')}}</label>
                                    <input type="file" name="avatar" class="file-input-overwrite" data-field="avatar">
                                    {!! $errors->first('avatar', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required">{{trans('base.trainer')}}</label>
                                    <select name="trainer_id" class="form-control select">
                                        @foreach($trainer as $val)
                                        <option value="{{$val->id}}" {{$val->id==old('trainer_id')?'selected':''}}>{{$val->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('trainer_id', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="required">{{trans('base.phone')}}</label>
                                <input name="phone" type="phone" class="form-control" value="{!!old('phone')!!}">
                                {!! $errors->first('phone', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>{{trans('base.address')}}</label>
                                <input name="address" type="address" class="form-control" value="{!!old('address')!!}">                             
                            </div>
                            <div class="form-group col-md-6">
                                <label>{{trans('base.social_id')}}</label>
                                <input name="provider_id" type="provider_id" class="form-control" value="{!!old('provider_id')!!}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="required">{{trans('base.password')}}</label>
                                <input name="password" type="password" class="form-control" value="{!!old('password')!!}">
                                {!! $errors->first('password', '<span class="text-danger">:message</span>') !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label class="required">{{trans('base.password_confirm')}}</label>
                                <input name="password_confirmation" type="password" class="form-control" value="{!!old('password_confirmation')!!}">
                                {!! $errors->first('password_confirmation', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>
                    </fieldset>
                </div>


            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">{{trans('base.submit')}} <i class="icon-arrow-right14 position-right"></i></button>
            </div>


        </div>
    </form>
</div>
@stop

