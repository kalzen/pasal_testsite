@extends('backend.layouts.master')
@section('content')
<!-- Dashboard content -->
<div class="row">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_user')}}</span></h4>
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="javascript:void(0)" id="selectMember" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-user-check text-primary"></i><span>Chọn giáo viên</span></a>
                    @if(\Auth::user()->role_id == \App\User::ROLE_SUPERADMIN)<a href="javascript:void(0)" id="switchUser" class="btn btn-link btn-float text-size-small has-text legitRipple"><i class="icon-user-plus text-primary"></i><span>Chuyển quản lý</span></a>@endif
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i>{{trans('base.system')}}</a></li>
                <li><a href="{!!route('admin.member.index')!!}">{{trans('base.manage_user')}}</a></li>
            </ul>
        </div>
    </div>
    <div class="content">
        @if (Session::has('success'))
        <div class="alert bg-success alert-styled-left">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">{{ Session::get('success') }}</span>
        </div>
        @endif
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Công cụ tìm kiếm<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form action="{{route('admin.member.index')}}" method="GET">  
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-3 control-label" style="margin-top:10px">Lọc theo cơ sở:</label>
                                <div class="col-md-9 form-group">
                                    <select multiple="" class="select" name="basis[]" data-placeholder="Chọn cơ sở">
                                        @if(isset($search['basis']))
                                        @foreach($basis as $key=>$value)
                                        <option value="{{$value->id}}" {{in_array($value->id, $search['basis'])?'selected':''}}>{{$value->name}}</option>
                                        @endforeach
                                        @else
                                        @foreach($basis as $key=>$value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-3 control-label" style="margin-top:10px">Lọc theo tỉnh thành:</label>
                                <div class="col-md-9 form-group">
                                    <select multiple="" class="select" name="province" data-placeholder="Chọn tỉnh thành">
                                        @if(isset($search['province']))
                                        @foreach($provinces as $value)
                                        <option value="{{$value->id}}" {{$value->id == $search['province']?'selected':''}}>{{$value->name}}</option>
                                        @endforeach
                                        @else
                                        @foreach($provinces as $key=>$value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-3 control-label" style="margin-top:10px">Giảng viên chấm điểm:</label>
                                <div class="col-md-9 form-group">
                                    <select multiple="" class="select" name="teacher" data-placeholder="Chọn giáo viên">
                                        @if(isset($search['teacher']))
                                        @foreach($teachers as $value)
                                        <option value="{{$value->id}}" {{$value->id == $search['teacher']?'selected':''}}>{{$value->name}}</option>
                                        @endforeach
                                        @else
                                        @foreach($teachers as $key=>$value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-3 control-label" style="margin-top:10px">Lịch hẹn speaking hôm nay:</label>
                                <div class="col-md-9 form-group lh40">
                                    @if(isset($search['now_appointment']))
                                    <input type="checkbox" class="styled" name="now_appointment" value="1" {!!$search['now_appointment']==1?'checked':''!!}/>
                                    @else
                                    <input type="checkbox" class="styled" name="now_appointment" value="1"/>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-2 control-label" style="margin-top:10px">Lọc theo ngày test:</label>
                                <div class="col-md-3 form-group">
                                    <input class="form-control datepicker" data-date-format='dd-mm-yy' type="text" name="start_date" value="{!!isset($search['start_date'])?$search['start_date']:''!!}"/>
                                </div>
                                <div class="col-md-1 text-center lh40">---------</div>
                                <div class="col-md-3 form-group">
                                    <input class="form-control datepicker"  data-date-format='dd-mm-yy' type="text" name="end_date" value="{!!isset($search['end_date'])?$search['end_date']:''!!}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-2 control-label" style="margin-top:10px">Lọc theo ngày đăng ký:</label>
                                <div class="col-md-3 form-group">
                                    <input class="form-control datepicker" data-date-format='dd-mm-yy' type="text" name="register_start_date" value="{!!isset($search['register_start_date'])?$search['register_start_date']:''!!}"/>
                                </div>
                                <div class="col-md-1 text-center lh40">---------</div>
                                <div class="col-md-3 form-group">
                                    <input class="form-control datepicker"  data-date-format='dd-mm-yy' type="text" name="register_end_date" value="{!!isset($search['register_end_date'])?$search['register_end_date']:''!!}"/>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary legitRipple">Tìm kiếm <i class="icon-arrow-right14 position-right"></i></button>
                    </div>

                </div>
            </form>
        </div>
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('base.list_member')}}</h5> 
            </div>   
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                <form method="POST" class="form-group">  
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input class="styled" id="checkall" type="checkbox" name="group[]" value="0"/>
                                </form>
                            </th>

                            <th>{{trans('base.fullname')}}</th>
                            <th>{{trans('base.phone')}}</th>
                            <th>{{trans('base.email')}}</th>
                            <th>{{trans('base.province')}}</th>
                            <th>{{trans('base.basis')}}</th>
                            <th>GV test</th>
                            <th>Điểm trung bình</th>
                            <th>Điểm Listening</th>
                            <th>Điểm Reading</th>
                            <th>Điểm Writing</th>
                            <th>Lịch hẹn Speaking</th>
                            <th>Điểm Speaking</th>
                            <th>Link đăng ký</th>
                            <th>Tên link</th>
                            <th>{{trans('base.control')}}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if($members->count()>0)
                        @foreach($members as $key=>$member)
                        <tr>
                            <td>{{++$key}}</td>
                            <td><input class="check styled" type="checkbox" name="group[]" value="{{$member->id}}"/></td>

                            <td>{{$member->fullname}}</td>
                            <td>{{$member->tel}}</td>
                            <td>{{$member->email}}</td>
                            <td>{{$member->province->name}}</td>
                            <td>{{$member->basis->code}}</td>
                            <td>{{$member->user->name}}</td>
                            <td>@if($member->result!==null){{number_format(($member->result->point('phase_1')+$member->result->point('phase_2')+$member->result->point('phase_3')+$member->result->pointPhase4())/$total_point*100)}}%@endif</td>
                            <td>@if($member->result!==null){{$member->result->point('phase_1')}}/{{$member->result->totalPoint(1)}}@endif</td>
                            <td>@if($member->result!==null){{$member->result->point('phase_2')}}/{{$member->result->totalPoint(2)}}@endif</td>
                            <td>@if($member->result!==null){{$member->result->point('phase_3')}}/{{$member->result->totalPoint(3)}}@endif</td>
                            <td>{{$member->appointment!==null?date('d-m-Y H:i', strtotime($member->appointment)):''}}</td>
                            <td>
                                @if($member->result!==null)
                                @if($member->result->phase_4==null)
                                <i class="icon-pencil7 popup-phase text-info-800" data-customer="{{$member->id}}"></i>
                                @else
                                <a class='popup-phase' data-customer="{{$member->id}}">{{$member->result->pointPhase4()}}/{{$member->result->totalPoint(4)}}</a>
                                @endif
                                @endif
                            </td>
                            <td>{{$member->link_register}}</td>
                            <td>{{$member->link_name}}</td>
                            <td>
                                @if($member->result!==null)
                                <a href="{!!route('admin.member.export',['tel'=>$member->tel])!!}"><i class="icon-download"></i></a>
                                <a href="{!!route('admin.member.sendmail',['tel'=>$member->tel])!!}"><i class="text-purple icon-envelop5"></i></a>
                                @else
                                <i class="icon-download text-grey-300"></i>
                                <i class="icon-envelop5 text-grey-300"></i>
                                @endif
                                <a href="{!!route('admin.member.update',['tel'=>$member->id])!!}"><i class="text-success icon-pencil7"></i></a>
                                <form action="{!! route('admin.member.destroy', $member->id) !!}" method="POST" style="display: inline-block">
                                    {!! method_field('DELETE') !!}
                                    {!! csrf_field() !!}
                                    <a title="{!! trans('base.delete') !!}" class="delete text-danger">
                                        <i class="icon-close2"></i>
                                    </a>              
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="15" class="text-right">{!!$members->appends($search)->links()!!}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /basic datatable -->
    </div>
</div>

<div class="modal fade" tabindex="-1" id="switchUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Chuyển quản lý mới</h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 0.8;font-size: 30px">&times;</button>
            </div>
            <form action="{{route('admin.member.switch')}}" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <input class="hidden" name="member_ids" value="">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <select class="select" name="role_id" id="role_id">
                                    <option value="0">Chọn nhóm quản trị viên</option>
                                    @foreach($roles as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-6">
                                <select class="select" name="trainer_id" id="trainer_id">
                                    <option>Chọn quản lý mới</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="updatePhase4">
    <div class="modal-dialog wd-50">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">Chấm điểm speaking</h6>
            </div>
            <div class="modal-body">
                <div id="form-update-result" class="form-group">
                </div>

                
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">

    $(document).on('click', '.popup-phase', function(e) {
        var customer_id = $(this).data('customer');
        getResult(customer_id);
    });
    function getResult(customer_id) {
        $.ajax({
            url: '/api/getUpdatePhase4',
            method: 'GET',
            data: {
                customer_id: customer_id,
            },
            success: function (html) {
                $('#form-update-result').html(html);
                $('#updatePhase4').modal('toggle');
            }
        });
    }
    ;
</script>
@stop
