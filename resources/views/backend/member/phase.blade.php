<form action="{{route('admin.result.update')}}" method="post">
    <div class="text-center">
        <h6 class="text-semibold">Khách hàng:</h6>{{$customer->fullname}}
        <h6 class="text-semibold">Số điện thoại:</h6>{{$customer->tel}}
        <h6 class="text-semibold">Lịch hẹn:</h6>{{date('d-m-Y H:i', strtotime($customer->appointment))}}
    </div>
    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
    <input class="hidden" name="customer_id" value="{{$customer->id}}">
    <hr>
    <div class="row">
        @foreach($phase->tests as $key=>$test)
        @foreach($test->getChildren as $ckey=>$children)
        <div class="list-question question-{{++$key}}">
            <h4 class="col-md-12">{{$key}}.{{++$ckey}}. {!!$children->name!!}</h4>             
            <div class="list-listening">
                <div class="row">
                    @foreach($children->answers as $akey=>$record)
                    <div class="{{$children->layout==1?'col-md-6':'col-md-12'}}">
                        <div class="input-group mrbt-15">
                            <span class="input-group-addon">{{$ckey}}.{{++$akey}}. {!!$record->content!!}</span>
                            <div class="fl-right mr-right-50">
                                @if($phse_4_answer!==null)
                                <input name="answer[{{$children->id}}][{{$record->id}}]" value="{!!$phse_4_answer[$record->id]!!}" type="number" class="form-control wd-70px" min="1" max="10">
                                @else
                                <input name="answer[{{$children->id}}][{{$record->id}}]" type="number" class="form-control wd-70px" min="1" max="10">
                                @endif
                                <span class="input-group-addon">/10</span>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        @endforeach
        @endforeach
    </div>
    <hr>
    <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save changes</button>
    </div>
</form>