@extends('backend.layouts.master')
@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{trans('base.manage_member')}}</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a>
        </div>

    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{!!route('admin.index')!!}"><i class="icon-home2 position-left"></i> {{trans('base.system')}}</a></li>
            <li><a href="{!!route('admin.member.index')!!}">{{trans('base.manage_member')}}</a></li>
        </ul>
    </div>
</div>

<div class="content">
    @if (Session::has('success'))
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">{{ Session::get('success') }}</span>
    </div>
    @endif
    <form action="{!!route('admin.member.update', $member->id)!!}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <input type="hidden" name="role_id" value="{!!$member->role_id!!}" />
        <div class="panel panel-body results">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Cập nhật</legend>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label required">{{trans('base.email')}}</label>
                                <input name="email" type="text" class="form-control" value="{!!!is_null(old('email'))?old('email'):$member->email!!}">
                                {!! $errors->first('email', '<span class="text-danger">:message</span>') !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label required">{!!trans('base.fullname')!!}</label>
                                <input name="fullname" type="text" class="form-control" value="{!!!is_null(old('fullname'))?old('fullname'):$member->fullname!!}">
                                {!! $errors->first('fullname', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>   
                        <div class="row">                                   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label required">{{trans('base.trainer')}}</label>
                                    <select name="user_id" class="form-control select">
                                        @foreach($trainer as $val)
                                        <option value="{{$val->id}}" {{$val->id==$member->user_id?'selected':''}}>{{$val->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('user_id', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label required">{{trans('base.phone')}}</label>
                                <input name="tel" type="phone" class="form-control" value="{!!!is_null(old('tel'))?old('tel'):$member->tel!!}">
                                {!! $errors->first('tel', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label">Tỉnh thành</label>
                                <select name="province_id" class="form-control custom-select province-value" required>
                                    <option value="" selected>- Lựa chọn tỉnh thành -</option>
                                    @foreach($provinces as $record)
                                    <option value="{{$record->id}}" {!!$member->province_id==$record->id?'selected':''!!}>{{$record->name}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('province_id', '<span class="text-danger">:message</span>') !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="basis_id"><span>Chọn cơ sở Pasal gần nhất*:</span></label>
                                <select name="basis_id" class="form-control custom-select" required>
                                    <option value="" selected>- Lựa chọn cơ sở gần bạn nhất -</option>
                                    @foreach($basis as $record)
                                    <option class="province province-{{$record->province_id}}" value="{{$record->id}}" {!!$member->basis_id==$record->id?'selected':''!!}>{{$record->name}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('basis_id', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>                       
                    </fieldset>
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </form>
</div>
@stop

