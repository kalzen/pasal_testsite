@extends('frontend.layout.master')
@section('style')
@parent
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/ketqua.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
@endsection
@section('content')
@if (Session::has('success'))
<div class="alert bg-success alert-styled-left">
    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
    <span class="text-semibold">{{ Session::get('success') }}</span>
</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="top-result">
                Giao tiếp tiếng Anh trôi chảy với <b>Pronunciation Workshop & Effortless English ĐỘC QUYỀN</b>
            </div>         
        </div>
        <div class="content-header">
            <div class="col-md-4  col-sm-4 text-right">
                <p><span>Kết quả bài test</span><br>kiểm tra năng lực tiếng Anh tại Pasal</p>
            </div>
            <div class="col-md-4 col-sm-4 tongdiem text-center">
                <p>Tổng điểm</p>
                <p>{{$result->avPoint()}}</p>
            </div>
            <div class="col-md-4 col-sm-4 text-left">
                <p>{{$customer->fullname}}</p>
                <p>Ngày test: {{date('d/m/Y', strtotime($result->created_at))}}</p>
            </div>
        </div>

    </div>
    <hr>
    <div class="main-ketqua">
        <div class="tongquan">
            <h3>Tổng quan 4 kỹ năng</h3>
            <div class="row">
                <div class="col-md-2 col-sm-0"></div>
                <div class="col-md-2 col-sm-3">
                    <div class="circle text-center" data-value="{{number_format($result->point('phase_1')/$result->totalPoint(1)*100)}}">
                        <p class="diemso">{{number_format($result->point('phase_1')/$result->totalPoint(1)*100)}}</p>
                        <p>Listening</p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3">
                    <div class="circle text-center" data-value="{{number_format($result->point('phase_2')/$result->totalPoint(2)*100)}}">
                        <p class="diemso">{{number_format($result->point('phase_2')/$result->totalPoint(2)*100)}}</p>
                        <p>Reading</p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3">
                    <div class="circle text-center" data-value="{{number_format($result->point('phase_3')/$result->totalPoint(3)*100)}}">
                        <p class="diemso">{{number_format($result->point('phase_3')/$result->totalPoint(3)*100)}}</p>
                        <p>Writing</p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3">
                    <div class="circle text-center" data-value="{{number_format($result->pointPhase4()/$result->totalPoint(4)*100)}}">
                        <p class="diemso">{{number_format($result->pointPhase4()/$result->totalPoint(4)*100)}}</p>
                        <p>Speaking</p>
                    </div>
                </div>

                <div class="col-md-2 col-sm-0"></div>
            </div>
        </div>
    </div>
    <hr>
    <div class="main-ketqua">

        <div class="phantich">
            <h3>Phân tích chi tiết</h3>
            <div class="block">
                <div class="row">
                    <div class="col-md-4">
                        <div class="phase">
                            <img src="{!!asset('assets/images/frontend/listening-icon.png')!!}">
                            <p class="name-part">Listening</p>
                            <div class="phase_point">
                                Kết quả điểm: 
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{$result->point('phase_1')/$result->totalPoint(1)*100}}%;" aria-valuenow="{{$result->point('phase_1')/$result->totalPoint(1)*100}}" aria-valuemin="0" aria-valuemax="100">{{$result->point('phase_1')}}</div>
                                </div>
                            </div>
                            <div class="phase_point">
                                Trình độ:
                                <div class="trinhdo">
                                    {{$result->getRank(1)!==null?$result->getRank(1)->name:''}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p class="vande-title">Vấn đề của bạn: </p>
                        {!!$result->getRank(1)!==null?$result->getRank(1)->content:'<br><br><br><br>'!!}
                        <p class="giaiphap-title">Giải pháp học:</p>
                        {!!$result->getRank(1)!==null?$result->getRank(1)->solution:'<br><br><br><br>'!!}
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="row">
                    <div class="col-md-4">
                        <div class="phase">
                            <img src="{!!asset('assets/images/frontend/reading-icon.png')!!}">
                            <p class="name-part">Reading</p>
                            <div class="phase_point">
                                Kết quả điểm: 
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{$result->point('phase_2')/$result->totalPoint(2)*100}}%;" aria-valuenow="{{$result->point('phase_2')/$result->totalPoint(2)*100}}" aria-valuemin="0" aria-valuemax="100">{{$result->point('phase_2')}}</div>
                                </div>
                            </div>
                            <div class="phase_point">
                                Trình độ:
                                <div class="trinhdo">{{$result->getRank(2)!==null?$result->getRank(2)->name:''}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p class="vande-title">Vấn đề của bạn: </p>
                        {!!$result->getRank(2)!==null?$result->getRank(2)->content:'<br><br><br><br>'!!}
                        <p class="giaiphap-title">Giải pháp học:</p>
                        {!!$result->getRank(2)!==null?$result->getRank(2)->solution:'<br><br><br><br>'!!}
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="row">
                    <div class="col-md-4">
                        <div class="phase">
                            <img src="{!!asset('assets/images/frontend/writing-icon.png')!!}">
                            <p class="name-part">Writing</p>
                            <div class="phase_point">
                                Kết quả điểm: 
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{$result->point('phase_3')/$result->totalPoint(3)*100}}%;" aria-valuenow="{{$result->point('phase_3')/$result->totalPoint(3)*100}}" aria-valuemin="0" aria-valuemax="100">{{$result->point('phase_3')}}</div>
                                </div>
                            </div>
                            <div class="phase_point">
                                Trình độ:
                                <div class="trinhdo">{{$result->getRank(3)!==null?$result->getRank(3)->name:''}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p class="vande-title">Vấn đề của bạn: </p>
                        {!!$result->getRank(3)!==null?$result->getRank(3)->content:'<br><br><br><br>'!!}
                        <p class="giaiphap-title">Giải pháp học:</p>
                        {!!$result->getRank(3)!==null?$result->getRank(3)->solution:'<br><br><br><br>'!!}
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="row">
                    <div class="col-md-4">
                        <div class="phase">
                            <img src="{!!asset('assets/images/frontend/speaking-icon.png')!!}">
                            <p class="name-part">Speaking</p>
                            <div class="phase_point">
                                Kết quả điểm: 
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{$result->pointPhase4()/$result->totalPoint(4)*100}}%;" aria-valuenow="{{$result->pointPhase4()/$result->totalPoint(4)*100}}" aria-valuemin="0" aria-valuemax="100">{{$result->pointPhase4()}}</div>
                                </div>
                            </div>
                            <div class="phase_point">
                                Trình độ:
                                <div class="trinhdo">{{$result->getRank(4)!==null?$result->getRank(4)->name:''}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p class="vande-title">Vấn đề của bạn: </p>
                        {!!$result->getRank(4)!==null?$result->getRank(4)->content:'<br><br><br><br>'!!}
                        <p class="giaiphap-title">Giải pháp học:</p>
                        {!!$result->getRank(4)!==null?$result->getRank(4)->solution:'<br><br><br><br>'!!}
                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr>
    <div class="footer-kq text-center">
        <a href="{!!route('admin.member.export',['tel'=>$tel])!!}">
            <button class="download"><i class="fa fa-download"></i>
                Tải xuống
            </button>
        </a>
        <a href="{!!route('admin.member.sendmail',['tel'=>$tel])!!}">
            <button class="email">
                <i class="fa fa-send"></i>Gửi email cho tôi
            </button>
        </a>

    </div>
</div>


@stop
@section('script')
@parent
<script type="text/javascript" src="{!!asset('assets/js/frontend/circle-progress.min.js')!!}"></script>
<script>
$('.circle').each(function () {
    var value = $(this).data('value');
    if (value === 100) {
        var val = 1;
    } else {
        var val = '0.' + value;
    }
    if (value >= 0 && value < 25) {
        var color_1 = '#ff1e41';
        var color_2 = '#ff5f43';
    } else if (value >= 25 && value < 50) {
        var color_1 = '#92288f';
        var color_2 = '#dc443d';
    } else if (value >= 50 && value < 75) {
        var color_1 = '#ff896a';
        var color_2 = '#ffb774';
    } else {
        var color_1 = '#63d5a4';
        var color_2 = '#9ddb6e';
    }
    $(this).circleProgress({
        value: val,
        size: 144,
        fill: {gradient: [color_1, color_2]}
    });
})
</script>

@stop