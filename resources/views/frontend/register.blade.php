@extends('frontend.layout.master')
@section('style')
@parent
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
@endsection
@section('content')
<div id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="item-1">
                    <img src="{{asset('assets/images/frontend/text-banner.png')}}" alt="Pasal">
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="item-2 text-center">
                    <img src="{{asset('assets/images/frontend/banner-img.png')}}" alt="Pasal">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="main">
    <div class="container">
        <div class="box box-info">
            <div class="row">

                <div class="col-md-6">
                    <div class="note">
                        <div class="title-note">
                            <h2>Đăng ký test<br> lần đầu</h2>
                            <span class="free">Miễn phí</span>
                        </div>
                        <p>
                            Nếu bạn tham gia làm bài test lần đầu,  vui lòng nhập thông tin cá nhân bao gồm họ tên, email, số điện thoại để tiếp tục tham gia bài test. Pasal cam kết bảo mật thông tin cá nhân của bạn và không chia sẻ cho một bên thứ 3 bất kỳ.
                        </p>
                        <p>
                            Kết quả bài test sẽ được gửi tới bạn qua email và Giảng viên của Pasal sẽ liên hệ với bạn qua số điện thoại bạn cung cấp để test trình độ Speaking của bạn. Vì vậy, bạn lưu ý hãy nhập thông tin chính xác nhé !
                        </p>
                    </div>

                </div>
                <div class="col-md-6">
                    <form action="{{route('register.info')}}" method="POST">
                        {!!csrf_field()!!}
                        <input type="hidden" name="link_register" value="{{$link_register}}"/>
                        <div class="form-group">
                            <label for="hoten">Họ và tên*:</label>
                            <input type="text" id="hoten" name="fullname" placeholder="Nguyễn Văn A" class="form-control" required>
                            {!! $errors->first('fullname', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label for="sodienthoai">Số điện thoại*:</label>
                            <input type="text" id="sodienthoai" name="tel" placeholder="099999999" class="form-control" required>
                            {!! $errors->first('tel', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label for="email">Email*:</label>
                            <input type="text" id="email" name="email" placeholder="nguyenvana@gmail.com" class="form-control" required>
                            {!! $errors->first('email', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <label for="province_id">Tỉnh thành*:</label>
                            <select name="province_id" class="form-control custom-select province-value" required>
                                <option value="" selected>- Lựa chọn tỉnh thành -</option>
                                @foreach($provinces as $record)
                                <option value="{{$record->id}}">{{$record->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('province_id', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            <div class="coso">
                                <label for="basis_id"><span>Chọn cơ sở Pasal gần nhất*:</span></label>
                            </div>
                            <select name="basis_id" class="form-control custom-select" required>
                                <option value="" selected>- Lựa chọn cơ sở gần bạn nhất -</option>
                                @foreach($basis as $record)
                                <option class="province province-{{$record->province_id}}" value="{{$record->id}}">{{$record->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('basis_id', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="box-bottom form-bottom">
                            <p>* Click vào nút <span>tiếp tục</span> để bắt đầu nhé!</p>
                            <button class="btn" type="submit">Tiếp tục</button>
                        </div>

                    </form>
                </div>
            </div>

        </div>

        <div class="result">
            <div class="title">
                <h2 class="text-center">Xem lại kết quả test gần nhất</h2>
                <p class="text-center">Nếu bạn đã tham gia làm bài test trước đó và đã hoàn thành phần test Speaking với Giảng viên qua điện thoại,
                    vui lòng nhập số điện thoại đăng ký của bạn để xem kết quả test cuối cùng:
                </p>
            </div>

            <form class="text-center" action="{{route('result')}}" method="GET">
                <label class="text-center" for="sdt">Nhập số điện thoại*:</label><br>
                <input type="text" class="sdt text-center form-control" id="sdt" name="tel" placeholder="- nhập chính xác số điện thoại bạn đã đăng ký trước đó -" required>
                <div class="box-bottom text-center">
                    <button class="btn" type="submit">Tra cứu kết quả</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
