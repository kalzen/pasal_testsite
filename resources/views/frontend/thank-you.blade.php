@extends('frontend.layout.master')
@section('style')
@parent
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
@endsection
@section('content')
<div id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="item-1">
                    <img src="{{asset('assets/images/frontend/text-banner.png')}}" alt="Pasal">
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="item-2 text-center">
                    <img src="{{asset('assets/images/frontend/banner-img.png')}}" alt="Pasal">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="main">
    <div class="container">
        <div class="box box-home">
            <div class="title">
                <h2 class="text-center">Cảm ơn bạn đã tham gia bài test chúng tôi sẽ liên hệ sớm để thực hiện phần SPEAKING</h2>             
            </div>      
            <br><br><br>
            <div class="box-bottom text-center">
                @if(session('tel'))
                <a class="btn" href="{!!route('result', ['tel'=>session('tel')])!!}">Xem trước kết quả</a>
                @else
                <a class="btn" href="{!!route('register')!!}">Tra cứu kết quả</a>
                @endif
            </div>
            <br><br>
        </div>
    </div>
</div>
@stop
