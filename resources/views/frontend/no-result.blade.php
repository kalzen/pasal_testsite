@extends('frontend.layout.master')
@section('style')
@parent
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
@endsection
@section('content')
<div id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="item-1">
                    <img src="{{asset('assets/images/frontend/text-banner.png')}}" alt="Pasal">
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="item-2 text-center">
                    <img src="{{asset('assets/images/frontend/banner-img.png')}}" alt="Pasal">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="main">
    <div class="container">
        <div class="box box-home">
            <div class="title">
                <h2 class="text-center">Số điện thoại bạn tra cứu không tồn tại</h2>   
                <div class="text-center">
                     <a href="{!!route('register')!!}">Quay trở lại tra cứu</a>
                </div>
               
            </div>       
        </div>
    </div>
</div>
@stop
