@extends('frontend.layout.master')
@section('style')
@parent
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
@endsection
@section('content')
<h3>{{session('result-'.$phase->id)}}</h3>
<div id="main">
    <input id="next-request" type="hidden" value="{!!$next_rq!!}" />
    <div class="container">
        <div class="box">
            <div class="title">
                <div class="tieude">
                    <h3>{!!$phase->name!!}</h3>
                </div>
                <div class="count">
                    <p>Question <span class="nth-question">1</span>/{{count($phase->tests)}}</p>                
                </div>
            </div>
            @foreach($phase->tests as $key=>$test)
            @if($test->type==1 && $phase->id==3)
            <div class="list-question question-{{++$key}}">  
                @foreach($test->getChildren as $ckey=>$children)
                <h3>{{$key}}. {!!$children->content!!} </h3>
                 @foreach($children->answers as $akey=>$record)
                    <div class="list-listening">
                        <div class="row {{$test->layout==1?'row-eq-height':''}}">
                            <div class="{{$test->layout==1?'col-md-6':'col-md-12'}}">
                                <div class="a-question">
                                    <p class="question">{{$key}}.{{++$ckey}}. {!!$children->content!!} </p>
                                       @foreach($record->random_content() as $value)
                                       <button class="btn btn-pasal">{!!$value!!}</button>
                                       @endforeach
                                       
                                       <button class="btn btn-danger btn-re-pasal">Chọn lại</button>
                                       <input type="text" name="question" data-question='{{$record->id}}'  class="form-control form-pasal" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endforeach                 
            </div>
            @endforeach


            @else
            <div class="list-question question-{{++$key}}">
                <h3>{{$key}}. {{$test->content}} </h3>

                <div class="text-center">
                    @if(!empty($test->file))
                    @if(in_array(pathinfo($test->file, PATHINFO_EXTENSION), ['mp3']))
                    <audio controls>
                        <source src="{{$test->file}}" type="audio/mp3">
                    </audio>
                    @elseif(in_array(pathinfo($test->file, PATHINFO_EXTENSION), ['jpg', 'png']))
                    <img src="{{$test->file}}" />
                    @elseif(in_array(pathinfo($test->file, PATHINFO_EXTENSION), ['mp4']))
                    <video controls width="500" height="240" >
                        <source src="{{$test->file}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    @endif
                    @endif
                </div>
                <div class="list-listening">
                    <div class="row row-eq-height">
                        @foreach($test->getChildren as $ckey=>$children)
                        <div class="{{$test->layout==1?'col-md-6':'col-md-12'}}">
                            <div class="a-question">
                                <p class="question">{{$key}}.{{++$ckey}}. {!!$children->content!!} </p>

                                <div class="text-center">
                                    @if(!empty($children->file))
                                    @if(in_array(pathinfo($children->file, PATHINFO_EXTENSION), ['mp3']))
                                    <audio controls>
                                        <source src="{{$children->file}}" type="audio/mp3">
                                    </audio>
                                    @elseif(in_array(pathinfo($children->file, PATHINFO_EXTENSION), ['jpg', 'png']))
                                    <img src="{{$children->file}}" />
                                    @elseif(in_array(pathinfo($children->file, PATHINFO_EXTENSION), ['mp4']))
                                    <video controls width="500" height="240" >
                                        <source src="{{$children->file}}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    @endif
                                    @endif
                                </div>
                                <form action="">
                                    @foreach($children->answers as $akey=>$record)
                                    @if($test->layout==2)
                                    <div class="col-md-3">
                                        <label class="a-row row-inline" data-question="{!!$children->id!!}">{{$record->content}}
                                            <input type="radio" name="question[{{$children->id}}]" value="{{$record->id}}">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    @else
                                    <label class="a-row" data-question="{!!$children->id!!}">{{$record->content}}
                                        <input type="radio" name="question[{{$children->id}}]" value="{{$record->id}}">
                                        <span class="checkmark"></span>
                                    </label>
                                    @endif
                                    @endforeach                                  
                                </form>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            <div class="box-bottom question-box text-center">
                @if($test->type==1 && $phase->id==3)
                <a class="btn btn-next-question" data-type='2' data-nth="1" data-max="{{count($phase->tests)}}">Tiếp tục</a>
                @else
                <a class="btn btn-next-question" data-type='1' data-nth="1" data-max="{{count($phase->tests)}}">Tiếp tục</a>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
<script type="text/javascript">
    $('.btn-next-question').click(function () {
        getResult($(this).data('type'));
    });
    function getResult(type) {
        var val = '';
        console.log(type);
        if(type===1){
            $('input[type=radio]:checked').each(function () {
                var question = $(this).parent().data('question');
                var answer = $(this).val();
                var new_value = question + ':' + answer;
                if (val !== '') {
                    val = val + '|' + new_value;
                } else {
                    val = new_value;
                }
            });
        }else{  
            $('input[type=radio]:checked').each(function () {
                var question = $(this).parent().data('question');
                var answer = $(this).val();
                var new_value = question + ':' + answer;
                if (val !== '') {
                    val = val + '|' + new_value;
                } else {
                    val = new_value;
                }
            });
            $('input[name=question]').each(function () {
               var question = $(this).data('question')
               var answer = $(this).val();
               var new_value = question + ':' + answer;
                if (val !== '') {
                    val = val + '|' + new_value;
                } else {
                    val = new_value;
                }
            });
        }
        console.log(val);
        $.ajax({
                    url: '/api/push-result',
                    method: 'post',
                    data: {
                    data: val,
                            phase: {{$phase->id}}
                    },
                    success: function (resp) {
                            console.log(resp);
                    }
        });
        
    };
    $('.btn-pasal').click(function() {
        $(this).attr('disabled', 'true');
        var value = $(this).html();
        var old = $(this).parent().find('input').val();
        if(old.length == 0){
             $(this).parent().find('input').val(value);
        }else{
            $(this).parent().find('input').val(old+' '+value);
        }
       
    });
    $('.btn-re-pasal').click(function() {
        $(this).parent().find('.btn-pasal').removeAttr('disabled');
        $(this).parent().find('input').val('');
    });
</script>
@stop
