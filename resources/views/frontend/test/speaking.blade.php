@extends('frontend.layout.master')
@section('style')
@parent
<link href="{!!asset('assets/css/icons/icomoon/styles.css')!!}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/datepicker.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/jquery.timepicker.min.css')}}">

@endsection
@section('content')
<h3>{{session('result-'.$phase->id)}}</h3>
<div id="main" class="speaker">
    <input id="next-request" type="hidden" value="{!!$next_rq!!}" />
    <div class="container">
        <div class="box">
            <div class="title">
                <div class="tieude">
                    <h3>{!!$phase->name!!}</h3>
                </div>

            </div>
            <div class="list-question" style="display: block">
                <div class="introduction ">
                    <p>Phần thi Speaking sẽ được Giảng viên Pasal trực tiếp tiến hành cùng với bạn thông qua hình thức gọi điện.</p>
                    <p>Bạn hãy đọc kỹ đề bài dưới đây và đăng ký lịch rảnh để Giảng viên Pasal liên hệ lại nhằm kiểm tra khả năng Speaking của bạn.</p>
                    <p>Kết quả bài test hoàn chỉnh sẽ được gửi tới bạn thông qua email sau khi bạn hoàn thành nốt phần thi Speaking này nhé.</p>
                </div>
                <!--<h3>1. Choose the best answer to complete each of the following sentences</h3>-->
                @foreach($phase->tests as $key=>$test)                
                <div class="list">
                    @foreach($test->getChildren as $ckey=>$children)
                    <div class="a-question">
                        <p class="question">{{$ckey+1}}. {{$children->content}} </p>
                        @foreach($children->answers as $akey=>$record)
                        @if($children->layout == 3)
                        <div class="col-md-2">
                            <p>{{$ckey+1}}.{{$akey+1}} - {!!$record->content!!}.</p>
                        </div>
                        @elseif($children->layout == 1)
                        <div class="col-md-6">
                            <p>{{$ckey+1}}.{{$akey+1}} - {!!$record->content!!}.</p>
                        </div>
                        @else
                        <p>{{$ckey+1}}.{{$akey+1}} - {!!$record->content!!}.</p>
                        @endif
                        @endforeach
                    </div>
                    @endforeach
                </div>
                @endforeach

            </div>
            <form action="{!!route('appointment')!!}" method="POST">
                <div class="time-box">
                    {{csrf_field()}}
                    <p>Bạn vui lòng nhập thời gian rảnh để Giảng viên Pasal gọi điện hoàn thành phần test Speaking:</p>
                    <div class="row appointment">
                        <label class="col-md-2">Nhập ngày*: </label>
                        <div class="input-group col-md-3">
                            <input type="text" name="date" class="form-control daterange-single" value="" readonly>
                            <span class="input-group-addon"><i class="icon icon-calendar2"></i></span>
                        </div>
                        <label class="col-md-2 col-md-offset-1">Nhập giờ*: </label>
                        <div class="input-group col-md-3">
                            <input type="text" name="time" class="form-control timepicker" placeholder="Từ 5h tối đến 9h tối" readonly>
                            <span class="input-group-addon"><i class="icon-alarm"></i></span>
                        </div>
                    </div>
                </div>
                <div class="box-bottom question-box text-center">
                    <button class="btn" type="submit">Đặt lịch hẹn</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script type="text/javascript" src="{!!asset('assets/js/frontend/daterangepicker.js')!!}"></script>
<script type="text/javascript" src="{!!asset('assets/js/frontend/jquery.timepicker.min.js')!!}"></script>
<script>
// Single picker
$('.daterange-single').daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'DD/MM/YYYY'
    }
});
$('.timepicker').timepicker({
    timeFormat: 'H:mm',
    minTime: '17',
    maxTime: '21',
    interval: 15,
    startTime: '17:00'
});
</script>
@stop
