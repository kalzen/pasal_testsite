@extends('frontend.layout.master')
@section('style')
@parent
<link rel="stylesheet" href="{{asset('assets/css/frontend/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/responsive.css')}}">
@endsection
@section('content')
<div id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="item-1">
                    <img src="{{asset('assets/images/frontend/text-banner.png')}}" alt="Pasal">
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="item-2 text-center">
                    <img src="{{asset('assets/images/frontend/banner-img.png')}}" alt="Pasal">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="main">
    <div class="container">
        <div class="box box-home">
            <div class="title">
                <h2 class="text-center">Bài test khả năng giao tiếp tiếng anh</h2>
                <p class="text-center">Chào mừng bạn đến với bài test trình độ tiếng Anh giao tiếp cùa Pasal. Bài test này gồm có 4 phần, để đánh giá 4 kỹ năng:</p>
                <p class="text-center">Listening - Reading - Writing - Speaking. </p>
                <p class="text-center">Mỗi phần sẽ có hệ thống các câu hỏi dạng trắc nghiệm, sắp xếp từ và nhiệm vụ của bạn là lựa chọn đáp án chính xác nhất.</p>
            </div>
            <div class="row">
                
                @foreach($blocks as $record)
                <div class="col-md-3 col-sm-3">
                    <div class="part text-center">
                        <img src="{{asset($record->icon_url)}}">
                        <p>{!!$record->tag!!}</p>
                        <h3>{!!$record->name !!}</h3>
                        <p>{!!$record->content!!}</p>
                    </div>
                </div>
                @endforeach
                
            </div>

            <div class="box-bottom text-center">
                <p>Bạn đã sẵn sàng tham gia bài test chưa? Click vào nút <span>Sẵn sàng</span> để bắt đầu nhé!</p>
                <a class="btn" href="{!!route('register')!!}">Sẵn sàng</a>
            </div>


        </div>
    </div>
</div>
@stop
