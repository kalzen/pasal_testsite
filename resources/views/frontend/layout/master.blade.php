<!DOCTYPE html>
<html lang="en">
    <head>
        @include('frontend/layout/__header')
    </head>

    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T6BZKK9"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

        @include('frontend/layout/__sidebar')
        @yield('content')   

        @include('frontend/layout/__footer')
    </body>
    @yield('script')   
</html>
