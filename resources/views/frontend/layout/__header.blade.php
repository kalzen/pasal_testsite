<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T6BZKK9');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="{{$config->keywords}}" />
<meta name="description" content="{{$config->description}}" />
<meta name="author" content="kalzen.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta property="og:image" content="{{asset($config->image)}}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{!!route('home')!!}" />
<meta property="og:description" content="{{$config->description}}"/>
<meta property="og:title" content="{{$config->title}}" />
<title>{{$config->title}}</title>
<link rel="shortcut icon" href="{{$config->favicon}}" />
@section('style')
<link rel="stylesheet" href="{{asset('assets/css/frontend/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/frontend/font-awesome.min.css')}}">
@show
