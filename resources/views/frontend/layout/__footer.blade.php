<div id="footer">
    <div class="container">
        <p>Copyright @ 2019 PASAL</p>
    </div>
</div>  
<!-- Start of widget script -->
<script type="text/javascript">
    function loadJsAsync(t, e) {
        var n = document.createElement("script");
        n.type = "text/javascript", n.src = t, n.addEventListener("load", function (t) {
            e(null, t)
        }, !1);
        var a = document.getElementsByTagName("head")[0];
        a.appendChild(n)
    }
    window.addEventListener("DOMContentLoaded", function () {
        loadJsAsync("https://webchat.caresoft.vn:8090/js/CsChat.js?v=2.0", function () {
            var t = {domain: "pasal"};
            embedCsChat(t)
        })
    }, !1);
</script>
<!-- End of widget script -->
<script src="{{asset('assets/js/frontend/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('assets/js/frontend/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/frontend/moment.min.js')}}"></script>
<script src="{{asset('assets/js/frontend/script.js')}}"></script>