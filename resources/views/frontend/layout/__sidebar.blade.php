<div id="header">
    <div class="container">
        <div class="logo">
            <a href="{{route('home')}}"><img src="{{asset('assets/images/frontend/logo.png')}}" alt="Pasal"></a>
        </div>
        <div class="info">
            <div class="phone">
                <img src="{{asset('assets/images/frontend/icon-phone.png')}}" alt="">
                @foreach(explode('|',$config->hotline) as $key=>$phone_number)
                @if($key==0)
                <span class="number-phone">{{$phone_number}}</span>
                @else
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                <span class="number-phone">{{$phone_number}}</span>
                @endif
                @endforeach
            </div>
            <div class="social">
                <a href="{!!$config->youtube_channel!!}" target="blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                <a href="{!!$config->facebook!!}" target="blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="{!!$config->google_plus!!}" target="blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </div>
        </div>

    </div>
</div>