<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Repositories\UserRepository;
use Auth;

class AuthController extends Controller {

    public function __construct(UserRepository $userRepo) {
        $this->userRepo = $userRepo;
    }

    public function login(Request $request) {
        $remember = $request->get('remember');
        if (!isset($remember)){
            $remember = false;
        }
// Validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

// Attempt to log the user in
        if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
// if successful, then redirect to their intended location
            return response()->json([
                        'status' => 'success',
                        'user' => Auth::guard('member')->user(), 
                        'url' => route('member.test')
                    ]);
        }
// if unsuccessful, then redirect back to the login with the form data
        return response()->json([
                    'status' => 'error',
        ]);
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     * 
     * @param Request $request
     */
    public function logout() {
        Auth::guard('member')->logout();
        return redirect()->route('index');
    }

}
