<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\MemberRepository;
use Repositories\RoleRepository;
use Mail;
use App\Member;
use Storage;
use Auth;
use Repositories\MembertestRepository;

class UserController extends Controller {

    public function __construct(MemberRepository $memberRepo, MembertestRepository $membertest) {
        $this->memberRepo = $memberRepo;
        $this->membertest = $membertest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {
        $input = $request->all();
        $remember = $request->get('remember');
        if (!isset($remember)) {
            $remember = false;
        }
        $status = Member::where('email', '=', $input['email'])->first();
        if ($status != null) {
            return response()->json([
                        'error' => 'true',
                        'log' => 'Tài khoản đã tồn tại'
            ]);
        } else {
            $password = $request->get('password');
            $input['password'] = bcrypt($password);
            $input['avatar'] = 'uploads/no-user-image.png';
            $user = $this->memberRepo->create($input);
            Auth::guard('member')->login($user);
            return response()->json([
                        'status' => 'success',
                        'user' => Auth::guard('member')->user(),
                        'url' => route('member.test')
            ]);
        }
    }

    public function updateUser($user_id, Request $request) {
        $input['phone_number'] = $request->get('phone_number');
        $input['birthday'] = date('Y-m-d', strtotime($request->get('birthday')));
        $input['gender'] = $request->get('gender');
        $input['password'] = bcrypt($request->get('password'));

        if ($this->memberRepo->update($input, $user_id)) {

            return response()->json(['error' => false, 'log' => 'Cập nhật thành công!']);
        }
    }

    public function updatePhone($user_id, $phone_number) {
        $input['phone_number'] = $phone_number;
        if ($this->memberRepo->update($input, $user_id)) {
            return response()->json(['error' => false, 'log' => 'Cập nhật thành công!']);
        }
    }

    public function resetpassword(Request $request) {
        $email = $request->get('email');
        $user = $this->memberRepo->whereBy('email', $email)->first();
        $pass = str_random(10);
        $input['password'] = bcrypt($pass);
        $this->memberRepo->update($input, $user->id);
        Mail::send('mailfb', array('pass' => $pass), function($message) use ($email) {
            $message->to($email, 'Visitor')->subject('Mật khẩu mới của bạn là:');
        });
        return response()->json([
                    'log' => 'Hãy kiểm tra email của bạn để lấy mật khẩu mới nhất',
        ]);
    }

    public function getInfo($id) {
        $user = $this->memberRepo->find($id);
        return response()->json([
                    'error' => 'false',
                    'data' => $user
        ]);
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if ($this->memberRepo->update($input, $id)) {
            return response()->json(['error' => false, 'log' => 'Cập nhật thành công!']);
        }
    }

    public function uploadAudio(Request $request) {
        if ($request->hasFile('audio_data')) {
            // The file
            $audio_file = $request->file('audio_data');
            // This will return "wav" not the file name
            $filename = $audio_file->getClientOriginalExtension() . '_' . time() . ".wav";
            // This will return /audio/mp3
            $location = public_path('uploads/audio');
            // This will move the file to /public/audio/wav/
            $audio_file->move($location, $filename);
            $input['member_id'] = Auth::guard('member')->user()->id;
            $input['test_id'] = $request->get('test_id');
            $input['audio_link'] = $filename;
            $this->membertest->create($input);
        }
        return response()->json(['error' => false, 'log' => 'Upload Audio thành công!', 'filename' => $filename]);
    }

}
