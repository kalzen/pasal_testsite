<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\BlockRepository;

class BlockController extends Controller {

    public function __construct(BlockRepository $blockRepo) {
        $this->blockRepo = $blockRepo;
    }

    public function index() {
        $block = $this->blockRepo->all();
        return view('backend/block/index', compact('block'));
    }

    public function create() {
        $parent_list = $this->blockRepo->all();
        return view('backend/block/create', compact('parent_list'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->blockRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $file = $request->file('icon_url');
        if ($file == null) {
            $input['icon_url'] = null;
        } else {
            $destinationPath = 'uploads';
            $input['icon_url'] = $destinationPath . '/' . $file->getClientOriginalName();
            $file->move($destinationPath, $file->getClientOriginalName());
        }
        $this->blockRepo->create($input);
        return redirect()->route('admin.block.index');
    }

    public function edit($id) {
        $block = $this->blockRepo->find($id);
        $block->icon_url_name = basename($block->icon_url);
//        $block->icon_url_size = filesize($block->icon_url);
        $parent_list = $this->blockRepo->all();
        return view('backend/block/update', compact('block', 'parent_list'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->blockRepo->validateUpdate($id));
        $file = $request->file('icon_url');
        $old_file = $request->get('icon_url_old');
        if (is_null($old_file)) {
            if ($file == null) {
                $input['icon_url'] = null;
            } else {
                $destinationPath = 'uploads';
                $input['icon_url'] = $destinationPath . '/' . $file->getClientOriginalName();
                $file->move($destinationPath, $file->getClientOriginalName());
            }
        } else {
            $input['icon_url'] = $old_file;
        }
        unset($input['icon_url_old']);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->blockRepo->update($input, $id);
        return redirect()->route('admin.block.edit', $id)->with('success', trans('base.mss_success'));
    }

    public function destroy($id) {
        $this->blockRepo->delete($id);
        return redirect()->back();
    }

}
