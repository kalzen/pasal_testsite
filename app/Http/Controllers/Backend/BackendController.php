<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class BackendController  extends Controller
{
    

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend/index');
    }

    public function quickUpdate(Request $request){
        $table = $request->get('table');
        $new_value = $request->get('newValue');
        $attr = $request->get('attr');
        $id = $request->get('id');
        return \DB::table($table)->where('id', $id)->update([$attr=>$new_value]);
    }
}