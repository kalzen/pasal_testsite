<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\TestimonialRepository;

class TestimonialController extends Controller {

    public function __construct(TestimonialRepository $testimonialRepo) {
        $this->testimonialRepo = $testimonialRepo;
    }

    public function index() {
        $testimonial = $this->testimonialRepo->all();
        return view('backend/testimonial/index', compact('testimonial'));
    }

    public function create() {
        $parent_list = $this->testimonialRepo->all();
        return view('backend/testimonial/create', compact('parent_list'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->testimonialRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->testimonialRepo->create($input);
        return redirect()->route('admin.testimonial.index');
    }

    public function edit($id) {
        $testimonial = $this->testimonialRepo->find($id);
        $parent_list = $this->testimonialRepo->all();
        return view('backend/testimonial/update', compact('testimonial', 'parent_list'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->testimonialRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->testimonialRepo->update($input, $id);
        return redirect()->route('admin.testimonial.edit', $id)->with('success', trans('base.mss_success'));

    }

    public function destroy($id) {
        $this->testimonialRepo->delete($id);
        return redirect()->back();
    }

}
