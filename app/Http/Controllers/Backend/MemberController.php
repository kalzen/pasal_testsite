<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\MemberRepository;
use Repositories\TrainerRepository;
use Repositories\BasisRepository;
use Repositories\ProvinceRepository;
use Repositories\TestRepository;
use Repositories\ConfigRepository;
use Repositories\CustomerRepository;
use Anam\PhantomMagick\Converter;
use Repositories\UserRepository;

class MemberController extends Controller {

    public function __construct(UserRepository $userRepo,CustomerRepository $customerRepo, ConfigRepository $configRepo, TestRepository $testRepo, ProvinceRepository $provinceRepo, BasisRepository $basisRepo, MemberRepository $memberRepo, TrainerRepository $trainerRepo, \Repositories\RoleRepository $roleRepo, \Repositories\MemberTestRepository $memberTestRepo) {
        $this->memberRepo = $memberRepo;
        $this->trainerRepo = $trainerRepo;
        $this->basisRepo = $basisRepo;
        $this->roleRepo = $roleRepo;
        $this->membertestRepo = $memberTestRepo;
        $this->provinceRepo = $provinceRepo;
        $this->testRepo = $testRepo;
        $this->configRepo = $configRepo;
        $this->customerRepo = $customerRepo;
        $this->userRepo = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $provinces = $this->provinceRepo->all();
        $members = $this->memberRepo->getAllMember($request);
        foreach ($members as $key => $val) {
            $members[$key]['hasNewtest'] = $this->membertestRepo->checkHasNewtest($val->id);
        }
        $teachers = $this->trainerRepo->getAllTrainer();
        $trainer = $this->trainerRepo->getAllTrainer();
        $roles = $this->roleRepo->all();
        $search = $request->all();
        $basis = $this->basisRepo->getAllBasis();
        $total_point = $this->testRepo->getTotalPoint();
        unset($search['_token']);
        return view('backend/member/index', compact('members', 'trainer', 'provinces', 'roles', 'basis', 'search', 'total_point', 'teachers'));
    }

    public function create() {
        $trainer = $this->trainerRepo->getAllTrainer();
        return view('backend/member/create', compact('trainer'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->memberRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $this->memberRepo->create($input);
        return redirect()->route('admin.member.index');
    }

    public function edit($id) {
        $member = $this->memberRepo->find($id);
        $trainer = $this->trainerRepo->getAllTrainer();
        $basis = $this->basisRepo->allOrder();
        $provinces = $this->provinceRepo->allOrder();
//        dd(is_null($member->provider_id));
        return view('backend/member/update', compact('member', 'trainer', 'basis', 'provinces'));
    }

    public function update(Request $request, $id) {
        $member = $this->customerRepo->find($id);
        $input = $request->all();
        $validator = \Validator::make($input, $this->customerRepo->validateBEUpdate($id));
        $file = $request->file('avatar');
        $old_file = $request->get('avatar_old');
        if (is_null($old_file)) {
            if ($file == null) {
                $input['avatar'] = null;
            } else {
                $destinationPath = 'uploads';
                $input['avatar'] = $destinationPath . '/' . $file->getClientOriginalName();
                $file->move($destinationPath, $file->getClientOriginalName());
            }
        } else {
            $input['avatar'] = $old_file;
        }
        unset($input['avatar_old']);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->memberRepo->update($input, $id);
        return redirect()->route('admin.member.edit', $id)->with('success', 'Cập nhật thành công');
    }

    public function destroy($id) {
        $this->memberRepo->delete($id);
        return redirect()->back();
    }

    public function switchTrainer(Request $request) {
        $trainer_id = $request->get('trainer_id');
        $member_ids = $request->get('member_ids');
        foreach (explode(',', $member_ids) as $key => $val) {
            $this->memberRepo->update(['user_id' => $trainer_id], $val);
        }
        return redirect()->back()->with('success', 'Chuyển quản lý mới thành công!');
    }

    public function updateTrainer(Request $request) {
        $member_ids = $request->get('member_ids');
        $trainer_id = \Auth::user()->id;
        $res = 0;
        foreach (explode(',', $member_ids) as $val) {
            $member = $this->memberRepo->find($val);
            if (!$member->trainer_id) {
                $res = $this->memberRepo->update(['trainer_id' => $trainer_id], $val);
            }
        }
        if ($res) {
            return response()->json(['message' => 'Chăm sóc thành công!']);
        } else {
            return response()->json(['message' => 'Thao tác không thành công!']);
        }
    }

    public function export($tel) {
        Converter::make(route('result', ['tel'=>$tel]))
//                ->setBinary('C:\Users\trinh\Downloads\Compressed\phantomjs-2.1.1-windows\phantomjs-2.1.1-windows\bin\phantomjs.exe')
                ->setBinary('phantomjs')
                ->toPng()
                ->download($tel.'.png');
    }
    
    public function sendmail($tel, $type=null) {
        Converter::make(route('result', ['tel'=>$tel, 'type'=>$type]))
//                ->setBinary('C:\Users\trinh\Downloads\Compressed\phantomjs-2.1.1-windows\phantomjs-2.1.1-windows\bin\phantomjs.exe')
                ->setBinary('phantomjs')
                ->toPng()
                ->save('export/'.$tel.'.png');
        $message='<img src='.asset('export/'.$tel.'.png').'></img>';
        $email = $this->customerRepo->findBy('tel', $tel)->email;
        $config['to']=$email;
        $config['to_name']='Admin Pasal';
        $config['subject'] = 'Kết quả test Pasal';
        \Mail::send('emails.default', ['html' => $message],function($message) use($config) {
                    $message->to($config['to'], $config['to_name'])->subject($config['subject']);
                });
        return redirect()->back()->with('success', 'Send mail thành công!');
    }

}
