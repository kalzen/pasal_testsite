<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ScheduleRepository;
use App\Schedule;

class ScheduleController extends Controller {

    public function __construct(ScheduleRepository $scheduleRepo) {
        $this->scheduleRepo = $scheduleRepo;
    }

    public function index() {
        $weekdays = $this->scheduleRepo->weekdays();
        $schedule = $this->scheduleRepo->all();
        return view('backend/schedule/index', compact('schedule', 'weekdays'));
    }

    public function create() {
        $weekdays = $this->scheduleRepo->weekdays();
        return view('backend/schedule/create', compact('weekdays'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->scheduleRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input['schedule'] = implode(',', $input['schedule']);
        $this->scheduleRepo->create($input);
        return redirect()->route('admin.schedule.index');
    }

    public function edit($id) {
        $weekdays = $this->scheduleRepo->weekdays();
        $schedule = $this->scheduleRepo->find($id);
        return view('backend/schedule/update', compact('schedule', 'weekdays'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->scheduleRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input['schedule'] = implode(',', $input['schedule']);
        $this->scheduleRepo->update($input, $id);
        return redirect()->route('admin.schedule.edit', $id)->with('success', trans('base.mss_success'));

    }

    public function destroy($id) {
        $this->scheduleRepo->delete($id);
        return redirect()->back();
    }

}
