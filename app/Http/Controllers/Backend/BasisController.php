<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\BasisRepository;

class BasisController extends Controller {

    public function __construct(BasisRepository $basisRepo) {
        $this->basisRepo = $basisRepo;
    }

    public function index() {
        $basis = $this->basisRepo->all();
        return view('backend/basis/index', compact('basis'));
    }

    public function create() {

        return view('backend/basis/create');
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->basisRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->basisRepo->create($input);
        return redirect()->route('admin.basis.index');
    }

    public function edit($id) {
        $basis = $this->basisRepo->find($id);
        return view('backend/basis/update', compact('basis'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->basisRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->basisRepo->update($input, $id);
        return redirect()->route('admin.basis.edit', $id)->with('success', trans('base.mss_success'));

    }

    public function destroy($id) {
        $this->basisRepo->delete($id);
        return redirect()->back();
    }

}
