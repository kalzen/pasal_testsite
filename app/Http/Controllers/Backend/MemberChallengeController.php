<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\MembertestRepository;

class MembertestController extends Controller {

    public function __construct(MembertestRepository $membertestRepo, \Repositories\MemberRepository $memberRepo, \Repositories\QuestionRepository $questionRepo) {
        $this->membertestRepo = $membertestRepo;
        $this->memberRepo = $memberRepo;
        $this->questionRepo = $questionRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $filter = $request->all();
        if ($request->get('chat_status')) {
            $records = $this->membertestRepo->getAllByRequest($request);
        } else {
            $records = $this->membertestRepo->getAll();
        }
        return view('backend/member_test/index', compact('records', 'filter'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $input['updated_at'] = date('Y-m-d H:i:s');
        $input['is_viewed'] = 0;
        $res = $this->membertestRepo->updateMembertest($input, $id);
        if ($res) {
            return redirect()->back()->with('success', 'Chấm điểm thành công');
            ;
        }
    }

    public function destroy($id) {
        $this->membertestRepo->delete($id);
        return redirect()->back();
    }

    public function detail($member_id) {
        $records = $this->membertestRepo->getMembertestByMemberId($member_id);
        $member = $this->memberRepo->find($member_id);
        $questions = $this->questionRepo->findByMemberId($member_id);
        return view('backend/member_test/detail', compact('records', 'member', 'questions'));
        //if($res) return \GuzzleHttp\json_encode(['success'=>true]);
    }

    public function getMarkModal(Request $request) {
        $id = $request->get('id');
        $res = $this->membertestRepo->getMarkModal($id);
        return json_encode(['html' => $res]);
    }

    public function toggle(Request $request) {
        $id = $request->get('id');
        $res = $this->membertestRepo->toggleIsHot($id);
        if ($res) {
            return response()->json(['success' => true]);
        }
    }

}
