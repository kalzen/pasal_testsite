<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\BasisRepository;

class ContentController extends Controller
{
     public function __construct(BasisRepository $basisRepo) {
        $this->basisRepo = $basisRepo;
    }
}
