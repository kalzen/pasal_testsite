<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\RankRepository;

class RankController extends Controller {

    public function __construct(RankRepository $rankRepo) {
        $this->rankRepo = $rankRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($phase, Request $request) {
        $records = $this->rankRepo->getByPhase($phase);
        return view('backend/rank/index', compact('records', 'phase'));
    }

    public function create($phase) {
        return view('backend/rank/create',  compact('phase'));
    }

    public function store(Request $request) {
        $phase = $request->get('phase_id');
        $input = $request->all();
        $validator = \Validator::make($input, $this->rankRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->rankRepo->create($input);

        return redirect()->route('admin.rank.index', ['phase' => $phase]);
    }

    public function edit($id) {
        $record = $this->rankRepo->find($id);
        $phase = $record->phase_id;
        return view('backend/rank/update', compact('record', 'phase'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->rankRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->rankRepo->update($input, $id);
        return redirect()->route('admin.rank.edit', $id)->with('success', 'Update thành công');
    }

    public function destroy($id) {
        $this->rankRepo->delete($id);
        return redirect()->back();
    }

    public function toggle($id) {
        $this->rankRepo->toggle($id);
        return redirect()->back();
    }

    public function toggleGroup(Request $request) {
        $group = $request->get('group');
        if (is_null($group)) {
            return redirect()->back();
        }
        $status = $request->get('status');
        $this->rankRepo->toggleGroup($group, $status);
        return redirect()->back();
    }

    public function report(Request $request) {
        $monthly = $this->monthRepo->getByMonthly($request);
        $tests = $this->rankRepo->all();
        $monthly = array_flip($monthly);
        $search = $request->all();
        $allMonthly = $this->monthRepo->getAll();
        return view('backend/rank/report', compact('tests', 'monthly', 'search', 'allMonthly'));
    }

}
