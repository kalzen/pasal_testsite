<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\TestRepository;
use Repositories\AnswerRepository;
use Repositories\ResultRepository;
use Repositories\BasisRepository;
use Repositories\PhaseRepository;
use Repositories\CustomerRepository;
use Repositories\ProvinceRepository;

class TestController extends Controller {

    public function __construct(ProvinceRepository $provinceRepo, CustomerRepository $customerRepo, PhaseRepository $phaseRepo, BasisRepository $basisRepo, TestRepository $testRepo, AnswerRepository $anwserRepo, ResultRepository $resultRepo) {
        $this->testRepo = $testRepo;
        $this->anwserRepo = $anwserRepo;
        $this->resultRepo = $resultRepo;
        $this->basisRepo = $basisRepo;
        $this->phaseRepo = $phaseRepo;
        $this->customerRepo = $customerRepo;
        $this->provinceRepo = $provinceRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($phase, Request $request) {
        $records = $this->testRepo->getTestByPhase($phase);
        return view('backend/test/index', compact('records', 'phase'));
    }

    public function listQuestion($phase, Request $request) {
        $parent = $request->get('parent_id');
        $records = $this->testRepo->getQuestionByPhase($phase, $parent);
        $arr_phase = $this->testRepo->getTestByPhase($phase)->pluck('name', 'id')->toArray();
        return view('backend/test/list', compact('records', 'phase', 'arr_phase'));
    }

    public function create() {
        return view('backend/test/create');
    }

    public function store(Request $request) {
        $phase = $request->get('phase_id');
        $parent = $request->get('parent_id');
        $input = $request->all();
        if ($parent == 0) {
            $validator = \Validator::make($input, $this->testRepo->validateCreate());
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $this->testRepo->create($input);
            return redirect()->route('admin.test.index', ['phase' => $phase]);
        } else {
            $question_input = $request->only('phase_id', 'name', 'parent_id', 'order', 'point', 'content', 'file');
            $anwser_input = $request->only('content_question', 'status_question');
            $validator = \Validator::make($input, $this->testRepo->validateCreate());
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $question = $this->testRepo->create($question_input);
            $anwser_input['test_id'] = $question->id;
            $answer = $this->anwserRepo->createAnswer($anwser_input);
            return redirect()->route('admin.test.list', ['phase' => $phase]);
        }
    }

    public function edit($id) {
        $record = $this->testRepo->find($id);
        $phase = $record->phase_id;
        return view('backend/test/update', compact('record', 'phase'));
    }

    public function editQuestion($id) {
        $record = $this->testRepo->find($id);
        $phase_id = $record->phase_id;
        $arr_phase = $this->testRepo->getTestByPhase($phase_id)->pluck('name', 'id')->toArray();

        return view('backend/test/update_question', compact('record', 'phase_id', 'arr_phase'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->testRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->testRepo->update($input, $id);
        return redirect()->route('admin.test.edit', $id)->with('success', 'Update thành công');
    }

    public function updateQuestion(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->testRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->testRepo->update($input, $id);
        $this->testRepo->find($id)->answers()->delete();
        $anwser_input = $request->only('content_question', 'status_question');
        $anwser_input['test_id'] = $id;
        $answer = $this->anwserRepo->createAnswer($anwser_input);
        return redirect()->route('admin.test.question.edit', $id)->with('success', 'Update thành công');
    }

    public function destroy($id) {
        $check = $this->testRepo->find($id);
        $childen = $this->testRepo->find($check->parent_id);
        if ($childen !== null) {
            foreach ($childen as $c) {
                $this->testRepo->delete($c->id);
            }
        }
        $this->testRepo->delete($id);
        return redirect()->back();
    }

    public function toggle($id) {
        $this->testRepo->toggle($id);
        return redirect()->back();
    }

    public function toggleGroup(Request $request) {
        $group = $request->get('group');
        if (is_null($group)) {
            return redirect()->back();
        }
        $status = $request->get('status');
        $this->testRepo->toggleGroup($group, $status);
        return redirect()->back();
    }

    public function report(Request $request) {
        $monthly = $this->monthRepo->getByMonthly($request);
        $tests = $this->testRepo->all();
        $monthly = array_flip($monthly);
        $search = $request->all();
        $allMonthly = $this->monthRepo->getAll();
        return view('backend/test/report', compact('tests', 'monthly', 'search', 'allMonthly'));
    }

    public function updateResult(Request $request) {
        $customer_id = $request->get('customer_id');
        $answer = $request->get('answer');
        $phase_4 = '';
        $phase_4_answer = [];
        $i = 0;
        foreach ($answer as $key => $record) {
            $phase_4_answer = array_replace($phase_4_answer, $record);
            $percent = array_sum($record) / (count($record) * 10);
            $point_speak = \App\Test::find($key)->point;
            if ($i == 0) {
                $phase_4 = $key . ":" . ($point_speak * $percent);
            } else {
                $phase_4 = $phase_4 . "|" . $key . ":" . ($point_speak * $percent);
            }
            $i++;
        }
        $data['customer_id'] = $customer_id;
        $data['phase_4'] = $phase_4;
        $data['phase_4_answer'] = json_encode($phase_4_answer);
        $this->resultRepo->createOrUpdate($data);
        return redirect()->back();
    }

    public function inputTest() {
        $basis = $this->basisRepo->getAllBasis();
        $phase1 = $this->phaseRepo->find(1);
        $phase2 = $this->phaseRepo->find(2);
        $phase3 = $this->phaseRepo->find(3);
        $phase4 = $this->phaseRepo->find(4);
        $provinces = $this->provinceRepo->allOrder();
        return view('backend/test_input/index', compact('basis', 'phase1', 'phase2', 'phase3', 'phase4', 'provinces'));
    }

    public function inputTestPost(Request $request) {
        $data_user = $request->only('fullname', 'tel', 'email', 'basis_id', 'province_id');
        $validator = \Validator::make($data_user, $this->customerRepo->validateBE());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $customer = $this->customerRepo->create($data_user);
        $result = $this->resultRepo->create(['customer_id' => $customer->id]);
        $questions = $request->get('question');
        $answers = $request->get('answer');
        foreach ($questions as $key => $question) {
            $phase_id = 'phase_' . $key;
            $val = '';
            foreach ($question as $akey => $value) {
                if ($val == '') {
                    $val = $akey . ':' . $value;
                } else {
                    $val = $val . '|' . $akey . ':' . $value;
                }
            }
            $this->resultRepo->find($result->id)->update([$phase_id => $val]);
        }
        $phase_4 = '';
        foreach ($answers as $key => $record) {
            $percent = array_sum($record) / (count($record) * 10);
            $point_speak = \App\Test::find($key)->point;
            if ($phase_4 == '') {
                $phase_4 = $key . ":" . ($point_speak * $percent);
            } else {
                $phase_4 = $phase_4 . "|" . $key . ":" . ($point_speak * $percent);
            }
        }
        $this->resultRepo->find($result->id)->update(['phase_4' => $phase_4]);
        
        return redirect()->route('admin.member.export', ['tel'=>$customer->tel]);
    }

}
