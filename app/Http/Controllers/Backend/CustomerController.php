<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\CustomerRepository;
use Repositories\BasisRepository;
use Repositories\MonthlyRepository;

class CustomerController extends Controller {

    public function __construct(CustomerRepository $customerRepo, BasisRepository $basisRepo, MonthlyRepository $monthRepo) {
        $this->customerRepo = $customerRepo;
        $this->basisRepo = $basisRepo;
        $this->monthRepo = $monthRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->all();
        $customers = $this->customerRepo->getAllCustomer($request);
        $weekdays = $this->customerRepo->weekdays();
        $basis = $this->basisRepo->all();
        return view('backend/customer/index', compact('customers', 'weekdays', 'basis', 'search'));
    }

    public function create() {

        return view('backend/customer/create', compact('parent'));
    }

    public function store(Request $request) {
        $input = $request->all();

        $validator = \Validator::make($input, $this->customerRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $role_id = $this->customerRepo->getRoleID();
        $input['role_id'] = $role_id;
        $this->customerRepo->create($input);

        return redirect()->route('admin.customer.index');
    }

    public function edit($id) {
        $customer = $this->customerRepo->find($id);
        return view('backend/customer/update', compact('customer'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->customerRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->customerRepo->update($input, $id);
        return redirect()->route('admin.customer.edit', $id)->with('success', 'Update thành công');
        ;
    }

    public function destroy($id) {
        $this->customerRepo->delete($id);
        return redirect()->back();
    }

    public function export(Request $request) {
        $group = $request->get('group');
        if (!is_null($group)) {
            $this->customerRepo->exportCustomer($group);
        }
        return redirect()->back();
    }

    public function chart() {
        $basis = $this->basisRepo->all();
        $obj_monthlys = $this->monthRepo->getAll();
        $monthly = $this->monthRepo->getString();
        $data = '';
        foreach ($basis as $key => $value) {
            $count = '';

            foreach ($obj_monthlys as $mkey => $month) {
                if (!isset($value->monthly($month->id)->count_data)) {
                    $number = 0;
                } else {
                    $number = $value->monthly($month->id)->count_data;
                }
                if ($mkey == 0) {
                    $count = $number;
                } else {
                    $count = $count . ', ' . $number;
                }
            }
            $rgb = 'rgb(' . rand(0, 255) . ',' . rand(0, 255) . ',' . rand(0, 255) . ')';
            $new_data = "{label: '" . $value->name . "', backgroundColor: '" . $rgb . "', borderColor: '" . $rgb . "', data: [" . $count . "]}";

            if ($key == 0) {
                $data = $new_data;
            } else {
                $data = $data . ', ' . $new_data;
            }
        }

        return view('backend/customer/chart', compact('monthly', 'basis', 'data'));
    }

}
