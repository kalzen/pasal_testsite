<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\UserRepository;
use Repositories\BasisRepository;
use Repositories\RoleRepository;

class UserController extends Controller {

    public function __construct(UserRepository $userRepo, BasisRepository $basisRepo, RoleRepository $roleRepo) {
        $this->userRepo = $userRepo;
        $this->basisRepo = $basisRepo;
        $this->roleRepo = $roleRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() { 
        $users = $this->userRepo->getAllUser();
        return view('backend/user/index', compact('users'));
    }

    public function create() {
        $roles = $this->roleRepo->getAllRole();
        return view('backend/user/create', compact('roles'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->userRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        if(is_null($request->get('avatar'))){
            $input['avatar'] = 'uploads/paul-gruber-vip.png';
        }
        $this->userRepo->create($input);

        return redirect()->route('admin.user.index');
    }

    public function edit($id) {
        $user = $this->userRepo->find($id);
        $roles = $this->roleRepo->all();
        return view('backend/user/update', compact('user', 'roles'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $user = $this->userRepo->find($id);
        //Nếu không nhập mật khẩu thì sẽ gán mặc định bằng mật khẩu cũ
        if ($request->password == null || $request->password == "") {
            $input['password'] = $input['password_confirmation'] = $user->password;
        }
        $validator = \Validator::make($input, $this->userRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //Nếu không nhập mật khẩu thì sẽ gán mặc định bằng mật khẩu cũ
        if ($request->password == null || $request->password == "") {
            $input['password'] = $user->password;
        } else {
            $input['password'] = bcrypt($input['password']);
        }
        $this->userRepo->update($input, $id);
        return redirect()->route('admin.user.edit', $id)->with('success', 'Update thành công');
    }
    
    public function destroy($id) {
        $this->userRepo->delete($id);
        return redirect()->back();
    }
		
}
