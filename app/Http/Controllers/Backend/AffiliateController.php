<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\AffiliateRepository;
use Repositories\MonthlyRepository;
use Repositories\BasisRepository;

class AffiliateController extends Controller {

    public function __construct(AffiliateRepository $affiliateRepo, MonthlyRepository $monthRepo, BasisRepository $basisRepo) {
        $this->affiliateRepo = $affiliateRepo;
        $this->monthRepo = $monthRepo;
        $this->basisRepo = $basisRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->all();
        $affiliates = $this->affiliateRepo->getAllAffiliate($request);
        $server_link = 'http://' . \Request::server('SERVER_NAME') . '/';
        $basis = $this->basisRepo->allOrder();
        return view('backend/affiliate/index', compact('affiliates', 'basis', 'search', 'server_link'));
    }

    public function create() {
        $code = str_random(6);
        $server_link = 'http://' . \Request::server('SERVER_NAME') . '/';
        return view('backend/affiliate/create', compact('code', 'server_link'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->affiliateRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->affiliateRepo->create($input);

        return redirect()->route('admin.affiliate.index');
    }

    public function edit($id) {
        $server_link = 'http://' . \Request::server('SERVER_NAME') . '/';
        $affiliate = $this->affiliateRepo->find($id);
        $weekdays = $this->affiliateRepo->weekdays();
        return view('backend/affiliate/update', compact('affiliate', 'server_link', 'weekdays'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->affiliateRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->affiliateRepo->update($input, $id);
        return redirect()->route('admin.affiliate.edit', $id)->with('success', 'Update thành công');
        ;
    }

    public function destroy($id) {
        $this->affiliateRepo->delete($id);
        return redirect()->back();
    }

    public function toggle($id) {
        $this->affiliateRepo->toggle($id);
        return redirect()->back();
    }

    public function toggleGroup(Request $request) {
        $group = $request->get('group');
        if (is_null($group)) {
            return redirect()->back();
        }
        $status = $request->get('status');
        $this->affiliateRepo->toggleGroup($group, $status);
        return redirect()->back();
    }

    public function report(Request $request) {
        $monthly = $this->monthRepo->getByMonthly($request);
        $affiliates = $this->affiliateRepo->all();
        $monthly = array_flip($monthly);
        $search = $request->all();
        $allMonthly = $this->monthRepo->getAll();
        return view('backend/affiliate/report', compact('affiliates', 'monthly', 'search', 'allMonthly'));
    }

}
