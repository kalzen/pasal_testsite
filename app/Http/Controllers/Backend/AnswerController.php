<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\AnswerRepository;
class AnswerController extends Controller
{
    public function __construct(AnswerRepository $answerRepo){
        $this->answerRepo = $answerRepo;
    }
    public function update(Request $request){
        $input = $request->all();
        $this->answerRepo->updateContent($input);
        return redirect()->back()->with('success','Lưu thành công!');
    }
}
