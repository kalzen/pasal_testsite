<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\TrainerRepository;

class TrainerController extends Controller {

    public function __construct(TrainerRepository $trainerRepo) {
        $this->trainerRepo = $trainerRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $trainers = $this->trainerRepo->getAllTrainer();
        return view('backend/trainer/index', compact('trainers'));
    }

    public function create() {
        return view('backend/trainer/create', compact('roles'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->trainerRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $input['role_id'] = \App\User::ROLE_TRAINER;
        $this->trainerRepo->create($input);

        return redirect()->route('admin.trainer.index');
    }

    public function edit($id) {
        $trainer = $this->trainerRepo->find($id);
        $trainer->avatar_name = basename($trainer->avatar);
        $trainer->avatar_size = filesize($trainer->avatar);
        return view('backend/trainer/update', compact('trainer', 'roles'));
    }

    public function update(Request $request, $id) {
        $trainer = $this->trainerRepo->find($id);
        $input = $request->all();
        //Nếu không nhập mật khẩu thì sẽ gán mặc định bằng mật khẩu cũ
        if ($request->password == null || $request->password == "") {
            $input['password'] = $input['password_confirmation'] = $trainer->password;
        }
        $validator = \Validator::make($input, $this->trainerRepo->validateUpdate($id));
        //Nếu không nhập mật khẩu thì sẽ gán mặc định bằng mật khẩu cũ
        if ($request->password == null || $request->password == "") {
            $input['password'] = $trainer->password;
        } else {
            $input['password'] = bcrypt($input['password']);
        }
        $file = $request->file('avatar');
        $old_file = $request->get('avatar_old');
        if (is_null($old_file)) {
            if ($file == null) {
                $input['avatar'] = null;
            } else {
                $destinationPath = 'uploads';
                $input['avatar'] = $destinationPath . '/' . $file->getClientOriginalName();
                $file->move($destinationPath, $file->getClientOriginalName());
            }
        } else {
            $input['avatar'] = $old_file;
        }
        unset($input['avatar_old']);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->trainerRepo->update($input, $id);
        return redirect()->route('admin.trainer.edit', $id)->with('success', 'Cập nhật thành công');
    }

    public function destroy($id) {
        $this->trainerRepo->delete($id);
        return redirect()->back();
    }

    public function getTrainerOptions(Request $request){
        $trainer = $this->trainerRepo->getTrainerByRole($request->get('role_id'));
        $html='<option value="0">Chọn quản lý mới</option>';
        foreach($trainer as $val){
            $html .='<option value="'.$val->id.'">'.$val->name.'</option>';
        }
        return response()->json(['html'=>$html]);
    }
}
