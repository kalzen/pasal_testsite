<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProvinceRepository;

class ProvinceController extends Controller {

    public function __construct(ProvinceRepository $provinceRepo) {
        $this->provinceRepo = $provinceRepo;
    }

    public function index() {
        $provinces = $this->provinceRepo->all();
        return view('backend/province/index', compact('provinces'));
    }

    public function create() {

        return view('backend/province/create');
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->provinceRepo->validateCreate());
        if ($validator->fails()) {
                    dd($input);
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $this->provinceRepo->create($input);
        return redirect()->route('admin.province.index');
    }

    public function edit($id) {
        $province = $this->provinceRepo->find($id);
        return view('backend/province/update', compact('province'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->provinceRepo->validateUpdate($id));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $this->provinceRepo->update($input, $id);
        return redirect()->route('admin.province.edit', $id)->with('success', trans('base.mss_success'));

    }

    public function destroy($id) {
        $this->provinceRepo->delete($id);
        return redirect()->back();
    }

}
