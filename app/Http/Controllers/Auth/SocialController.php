<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Auth;

class SocialController extends Controller {

    public function redirect($provider) {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider) {
        $userSocial = Socialite::driver($provider)->stateless()->user();
        $user = \App\Member::where(['provider_id' => $userSocial->getId(), 'provider'=>$provider])->first();
        if ($user) {
            Auth::guard('member')->login($user);
        } else {
            $user = \App\Member::firstOrCreate([
                        'fullname' => $userSocial->getName(),
                        'email' => $userSocial->getEmail(),
                        'avatar' => $userSocial->getAvatar(),
                        'provider_id' => $userSocial->getId(),
                        'provider' => $provider,
                        'status' => 0
            ]);
            Auth::guard('member')->login($user);
        }
        return redirect()->route('member.test');
    }

}
