<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;
use Repositories\CustomerRepository;
use Repositories\AffiliateRepository;
use Repositories\BasisRepository;
use Repositories\ProvinceRepository;
use Repositories\BlockRepository;
use Repositories\ConfigRepository;
use Repositories\MemberRepository;
use Repositories\PhaseRepository;
use Repositories\ResultRepository;

class FrontendController extends Controller {

    public function __construct(ResultRepository $resultRepo, PhaseRepository $phaseRepo, MemberRepository $memberRepo, ConfigRepository $configRepo, BlockRepository $blockRepo, CustomerRepository $customerRepo, AffiliateRepository $affRepo, BasisRepository $basisRepo, ProvinceRepository $provinceRepo) {
        $this->customerRepo = $customerRepo;
        $this->affRepo = $affRepo;
        $this->basisRepo = $basisRepo;
        $this->provinceRepo = $provinceRepo;
        $this->blockRepo = $blockRepo;
        $this->configRepo = $configRepo;
        $this->memberRepo = $memberRepo;
        $this->phaseRepo = $phaseRepo;
        $this->resultRepo = $resultRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ref = null, Request $request) {
        $utm = $request->all();
        if ($utm != null) {
            $link_register = url()->full();
            session()->put('link_register', $link_register);
        }
        $blocks = $this->blockRepo->getBlockByPosition('trang-chu');
        $config = $this->configRepo->first();
        return view('frontend/index', compact('config', 'ref', 'blocks'));
    }

    public function registerFE() {
        $link_register = session()->exists('link_register') ? session()->get('link_register') : '';
        $config = $this->configRepo->first();
        $basis = $this->basisRepo->allOrder();
        $provinces = $this->provinceRepo->getAllProvince();
        return view('frontend/register', compact('config', 'provinces', 'basis', 'link_register'));
    }

    public function test($phase) {
        $config = $this->configRepo->first();
        $phase = $this->phaseRepo->getPhaseByAlias($phase);
        $next_phase = $this->phaseRepo->find($phase->id + 1);
        if ($next_phase != null) {
            $next_rq = route('test', ['phase' => $next_phase->alias]);
        } else {
            $next_rq = route('thank.you');
        }
        if ($phase->id == 4) {
            return view('frontend/test/speaking', compact('config', 'phase', 'next_rq'));
        }
        return view('frontend/test/index', compact('config', 'phase', 'next_rq'));
    }

    public function register(Request $request) {
        $input = $request->all();
        $ref = $request->get('link_register');

        $validator = \Validator::make($input, $this->customerRepo->validate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if (!is_null($ref)) {
            $input['link_register'] = $ref;
            $parts = parse_url($ref);
            parse_str($parts['query'], $query);
            $input['link_name'] = $query['utm_content'];
        }
        $check = $this->customerRepo->findBytel($request->get('tel'));
        if ($check !== null) {
            session(['customer_id' => $check->id]);
            session(['tel' => $check->tel]);
            if ($check->result !== null) {
                if ($check->result->phase_1 == null) {
                    return redirect()->route('test', \App\Phase::where('id', '=', 1)->first()->alias);
                } elseif ($check->result->phase_2 == null) {
                    return redirect()->route('test', \App\Phase::where('id', '=', 2)->first()->alias);
                } elseif ($check->result->phase_3 == null) {
                    return redirect()->route('test', \App\Phase::where('id', '=', 3)->first()->alias);
                } elseif ($check->result->phase_4 == null) {
                    return redirect()->route('test', \App\Phase::where('id', '=', 4)->first()->alias);
                }
            } else {
                return redirect()->route('test', \App\Phase::where('id', '=', 1)->first()->alias);
            }
            return redirect()->route('result', ['tel' => $request->get('tel')]);
        } else {
            $customer = $this->customerRepo->create($input);
            $this->basisRepo->countData($customer->basis_id);
            session(['customer_id' => $customer->id]);
            session(['tel' => $customer->tel]);
            if ($ref != null) {
                $this->affRepo->countData($ref);
            }
            return redirect()->route('test', \App\Phase::first()->alias);
        }
    }

    public function traffic(Request $request) {
        $ref = $request->get('ref');
        if (!is_null($ref)) {
            $this->affRepo->countTraffic($ref);
        }
        return response()->json(['error' => false]);
    }

    public function thankYou() {
        $config = $this->configRepo->first();
        return view('frontend/thank-you', compact('config'));
    }

    public function result(Request $request) {
        $config = $this->configRepo->first();
        $tel = $request->get('tel');
        $customer = $this->customerRepo->findBy('tel', $tel);
        if ($customer == null) {
            return view('frontend/no-result', compact('config'));
        }
        $result = $customer->result;

        return view('frontend/result', compact('config', 'customer', 'result', 'tel'));
    }

    public function pushResult(Request $request) {
        $user = session('customer_id');
        $phase = $request->get('phase');
        $result = $request->get('data');
        $data = ['customer_id' => $user, 'phase_' . $phase => $result];
        $check = $this->resultRepo->createOrUpdate($data);
        return response()->json(['sucess' => $check]);
    }

    public function appointment(Request $request) {
        $user = session('customer_id');
        $date = $request->get('date');
        $time = $request->get('time');
        if (empty($time)) {
            $time = '17:00';
        }
        $date_time = $date . ' ' . $time;
        $timestamp = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $date_time);
        $user = $this->customerRepo->find($user)->update(['appointment' => $timestamp]);
        return redirect()->route('thank.you');
    }

    public function getUpdatePhase4(Request $request) {
        $customer_id = $request->get('customer_id');
        $customer = $this->customerRepo->find($customer_id);
        $phase = $this->phaseRepo->find(4);
        if ($customer->result->phase_4_answer !== null && !empty($customer->result->phase_4_answer)) {
            $phse_4_answer = json_decode($customer->result->phase_4_answer, true);
            $keys = array_keys($phse_4_answer);
            if ($keys === array_filter($keys, 'is_numeric')) {
                $phse_4_answer = array_combine(
                        array_map('intval', $keys), array_values($phse_4_answer)
                );
            }
        } else {
            $phse_4_answer = null;
        }

        return view('backend.member.phase', compact('customer', 'phase', 'phse_4_answer'));
    }

}
