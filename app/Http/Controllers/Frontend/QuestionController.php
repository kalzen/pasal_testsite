<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\QuestionRepository;
class QuestionController extends Controller
{
    public function __construct(QuestionRepository $questionRepo){
        $this->questionRepo = $questionRepo;
    }
    public function create(Request $request){
        $input = $request->all();
        $question = $this->questionRepo->createQuestion($input);
        $html = '
            <div class="message-item ">
                <div class="img-container">
                    <img src="'.asset(\Auth::guard('member')->user()->avatar).'" alt="'.\Auth::guard('member')->user()->fullname.'">
                    <span>'.\Auth::guard('member')->user()->fullname.'</span>
                    <time class="timeago" datetime="2019-06-10T09:40:13+07:00">cách đây '.$question->getDifferentTime().'</time>
                </div>
                <div class="message  ">
                    <div class="message-body-arrow"></div>
                    '.$question->content.'
                </div>
            </div>';
        return response()->json(['html'=>$html]);
    }
}
