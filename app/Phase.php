<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $table='phase';
    
    protected $fillable = [
        'id', 'name', 'order'
    ];
    
    public function tests($parent=0) {
        return $this->hasMany('App\Test')->where('parent_id', '=', $parent);
    }
}
