<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateMonthly extends Model
{
    protected $table='affiliate_monthly';
    
    protected $fillable = [
        'affiliate_id', 'monthly_id', 'count_data', 'count_traffic'
    ];
    
   
}
