<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberTest extends Model {

    protected $table = 'member_test';
    protected $fillable = [
        'member_id', 'test_id', 'percent_point', 'comment', 'created_at', 'updated_at', 'is_viewed', 'video_link', 'audio_link'
    ];

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getCreatedAt() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getUpdatedAt() {
        return date("d/m/Y H:i:s", strtotime($this->updated_at));
    }

    public function getDifferentHour() {
        $dif = (strtotime(date('Y-m-d H:i:s')) - strtotime($this->updated_at));
        if ($dif < 60 * 60)
            $res = round($dif / 60) . ' phút';
        elseif ($dif < 24 * 60 * 60)
            $res = round($dif / (60 * 60)) . ' tiếng';
        else
            $res = round($dif / (24 * 60 * 60)) . ' ngày';
        return $res;
    }

    public function member() {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function test() {
        return $this->belongsTo('App\Test', 'test_id');
    }

    public function getComment() {
        return $this->comment ? 'Đã phản hồi' : 'Chưa có phản hồi';
    }

    public function getIdYoutube() {
        $url = $this->video_link;
        if($url==null) return "";
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        $youtube_id = $match[1];
        return $youtube_id;
    }

    public function linkAudio() {
        return asset('uploads/audio/' . $this->audio_link);
    }

    public function message() {
        $question_query = \DB::table('question')->where('member_id', $this->member_id)->where('test_id', $this->test_id);
        $answer_query = \DB::table('answer')->whereIn('question_id', $question_query->pluck('id'));
        if ($question_query->get()->count() == 0) {
            return "(-)";
        }
        if ($question_query->get()->count() > $answer_query->get()->count()) {
            $message = "<a href=".route('admin.member_test.detail', $this->member_id).">Có tin nhắn mới</a>";
        } else {
            $message = "Đã trả lời";
        }
        return $message;
    }

}
