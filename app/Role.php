<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model{
    
    protected $table='role';
    
    protected $fillable = [
        'id', 'name', 'route'
    ];
    
    public function route() {
        return explode(',', $this->route);
    }
}
