<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model {

    protected $table = 'schedule';
    protected $fillable = [
        'id', 'name', 'schedule', 'start_time', 'end_time', 'start_date', 'end_date', 'order'
    ];
    
    
   
}
