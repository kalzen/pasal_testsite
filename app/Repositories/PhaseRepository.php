<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class PhaseRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Phase';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:phase',
            'order' => 'required'
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required|unique:phase,name,' . $id,
            'order' => 'required',
        ];
    }
    
    public function getPhaseByAlias($alias) {
        return $this->model->where('alias', '=', $alias)->first();
    }
   
}
