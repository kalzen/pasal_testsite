<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class RankRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Rank';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required',
        ];
    }
    
    public function getAll() {
        return $this->model->orderBy('order')->get();
    }
    
    public function findByAlias($alias) {
        return $this->model->where('alias', '=', $alias)->first();
    }
    
    public function getNextChanllenge($id) {
        return $this->model->where('id', '=', $id+1)->first();
    }
    
    public function getByPhase($phase_id=1) {
        return $this->model->where('phase_id', '=', $phase_id)->orderBy('start_point', 'ASC')->get();
    }

    

    

}
