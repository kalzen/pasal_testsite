<?php

namespace Repositories;

use Illuminate\Support\Facades\Schema;
use Cookie;
use Illuminate\Support\Facades\Cache;

class BootRepository {

    const LIMIT = 100;

    public function boot() {
        $config = \DB::table('config')->first();
        $notification = \DB::table('member_test')
                ->where('member_id', \Auth::guard('member')->user()->id)
                ->where('is_viewed', 0)
                ->get();
        $count_noti = $notification->count();
        \View::share(['config' => $config, 'count_noti' => $count_noti]);
    }
}