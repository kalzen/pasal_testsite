<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class MemberTestRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\MemberTest';
    }

    public function getAllByRequest($request) {
        $question_ids = \DB::table('answer')->pluck('question_id');
        $question_query = \DB::table('question')->whereNotIn('id', $question_ids);
        if (\Auth::user()->role_id == \App\User::ROLE_TRAINER) {
            $member_ids = \DB::table('member')->where('trainer_id', \Auth::user()->id)->pluck('id');
            return $this->model->whereIn('member_id', $member_ids)
                    ->whereIn('member_id', $question_query->pluck('member_id'))
                    ->whereIn('test_id', $question_query->pluck('test_id'))
                    ->orderBy('created_at', 'desc')->get();
        } else {
            return $this->model
                        ->whereIn('member_id', $question_query->pluck('member_id'))
                        ->whereIn('test_id', $question_query->pluck('test_id'))
                        ->orderBy('created_at', 'desc')->get();
        }
    }

    public function getAll() {
        if (\Auth::user()->role_id == \App\User::ROLE_TRAINER) {
            $member_ids = \DB::table('member')->where('trainer_id', \Auth::user()->id)->pluck('id');
            return $this->model->orderBy('created_at', 'desc')->whereIn('member_id', $member_ids)->get();
        } else {
            return $this->model->orderBy('created_at', 'desc')->get();
        }
    }

    public function getMembertestByMemberId($member_id) {
        return $this->model->where('member_id', $member_id)->orderBy('updated_at', 'desc')->get();
    }

    public function findByMemberId($member_id) {
        return $this->model->where('member_id', $member_id)
                        ->where(function($query) {
                            $query->where('percent_point', '<>', 0)
                            ->orWhere('comment', '<>', null);
                        })
                        ->orderBy('updated_at', 'desc')->get();
    }

    public function updateMembertest($input, $id) {
        return $this->model->where('id', $id)->update($input);
    }

    public function getMarkModal($id) {
        $record = $this->model->find($id);
        $html = '
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title text-bold">Chấm điểm</h5>
                    </div>

                    <form id="frmMembertest" action="' . route('api.member_test.update', $id) . '" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="text-bold">Thành viên</label>
                                        <input type="text" value="' . $record->member->fullname . '" readonly class="form-control">
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="text-bold">Thử thách</label>
                                        <input type="text" value="' . $record->test->name . '" readonly  class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="text-bold">Phản hồi</label>
                                        <textarea type="text" name="comment"  class="form-control">' . $record->comment . '</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="text-bold">Điểm hoàn thành</label>
                                        <input name="percent_point" value="' . $record->percent_point . '" class="touchspin-vertical input-md form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Hủy</button>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>';
        return $html;
    }

    public function getBestPoint($member_id, $test_id) {
        $model = $this->model->where('member_id', '=', $member_id)->where('test_id', '=', $test_id)->orderBy('percent_point', 'DESC')->first();
        if (!is_null($model)) {
            return $model->percent_point;
        } else {
            return 0;
        }
    }

    public function updateViewComment($id) {
        $model = $this->model->where('id', $id)->first();
        if ($model) {
            $model->is_viewed = 1;
            $model->timestamps = false;
            $model->save();
        }
        return $model;
    }

    public function updateViewCount($id) {
        $model = $this->model->where('id', $id)->first();
        if ($model) {
            $model->view_count += 1;
            $model->timestamps = false;
            $model->save();
        }
        return $model;
    }

    public function getMembertest($member_id, $test_id) {
        return $this->model->where('member_id', $member_id)->where('test_id', $test_id)->get();
    }

    public function getAudio($member_id, $test_id) {
        return $this->model
                        ->where('member_id', $member_id)
                        ->where('test_id', $test_id)
                        ->whereNotNull('audio_link')
                        ->get();
    }

    public function toggleIsHot($id) {
        $model = $this->find($id);
        return $this->model->where('id', $id)->update(['is_hot' => 1 - $model->is_hot]);
    }

    public function getIsHotMembertest($test_id) {
        return $this->model->where('test_id', $test_id)->where('is_hot', 1)->orderBy('view_count', 'desc')->get();
    }

    public function getRecordCtest() {
        $model = $this->model
                ->where('member_id', '=', \Auth::guard('member')->user()->id)
                ->selectRaw('test_id, max(percent_point) as point')
                ->groupBy('test_id')
                ->orderBy('test_id')
                ->get()
                ->toArray();
        $record = [];
        for ($i = 0; $i < \App\Test::MAX_test; $i++) {
            if (isset($model[$i]['test_id'])) {
                $record = array_merge($record, array($model[$i]['point']));
            } else {
                $record = array_merge($record, [0]);
            }
        }
        return $record;
    }

    public function getProgress() {
        $model = $this->model
                ->where('member_id', '=', \Auth::guard('member')->user()->id)
                ->where('percent_point', '>=', 50)
                ->selectRaw('test_id, max(percent_point) as point')
                ->groupBy('test_id')
                ->orderBy('test_id')
                ->get();
        $progress = count($model) / \App\Test::MAX_test * 100;
        return $progress;
    }

    public function getRanking() {
        $sub = $this->model
                ->where('percent_point', '>=', 50)
                ->selectRaw('member_id,test_id, max(percent_point) as point')
                ->groupBy('member_id', 'test_id')
                ->orderBy('member_id');
        $sql = $sub->toSql();
        $query = \DB::table(\DB::raw("($sql) as t1"))
                ->mergeBindings($sub->getQuery())
                ->selectRaw('member_id, avg(point) as avg_point, count(test_id) as count, avg(point)*count(test_id)/10 as total')
                ->groupBy('member_id')
                ->orderBy('total', 'DESC')
                ->get();

        return $query;
    }

    public function getNumberFinish() {
        $model = $this->model
                ->where('member_id', '=', \Auth::guard('member')->user()->id)
                ->where('percent_point', '>=', 50)
                ->selectRaw('test_id, max(percent_point) as point')
                ->groupBy('test_id')
                ->orderBy('test_id')
                ->get();
        return count($model);
    }

    public function checkHasNewtest($member_id){
        return $this->model->where('member_id', $member_id)->where('comment', null)->where('percent_point', null)->get()->count();
    }

}
