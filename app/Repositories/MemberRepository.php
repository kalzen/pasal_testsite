<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class MemberRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Customer';
    }

    public function getAllMember($request) {
        $end_date = $request->get('end_date');
        $query = $this->model;
        if ($request !== NULL) {
            //            Search basis 
            $basis = $request->get('basis');
            if (!empty($basis)) {
                $query = $query
                        ->whereIn('basis_id', $basis);
            }//            Search province 
            $province = $request->get('province');
            if (!empty($province)) {   
                $query = $query
                        ->where('province_id', '=', $province);
            }      
            $teacher = $request->get('teacher');
            if (!empty($teacher)) {
                $query = $query
                        ->where('user_id', '=', $teacher);
            }
            $now_appointment = $request->get('now_appointment');
            if (!empty($now_appointment)) {
                $query = $query
                        ->whereDate('appointment', \Carbon\Carbon::today());
            }
            
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');
            
            if (!empty($start_date)) {
                $query = $query
                        ->join('result','customer.id', '=', 'result.customer_id')
                        ->select('customer.*', 'result.created_at as resultdate')
                        ->whereDate('result.created_at', '>=', date('Y-m-d',strtotime($start_date)));
            }
            if (!empty($end_date)) {
                $query = $query
                        ->whereDate('result.created_at', '<=', date('Y-m-d',strtotime($end_date)));
            }
            
            $register_start_date = $request->get('register_start_date');
            $register_end_date = $request->get('register_end_date'); 
            if (!empty($register_start_date)) {
                $query = $query
                        ->whereDate('customer.created_at', '>=', date('Y-m-d',strtotime($register_start_date)));
            }
            if (!empty($register_end_date)) {
                $query = $query
                        ->whereDate('customer.created_at', '<=', date('Y-m-d',strtotime($register_end_date)));
            }
            
        }
        return $query->orderBy('created_at', 'desc')->paginate(100);
    }

    public function getTopMember() {
        $max_point_arr = \DB::table('member_test')->select('member_id', 'test_id', \DB::raw('MAX(percent_point) as max_point'))
                ->groupBy('member_id', 'test_id');
        $avg_point_arr = array();
    }

    public function switchTrainer($member_ids, $new_trainer_id) {
        return $this->model->whereIn('id', $member_ids)->update(['trainer_i2d' => $new_trainer_id]);
    }

}
