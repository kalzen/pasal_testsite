<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class BlockRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Block';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required',
            'order' => 'required',
            'position_title'=> 'required'
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required',
            'order' => 'required',
            'position_title'=> 'required'
        ];
    }

    public function getBlockByPosition($position) {
        $data = $this->model->where('position', '=', $position)->get();
        return $data;
    }

    public function getBlocksByParentId($parent_id = 0) {
        $block = $this->model->where('parent_id', '=', $parent_id)->orderBy('order', 'ASC')->get();
        $content = [];
        foreach ($block as $key => $val) {
            $check =  $this->checkPosition($val->position);
            if (!$check) {
                $content[$val->position] = $val; 
            } else {
                $content[$val->position][] = $val;  
            }
        }
        return $content;
    }
    
    public function getAllChildrenBLock() {
        $blockChildren = $this->model->where('parent_id', '<>', 0)->orderBy('order', 'ASC')->get();
        $contentChildren = [];
        foreach ($blockChildren as $key => $val) {
            $check =  $this->checkPosition($val->position);
            if (!$check) {
                $contentChildren[$val->position] = $val; 
            } else {
                $contentChildren[$val->position][] = $val;  
            }
        }
        return $contentChildren;
    }
    
    public function checkPosition($position) {
        $data = $this->model->where('position', '=', $position)->get();
        if (count($data) > 1) {
            return true;
        }
        return false;
    }

}
