<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class ScheduleRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Schedule';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:basis',
            'schedule' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required|unique:basis,name,' . $id,
            'schedule' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }

}
