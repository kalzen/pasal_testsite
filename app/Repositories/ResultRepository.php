<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class ResultRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Result';
    }
    
    public function createOrUpdate($data) {
        $check = $this->model->where('customer_id', '=', $data['customer_id'])->first();
        if($check==null){
            $this->create($data);
        }else{
            $check->update($data);
        }
        return true;
    }

   

}
