<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class TestRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Test';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required',
        ];
    }
    
    public function getAll() {
        return $this->model->orderBy('order')->get();
    }
    
    public function findByAlias($alias) {
        return $this->model->where('alias', '=', $alias)->first();
    }
    
    public function getNextChanllenge($id) {
        return $this->model->where('id', '=', $id+1)->first();
    }
    
    public function getTestByPhase($phase_id=1) {
        return $this->model->where('phase_id', '=', $phase_id)->where('parent_id', '=', 0)->orderBy('order', 'ASC')->get();
    }
    
    public function getQuestionByPhase($phase_id=1, $parent=null) {
        if($parent==null){
            return $this->model->where('phase_id', '=', $phase_id)->where('parent_id', '<>', 0)->orderBy('order', 'ASC')->get();
        }else{
            return $this->model->where('phase_id', '=', $phase_id)->where('parent_id', '=', $parent)->orderBy('order', 'ASC')->get();
        }
        
    }
    
    public function getTotalPoint() {
        return $this->model->where('parent_id', '<>', 0)->sum('point');
    }

    

}
