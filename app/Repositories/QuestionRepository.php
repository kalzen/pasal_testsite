<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class QuestionRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Question';
    }

    public function findByMemberId($member_id) {
        return $this->model->where('member_id', $member_id)->get();
    }

    public function getQuestionByMemberId($member_id, $test_id) {
        return $this->model->where('member_id', $member_id)->where('test_id', $test_id)->get();
    }
    
    public function createQuestion($input){
        return $this->model->create($input);
    }

}
