<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class MonthlyRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Monthly';
    }

    public function getAll() {
        return $this->model->orderBy('name')->get();
    }
    
    public function getString() {
        $string = '';
        $models = $this->model->orderBy('name', 'asc')
                ->get();
        foreach ($models as $key=>$model) {
            if($key==0){
                $string = '"'.$model->name.'"';
            }else{
                $string = $string.',"'.$model->name.'"';
            }     
        }
        return $string;
    }
    public function getByMonthly($request) {
        $query = $this->model;
        if ($request !== NULL) {
            $start = $request->get('start');
            $end = $request->get('end');
            if (!empty($start)) {
                $query = $query
                        ->where('id', '>=', $start);
            }
            if (!empty($end)) {
                $query = $query
                        ->where('id', '<=', $end);
            }
            
        }
        $array = [];
        $models = $query->get();
        foreach ($models as $key=>$model) {
            $array[$model->name] = $model->id;
        }
        return $array;
    }
}
