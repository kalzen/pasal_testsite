<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class TrainerRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Trainer';
    }

    public function validateCreate() {
        return $rules = [
            'username' => 'required|unique:user',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'name' => 'required'
        ];
    }
    public function validateUpdate($id) {
        return $rules = [
            'username' => 'required|unique:user,username,' . $id . ',id',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'name' => 'required'
        ];
    }

    function getAllTrainer() {
        $trainers = $this->model->where('role_id', '=', \App\User::ROLE_TRAINER)->get();
        return $trainers;
    }
    function getTrainerByRole($role_id){
        return $this->model->where('role_id', $role_id)->get();
    }
    public function exportUser($group) {
        if (in_array(0, $group)) {
            $users = $this->model->where('role_id', '<>', \App\User::ROLE_ADMIN)->get();
        } else {
            $check = true;
            if ($group[0] < 0) {
                $check = false;
                foreach ($group as $key => $value) {
                    $group[$key] = abs($value);
                }
            }
            if ($check == false) {
                $users = $this->model->where('role_id', '<>', \App\User::ROLE_ADMIN)->whereNotIn('id', $group)->get();
            } else {
                $users = $this->model->where('role_id', '<>', \App\User::ROLE_ADMIN)->whereIn('id', $group)->get();
            }
        }
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A1', trans('base.id'))
                ->setCellValue('B1', trans('base.fullname'))
                ->setCellValue('C1', trans('base.tel'))
                ->setCellValue('D1', trans('base.email'))
                ->setCellValue('E1', trans('base.school'))
                ->setCellValue('F1', trans('base.created_at'))
                ->setCellValue('G1', trans('base.basis_code'))
                ->setCellValue('H1', trans('base.schedule'))
                ->setCellValue('I1', trans('base.link'));

        foreach ($users as $key => $user) {
            $row = $key + 2;
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $user->id)
                    ->setCellValue('B' . $row, $user->fullname)
                    ->setCellValue('C' . $row, $user->tel())
                    ->setCellValue('D' . $row, $user->email)
                    ->setCellValue('E' . $row, $user->school)
                    ->setCellValue('F' . $row, $user->created_at)
                    ->setCellValue('G' . $row, $user->basis->name)
                    ->setCellValue('H' . $row, $user->schedule->name)
                    ->setCellValue('I' . $row, $user->affiliate->name);
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="danh-sach-thanh-vien.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

}
