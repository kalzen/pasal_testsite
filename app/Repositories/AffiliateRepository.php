<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class AffiliateRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Affiliate';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required',
            'code' => 'required|min:6',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'code' => 'required|unique:affiliate,code,' . $id
        ];
    }

    function getAllAffiliate($request) {
        $query = $this->model;
        if ($request !== NULL) {
            //            Search theo start_date
            $start_date = $request->get('start_date');
            if (!empty($start_date)) {
                $query = $query
                        ->where('created_at', '>=', $start_date. ' 00:00:00');
            }
            //             Search theo end_date
            $end_date = $request->get('end_date');
            if (!empty($end_date)) {
                $query = $query
                        ->where('created_at', '<=', $end_date. ' 23:59:59');
            }
//            dd($start_date, $end_date);
            //            Search keyword 
            $searchText = $request->get('keyword');
            if (!empty($searchText)) {
                $query = $query
                        ->where('name', 'LIKE', "%" . $searchText . "%");
            }
        }
        $affiliates = $query->orderBy('created_at', 'DESC')->get();
        return $affiliates;
    }

    public function countTraffic($ref) {
        $affiliate = $this->model->where('code', '=', $ref)->first();
        $monthly = date('m/Y');
        $month = \DB::table('monthly')->where('name', '=', $monthly)->first();
        if (!is_null($affiliate)) {
            if (is_null($month)) {
                \DB::table('monthly')->insert(['name' => $monthly]);
                $month = \DB::table('monthly')->where('name', '=', $monthly)->first();
            }
            $check = \DB::table('affiliate_monthly')->where('affiliate_id', '=', $affiliate->id)->where('monthly_id', '=', $month->id)->first();

            if (!is_null($check)) {
                $new_traffic = ++$check->count_traffic;
                \DB::table('affiliate_monthly')
                        ->where('affiliate_id', '=', $affiliate->id)
                        ->where('monthly_id', '=', $month->id)
                        ->update(['count_traffic' => $new_traffic]);
            } else {
                \DB::table('affiliate_monthly')->insert([
                    'affiliate_id' => $affiliate->id,
                    'monthly_id' => $month->id,
                    'count_traffic' => 1,
                    'count_data' => 0
                ]);
            }
        }
        return true;
    }

    public function countData($ref) {
        $affiliate = $this->model->where('code', '=', $ref)->first();
        $monthly = date('m/Y');
        $month = \DB::table('monthly')->where('name', '=', $monthly)->first();
        if (!is_null($affiliate) && !is_null($month)) {
            $check = \DB::table('affiliate_monthly')->where('affiliate_id', '=', $affiliate->id)->where('monthly_id', '=', $month->id)->first();
            if (!is_null($check)) {
                $new_data = ++$check->count_data;
                \DB::table('affiliate_monthly')
                        ->where('affiliate_id', '=', $affiliate->id)
                        ->where('monthly_id', '=', $month->id)
                        ->update(['count_data' => $new_data]);
            }
        }
    }

    function toggleGroup($group, $status) {

        if (in_array(0, $group)) {
            $affiliates = $this->model;
        } else {
            $check = true;
            if ($group[0] < 0) {
                $check = false;
                foreach ($group as $key => $value) {
                    $group[$key] = abs($value);
                }
            }
            if ($check == false) {
                $affiliates = $this->model->whereNotIn('id', $group);
            } else {
                $affiliates = $this->model->whereIn('id', $group);
            }
        }
        $affiliates->where('status', '<>', $status)->update(['status' => $status]);
    }

    public function getByMonthly($monthly) {
        $model = $this->model->leftJoin('affiliate_monthly', 'affiliate.id', 'affiliate_monthly.affiliate_id')
                        ->whereIn('affiliate_monthly.monthly_id', $monthly)->get();
        return $model;
    }

}
