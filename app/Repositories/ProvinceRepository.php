<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class ProvinceRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Province';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:province',
            'order' => 'required'
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required|unique:province,name,' . $id,
            'order' => 'required',
        ];
    }
  
    public function getAllProvince(){
        return $this->model->orderBy('order', 'asc')->get();
    }
}
