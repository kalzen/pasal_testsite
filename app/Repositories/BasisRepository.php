<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class BasisRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Basis';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:basis',
            'code' => 'required|unique:basis'
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required|unique:basis,name,' . $id,
            'code' => 'required|unique:basis,code,' . $id,
        ];
    }
    
    public function getString() {
        $string = '';
        $models = $this->model
                ->get();
        foreach ($models as $key=>$model) {
            if($key==0){
                $string = '"'.$model->name.'"';
            }else{
                $string = $string.',"'.$model->name.'"';
            }     
        }
        return $string;
    }
    
    public function countData($basis_id) {
        $basis = $this->model->where('id', '=', $basis_id)->first();
        $monthly = date('m/Y');
        $month = \DB::table('monthly')->where('name', '=', $monthly)->first();
        if (!is_null($basis)) {
            if (is_null($month)) {
                \DB::table('monthly')->insert(['name' => $monthly]);
                $month = \DB::table('monthly')->where('name', '=', $monthly)->first();
            }
        }
        return true;
    }
    public function getAllBasis(){
        return $this->model->orderBy('order', 'asc')->get();
    }
}
