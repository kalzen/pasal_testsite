<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class AnswerRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Answer';
    }
    public function updateContent($input){
        $input['trainer_id'] = \Auth::user()->id;
        return $this->model->updateOrCreate(['question_id'=>$input['question_id']],$input);
    }
    
    public function createAnswer($input) {
        foreach($input['content_question'] as $key=>$data){
            $this->model->create(['test_id'=>$input['test_id'], 'content'=>$data, 'status'=>(isset($input['status_question'][$key])?'1':'0')]);
        }
        return true;
    }
    
    public function getAnwserByTestId($id) {
        $data = $this->model->where('test_id', '=',$id)->get();
        return $data;
    }
}
