<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CustomerRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Customer';
    }

    public function validate() {
        return $rules = [
            'fullname' => 'required',
            'tel' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'email' => 'email|required',
            'basis_id' => 'required',
            'province_id' => 'required',
        ];
    }
    public function validateBE() {
        return $rules = [
            'fullname' => 'required',
            'tel' => 'required|unique:customer',
            'email' => 'email|required',
            'basis_id' => 'required',
            'province_id' => 'required',
        ];
    }
    public function validateBEUpdate($id) {
        return $rules = [
            'fullname' => 'required',
            'tel' => 'required|unique:customer,tel,' . $id . ',id',
            'email' => 'email|required',
            'basis_id' => 'required',
            'province_id' => 'required',
        ];
    }

    public function findBytel($tel) {
        return $this->model->where('tel', '=', $tel)->first();
    }
    function getAllCustomer($request) {
        $query = $this->model;
        if ($request !== NULL) {
            //            Search basis 
            $basis = $request->get('basis');
            if (!empty($basis)) {
                $query = $query
                        ->whereIn('basis_id', $basis);
            }
            //            Search schedule 
            $schedule = $request->get('schedule');
            if (!empty($schedule)) {
                $query = $query
                        ->whereIn('schedule_id', $schedule);
            }
            //            Search theo start_date
            $start_date = $request->get('start_date');
            if (!empty($start_date)) {
                $query = $query
                        ->whereDate('created_at', '>=', date('Y-m-d',strtotime($start_date)));
            }
            //             Search theo end_date
            $end_date = $request->get('end_date');
            if (!empty($end_date)) {
                $query = $query
                        ->whereDate('created_at', '<=', date('Y-m-d',strtotime($end_date)));
            }
           
            //            Search keyword 
            $searchText = $request->get('keyword');
            if (!empty($searchText)) {
                $query = $query
                        ->where('fullname', 'LIKE', "%" . $searchText . "%")
                        ->orWhere('tel', 'LIKE', "%" . $searchText . "%")
                        ->orWhere('school', 'LIKE', "%" . $searchText . "%");
            }
        }
        $members = $query->orderBy('created_at', 'DESC')->get();
        return $members;
    }

    public function exportCustomer($group) {
        if (in_array(0, $group)) {
            $members = $this->model->get();
        } else {
            $check = true;
            if ($group[0] < 0) {
                $check = false;
                foreach ($group as $key => $value) {
                    $group[$key] = abs($value);
                }
            }
            if ($check == false) {
                $members = $this->model->whereNotIn('id', $group)->get();
            } else {
                $members = $this->model->whereIn('id', $group)->get();
            }
        }
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A1', trans('base.id'))
                ->setCellValue('B1', trans('base.fullname'))
                ->setCellValue('C1', trans('base.tel'))
                ->setCellValue('D1', trans('base.email'))
                ->setCellValue('E1', trans('base.school'))
                ->setCellValue('F1', trans('base.created_at'))
                ->setCellValue('G1', trans('base.basis_code'))
                ->setCellValue('H1', trans('base.schedule'))
                ->setCellValue('I1', trans('base.link'));

        foreach ($members as $key => $member) {
            $row = $key + 2;
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $member->id)
                    ->setCellValue('B' . $row, $member->fullname)
                    ->setCellValue('C' . $row, $member->tel())
                    ->setCellValue('D' . $row, $member->email)
                    ->setCellValue('E' . $row, $member->school)
                    ->setCellValue('F' . $row, $member->created_at)
                    ->setCellValue('G' . $row, $member->basis->name)
                    ->setCellValue('H' . $row, $member->schedule->name)
                    ->setCellValue('I' . $row, !is_null($member->affiliate)?$member->affiliate->name:'');
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="danh-sach-thanh-vien.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

}
