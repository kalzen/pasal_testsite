<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basis extends Model
{
    protected $table='basis';
    
    protected $fillable = [
        'id', 'name', 'code', 'order', 'description'
    ];
    
   
}
