<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Answer extends Model {

    protected $table = 'answer';
    protected $fillable = [
        'test_id', 'status', 'content'
    ];

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getCreatedAt() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getUpdatedAt() {
        return date("d/m/Y", strtotime($this->updated_at));
    }

    public function trainer() {
        return $this->belongsTo('App\User', 'trainer_id');
    }

    function random_content() {
        $array = explode(' ', $this->content);
        shuffle($array);
        return $array;
    }

}
