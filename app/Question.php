<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $table = 'question';
    protected $fillable = [
        'member_id', 'test_id', 'content'
    ];
    
    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }
    public function getCreatedAt() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getDifferentTime(){
        $dif = (strtotime(date('Y-m-d H:i:s')) - strtotime($this->created_at));
        if($dif < 60*60) $res = round($dif/60).' phút';
        elseif($dif < 24*60*60) $res = round($dif/(60*60)).' tiếng';
        else $res = round($dif/(24*60*60)).' ngày';        
        return $res;
    }
    public function answer() {
        return $this->hasOne('App\Answer');
    }

}
