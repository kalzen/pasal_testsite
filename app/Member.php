<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable {

    protected $table = 'member';
    protected $hidden = [
        'password', 'remember_token'
    ];
    protected $fillable = [
        'email', 'fullname', 'provider_id', 'password', 'trainer_id', 'avatar', 'address', 'phone', 'remember_token', 'provider', 'status', 'province_id', 'basis_id', 'content', 'link_register'
    ];

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function trainer() {
        return $this->belongsTo('App\User', 'trainer_id');
    }

    public function test() {
        return $this->hasMany('App\MemberTest');
    }
    public function basis(){
        return $this->belongsTo('App\Basis');
    }
}
