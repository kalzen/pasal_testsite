<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable {
       
    protected $table = 'customer';
    
    const RBG_ARRAY = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname',  'school', 'tel', 'email', 'basis_id', 'province_id', 'affiliate_id', 'appointment', 'user_id', 'link_register', 'link_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'recustomer_token',
    ];

    public function role() {
        return $this->belongsTo('App\Role');
    }
    
    public function orders() {
        return $this->hasMany('App\Order', 'customer_id', 'id');
    }
    
    public function basis() {
        return $this->belongsTo('App\Basis');
    }
    
    public function schedule() {
        return $this->belongsTo('App\Schedule');
    }
    
    public function affiliate() {
        return $this->belongsTo('App\Affiliate');
    }
    public function result() {
        return $this->hasOne('App\Result', 'customer_id');
    }
    
    public function tel() {
        return '0'.$this->tel;
    }
    
    public function created_at() {
        return date( "d/m/Y H:i", strtotime($this->created_at));
    }
    
    public function updated_at() {
        return date( "d/m/Y H:i", strtotime($this->updated_at));
    }
    
    public function province() {
        return $this->belongsTo('App\Province');
    }
    
    public function user() {
        return $this->belongsTo('App\User');
    }

}
