<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model {

    protected $table = 'test';
    protected $fillable = [
        'id', 'name', 'content', 'order', 'point', 'file', 'parent_id', 'phase_id', 'layout', 'type'
    ];

    const MAX_test = 10;

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function createdAt() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getParent() {
        return $this->belongsTo('App\Test', 'parent_id')->first();
    }

    public function getChildren() {
        return $this->hasMany('App\Test', 'parent_id', 'id');
    }

    public function answers() {
        return $this->hasMany('App\Answer');
    }

   

}
