<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model{
    
    protected $table='rank';
    
    protected $fillable = [
        'id', 'phase_id', 'start_point', 'end_point', 'name', 'content', 'solution', 'order'
    ];
    
    public function route() {
        return explode(',', $this->route);
    }
}
