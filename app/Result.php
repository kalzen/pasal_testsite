<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model {

    protected $table = 'result';
    protected $fillable = [
        'id', 'customer_id', 'phase_1', 'phase_2', 'phase_3', 'phase_4', 'phase_4_answer'
    ];

    const MAX_test = 10;

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function createdAt() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function getParent() {
        return $this->belongsTo('App\Test', 'parent_id')->first();
    }

    public function getChildren() {
        return $this->hasMany('App\Test', 'parent_id', 'id');
    }

    public function answers() {
        return $this->hasMany('App\Answer');
    }

    public function point($phase = 'phase_1') {
        $point = 0;
        if ($this->$phase !== null) {
            $result = explode('|', $this->$phase);
            foreach ($result as $record) {
                if (array_key_exists(1, explode(':', $record))) {       
                    $question_id = explode(':', $record)[0];
                    $answer_id = explode(':', $record)[1];
                    $check = Answer::find($answer_id);
                    if ($check !== null && $check->status == 1) {
                        $new_point = Test::find($question_id)->point;
                        $point = $point + $new_point;
                    } elseif ($check == null) {            
                        $check = Answer::find($question_id);
                        if (isset($check)) {
                            $total_point = Test::find($check->test_id)->point;
                            $number = Test::find($check->test_id)->answers->count();
                            if ($answer_id == $check->content) {
                                $new_point = number_format(1/$number*$total_point);
                                $point = $point + $new_point;
                            }
                        }
                    }
                }
            }
        }
        return $point;
    }

    public function pointPhase4() {
        $point = 0;
        if ($this->phase_4 !== null) {
            $result = explode('|', $this->phase_4);
            foreach ($result as $record) {
                if (array_key_exists(1, explode(':', $record))) {
                    $question_id = explode(':', $record)[0];
                    $new_point = explode(':', $record)[1];
                    $point = $point + $new_point;
                }
            }
        }
        return $point;
    }

    public function totalPoint($phase = null) {
        $point = 0;
        if ($phase == null) {
            $point = Test::all()->sum('point');
        } else {
            $point = Test::where('phase_id', '=', $phase)->get()->sum('point');
        }

        return $point;
    }

    public function getRank($phase) {
        if ($phase != 4) {
            $point = $this->point('phase_' . $phase);
        } else {
            $point = $this->pointPhase4();
        }
        $rank = Rank::where('phase_id', '=', $phase)
                ->where('start_point', '<=', $point)
                ->where('end_point', '>=', $point)
                ->first();
        return $rank;
    }

    public function avPoint() {
        $av1 = $this->point('phase_1') / $this->totalPoint(1) * 100;
        $av2 = $this->point('phase_2') / $this->totalPoint(2) * 100;
        $av3 = $this->point('phase_3') / $this->totalPoint(3) * 100;
        $av4 = $this->pointPhase4() / $this->totalPoint(4) * 100;
//        dd($av1, $av2, $av3, $av4, ($av1+$av2+$av3+$av4)/4);
        return number_format(($av1 + $av2 + $av3 + $av4) / 4);
    }

}
