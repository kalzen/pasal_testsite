<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model {

    protected $table = 'affiliate';
    protected $fillable = [
        'id', 'name', 'code', 'description', 'count_data', 'count_traffic', 'status'
    ];

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function monthly($month = null) {
        if ($month == null) {
            return $this->hasMany('App\AffiliateMonthly');
        }else{
            return $this->hasMany('App\AffiliateMonthly')->where('monthly_id', '=', $month)->first();
        }
    }

}
